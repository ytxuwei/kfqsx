package com.example.index;

import com.example.jhf.OneFragment;
import com.example.jhf.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
/**
 * 创建日期: 2015/09/12 创建者： 刘永华 功能说明：学校概况 修改履历： VER 修改日 修改者 修改内容／理由
 * ──────────────────────────────────────────────────────────────
 */
public class ShoolBroad extends Activity {
	private Button BTShoolJS,BTShoolFM,BTShoolLS;
	private ImageView shooll_GK;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.shoolbroad);
		BTShoolJS=(Button) findViewById(R.id.Shool_jianjie);
		BTShoolFM=(Button) findViewById(R.id.shool_fengmao);
		BTShoolLS=(Button) findViewById(R.id.shool_xiaoshi);
		shooll_GK=(ImageView) findViewById(R.id.shooll_GK);
		shooll_GK.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ShoolBroad.this.finish();
			}
		});
		BTShoolJS.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					startActivity(new Intent(ShoolBroad.this, ShoolJS.class));
					
				}
			});	
		BTShoolFM.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(ShoolBroad.this, ShoolFM.class));
				
			}
		});	
		BTShoolLS.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(ShoolBroad.this, ShoolLI.class));
				
			}
		});	
		
	}
}
