package com.example.jhf;

import java.util.List;

import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.imooc.baseadapter.utils.BitmapCache;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.Gallery.LayoutParams;
import android.widget.ImageView.ScaleType;

public class ImgAdapter extends BaseAdapter {
	private Context _context;
	private List<String> imgList; 
	public ImgAdapter(Context context,List<String> imgList ) {
		_context = context;
		this.imgList=imgList;
	}

	public int getCount() {
		return Integer.MAX_VALUE;
	}

	public Object getItem(int position) {

		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		RequestQueue mQueue = Volley.newRequestQueue(_context);
		ImageLoader imageLoader = new ImageLoader(mQueue, new BitmapCache());
		if (convertView == null) {
			viewHolder = new ViewHolder();
			NetworkImageView imageView = new NetworkImageView(_context);
			imageView.setAdjustViewBounds(true);
			imageView.setScaleType(ScaleType.FIT_XY);
			imageView.setLayoutParams(new Gallery.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			convertView = imageView;
			viewHolder.imageView = (NetworkImageView) convertView;
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		//viewHolder.imageView.setImageResource(imgList.get(position % imgList.size()));
		viewHolder.imageView.setImageUrl(imgList.get(position%imgList.size()), imageLoader);
		return convertView;
	}

	private static class ViewHolder {
		NetworkImageView imageView;
	}
}
