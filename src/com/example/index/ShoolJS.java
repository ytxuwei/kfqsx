package com.example.index;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EncodingUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.global.Constant;
import com.example.jhf.IndexActivity;
import com.example.jhf.MainActivity;
import com.example.jhf.R;
import com.imooc.baseadapter.utils.Tools;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.NoCopySpan.Concrete;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 学校简介
 * 
 * @author Administrator
 * 
 */
public class ShoolJS extends Activity {
	private ImageView shooll_GK;
	private WebView tvContent;
	private ObjectMapper om = new ObjectMapper();
	private String noteStr;
	private String code;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.shooljs);
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		tvContent = (WebView) findViewById(R.id.tv_schooljs);
		shooll_GK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ShoolJS.this.finish();
			}
		});
		getJsonText();
	}

	/**
	 * 获取学校简介的JSON数据
	 */
	private void getJsonText() {
		final Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String string = msg.obj.toString();
					try {
						JSONObject jsonObject = new JSONObject(string);
						String str = jsonObject.get("data").toString();
						JSONObject jsonObject2 = new JSONObject(str);
						String noteStr = jsonObject2.get("note").toString();
						int a = 14; // 字体大小
						String strUrl = "<html> \n"
								+ "<head> \n"
								+ "<style type=\"text/css\"> \n"
								+ "body {text-align:justify; font-size: "
								+ a
								+ "px; line-height: "
								+ (a + 6)
								+ "px}\n"
								+ "</style> \n"
								+ "</head> \n"
								+ "<body>"
								+ EncodingUtils.getString(noteStr.getBytes(),
										"UTF-8") + "</body> \n </html>";
						tvContent.loadData(strUrl, "text/html; charset=UTF-8",
								null);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (msg.what == 6) {
					Toast.makeText(ShoolJS.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(ShoolJS.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "1");
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				String result = Tools.sendGet(Constant.synopsis, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						noteStr = jsonNode.get("data").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}
}
