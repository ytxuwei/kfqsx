package com.example.bean;

import java.util.ArrayList;

public class BlogDetialJsonBean {
	public String code;
	public Data data;
	
	public class Data{
		public String role;
		public String space_name;
		public String note;
		public String id;
		public ArrayList<DataImage> data_image;
		public ArrayList<DataComment> data_comment;
		
		
		public class DataImage{
			public int image_id;
			public String image_url;
			public String thumbnail;
		}
		public class DataComment{
			public String comment_note;
			public String comment_id;
			public String comment_date;
			public String comment_portrait;
			public String comment_user;
			
		}
	}
}
