package com.example.space;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.bean.BlogCommentBean;
import com.example.bean.BlogContentImageBean;
import com.example.bean.BlogDetialJsonBean;
import com.example.bean.BlogDetialJsonBean.Data;
import com.example.global.Constant;
import com.example.global.MyApplication;
import com.example.jhf.R;
import com.example.view.DropHtml;
import com.example.view.MyGridView;
import com.google.gson.Gson;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.SingleRequestQueue;
import com.imooc.baseadapter.utils.Tools;

/**
 * @author Administrator
 * 
 */
public class ClassBlogDetail extends Activity {
	private ListView lvDetail;
	private ImageView ivBack;
	private EditText etMsg;
	private WebView tvNote;
	private ArrayList<BlogContentImageBean> imageBean;
	private ArrayList<BlogCommentBean> commentBean;
	private MyAdapter adapter;
	// private TextView tvName;
	private MyGridView mGridView;
	private GridAdapter gridAdapter;
	private Button btnPost; // 发表评论
	private ObjectMapper om = new ObjectMapper();
	// private ArrayList<CommitPostBean> commitDatas;
	private String uid;
	private TextView tvGone; // 评论二字
	private View headerView;
	private NetworkImageView imageView;
	private RequestQueue mQueue;
	private ImageLoader imageLoader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.class_blog_detail);
		
		SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
		uid = sp.getString("uid", "0");

		mQueue = SingleRequestQueue.getRequestQueue(getApplicationContext());
		imageLoader = new ImageLoader(mQueue, new BitmapCache());
		MyApplication myApp = (MyApplication) getApplication();
		//imageLoader = myApp.imageLoader;

		imageBean = new ArrayList<BlogContentImageBean>();

		commentBean = new ArrayList<BlogCommentBean>();

		lvDetail = (ListView) findViewById(R.id.lv_blog_detail);
		headerView = View.inflate(getApplicationContext(), R.layout.class_blog_detail_header, null);
		tvNote = (WebView) headerView.findViewById(R.id.tv_class_blog_detail_note);
		tvGone = (TextView) headerView.findViewById(R.id.tv_gone_font);
		lvDetail.addHeaderView(headerView);
		btnPost = (Button) findViewById(R.id.btn_post_msg);
		getJsonText();
		// 图片
		mGridView = (MyGridView) headerView.findViewById(R.id.gv_blog);
		gridAdapter = new GridAdapter();
		mGridView.setAdapter(gridAdapter);
		etMsg = (EditText) findViewById(R.id.et_blg_leave_msg);
		etMsg.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				String contents = etMsg.getText().toString(); // 内容
				int len = contents.length(); // 内容长度
				int width = etMsg.getWidth(); // 控件宽度
				int height = etMsg.getHeight(); // 控件高度
				float px = etMsg.getTextSize();// 得到字体像素
				double length = Math.floor(width / px); // 能容纳字母个数

				if (len > length) {
					ViewGroup.LayoutParams lp = etMsg.getLayoutParams();
					int offset = 1; // 计算出需要行数
					if (len % length == 0) {
						offset = (int) (len / length);
					} else {
						offset = (int) (len / length) + 1;
					}

					// 需要的高度
					int need_h = (int) (px * offset) + (int) (9 * offset); // 前部分是行行高，后部分是行间距高度

					// Log.d(tag, "需要行数："+offset + ", 需要高度：" + need_h);

					lp.height = need_h;
					etMsg.setLayoutParams(lp);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub

			}
		});

		// adapter = new MyAdapter();
		ivBack = (ImageView) findViewById(R.id.shooll_GK);
		ivBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		btnPost.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String msg = etMsg.getText().toString();
				if (TextUtils.isEmpty(msg)) {
					Toast.makeText(getApplicationContext(), "亲，先输点内容吧", 0).show();
				} else {
					postToNet(msg);
				}
				InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(etMsg.getWindowToken(), 0);
				etMsg.setText(null);
				adapter.notifyDataSetChanged();
			}

		});

	}

	/**
	 * 发送留言
	 * 
	 * @param message
	 */
	protected void postToNet(final String message) {
		// TODO Auto-generated method stub
		final Handler handler2 = new Handler() {

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					Toast.makeText(getApplicationContext(), "评论成功！", 0).show();
					tvGone.setVisibility(View.VISIBLE);
					// 更新一条留言
					adapter.notifyDataSetChanged();
				} else if (msg.what == 6) {
					Toast.makeText(getApplicationContext(), "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(getApplicationContext(), "评论消失在二次元", 0).show();
				}
			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				commentBean.clear();
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // uid
				params.add(new BasicNameValuePair("note", message)); // 评论内容
				params.add(new BasicNameValuePair("space_id", getIntent().getStringExtra("spaceId")));
				String result = Tools.sendGet(Constant.postMsg, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						String code = jsonNode.get("code").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
							JSONObject object = new JSONObject(result);
							JSONObject objc = object.getJSONObject("data");
							JSONArray jsonArray = objc.getJSONArray("data_comment");
							for (int i = 0; i < jsonArray.length(); i++) {
								JSONObject obj = (JSONObject) jsonArray.opt(i);
								String id = obj.getString("comment_id");
								String user = obj.getString("comment_user");
								String portrait = obj.getString("comment_portrait");
								String date = obj.getString("comment_date");
								String note = obj.getString("comment_note");
								commentBean.add(new BlogCommentBean(note, id, date, portrait, user));
							}

						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler2.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler2.sendMessage(message);
				}
			};
		}.start();
	}

	/**
	 * listview的adapter
	 * 
	 * @author Administrator
	 * 
	 */
	class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return commentBean.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return commentBean.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			View view = View.inflate(getApplicationContext(), R.layout.class_blog_detail_item, null);
			// 评论内容的填充
			NetworkImageView img = (NetworkImageView) view.findViewById(R.id.iv_class_blog_detail_img);
			img.setImageUrl(commentBean.get(arg0).comment_portrait, imageLoader);
			TextView tvUser = (TextView) view.findViewById(R.id.tv_class_blog_detail_commit_user);
			tvUser.setText(commentBean.get(arg0).comment_user); // 用户名
			TextView tvDate = (TextView) view.findViewById(R.id.tv_class_blog_detail_commit_date);
			tvDate.setText(commentBean.get(arg0).comment_date);
			TextView tvNote = (TextView) view.findViewById(R.id.tv_class_blog_detail_commit_note);
			tvNote.setText(DropHtml.drop(commentBean.get(arg0).comment_note));

			return view;
		}

	}

	/**
	 * 获取数据
	 */
	private void getJsonText() {
		final Handler handler = new Handler() {

			public void handleMessage(Message msg) {
				String result = msg.obj.toString();

				Gson gson = new Gson();
				BlogDetialJsonBean json = gson.fromJson(result, BlogDetialJsonBean.class);
				Data data = json.data;
				// tvNote.setText(Html.fromHtml(DropHtml.drop(data.note)));
				tvNote.getSettings().setDefaultTextEncodingName("UTF-8");
				tvNote.loadData(data.note, "text/html;charset=UTF-8", null);
				ArrayList<com.example.bean.BlogDetialJsonBean.Data.DataImage> dataImage = data.data_image;
				for (int j = 0; j < dataImage.size(); j++) {
					int imageId = dataImage.get(j).image_id;
					imageBean.add(new BlogContentImageBean(imageId, dataImage.get(j).image_url, dataImage.get(j).thumbnail));
					gridAdapter.notifyDataSetChanged();
				}
				ArrayList<com.example.bean.BlogDetialJsonBean.Data.DataComment> dataComment = data.data_comment;
				for (int k = 0; k < dataComment.size(); k++) {
					String commentNote = dataComment.get(k).comment_note;
					String commentId = dataComment.get(k).comment_id;
					String commentDate = dataComment.get(k).comment_date;
					String commentPortrait = dataComment.get(k).comment_portrait;
					String commentUser = dataComment.get(k).comment_user;
					commentBean.add(new BlogCommentBean(commentNote, commentId, commentDate, commentPortrait, commentUser));
				}
				if (commentBean.size() != 0) {
					tvGone.setVisibility(View.VISIBLE);
				}

				adapter = new MyAdapter();
				lvDetail.setAdapter(adapter);

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // uid
				params.add(new BasicNameValuePair("space_id", getIntent().getStringExtra("spaceId")));
				String result = Tools.sendPost(Constant.spaceMsg, params);
				Message message = new Message();
				message.what = 7;
				message.obj = result;
				handler.sendMessage(message);
			};
		}.start();
	}

	// GridView 的适配器
	class GridAdapter extends BaseAdapter {
		private int selectPic = -1;

		public void setNotifyDataChange(int id) {
			selectPic = id;
			super.notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return imageBean.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return imageBean.get(arg0).image;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				imageView = new NetworkImageView(ClassBlogDetail.this);
				WindowManager wm = (WindowManager) ClassBlogDetail.this.getSystemService(Context.WINDOW_SERVICE);
				int width = wm.getDefaultDisplay().getWidth() / 5+3;
				imageView.setLayoutParams(new GridView.LayoutParams(width, width));
				// 设置ImageView对象布局
				imageView.setAdjustViewBounds(false);// 设置边界对齐
				imageView.setScaleType(ImageView.ScaleType.FIT_XY);// 设置刻度的类型
				imageView.setPadding(2, 2, 2, 2);
				// 设置间距
			} else {
				imageView = (NetworkImageView) convertView;
			}
			imageView.setImageUrl(imageBean.get(position).mThumbnail, imageLoader);

			imageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					setViewPagerAndZoom(v, position);
				}
			});

			return imageView;
		}

	}

	public void setViewPagerAndZoom(View v, final int position) {

		// 得到要放大展示的视图界面
		ViewPager expandedView = (ViewPager) (findViewById(R.id.detail_view));
		// 最外层的容器，用来计算
		View containerView = (FrameLayout) (findViewById(R.id.container));
		// 实现放大缩小类，传入当前的容器和要放大展示的对象
		final ZoomTutorial mZoomTutorial = new ZoomTutorial(containerView, expandedView);
		ViewPagerAdapter adapter = new ViewPagerAdapter(ClassBlogDetail.this, imageBean, mZoomTutorial,imageLoader);
		expandedView.setAdapter(adapter);
		expandedView.setCurrentItem(position);
		// 通过传入Id来从小图片扩展到大图，开始执行动画
		mZoomTutorial.zoomImageFromThumb(v);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mQueue.cancelAll(this);
		mQueue.getCache().clear();
	}
}
