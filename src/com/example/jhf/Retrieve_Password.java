package com.example.jhf;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

public class Retrieve_Password extends Activity {
	private Button mbtuButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.retrievepassword);
		mbtuButton=(Button) findViewById(R.id.mod1_next);
		mbtuButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
			
				Intent intent=new Intent();
				intent.setClass(getApplicationContext(), Retrieve_Password2.class);
				startActivity(intent);
			}
		});
	}
}
