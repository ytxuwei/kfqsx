package com.example.index;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.example.bean.NoticeBean;
import com.example.global.Constant;
import com.example.jhf.R;
import com.imooc.baseadapter.utils.Tools;
/**
 * 学校风貌
 * @author Administrator
 *
 */
public class ShoolFM extends Activity {
	
	private VideoView mVideo;
	private Button mButton;
	private ImageButton iButton;
	private ImageView shooll_GK;
	private String uid;
	private String jsonUrl;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.shoolff);
		SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
		uid = sp.getString("uid", "0");
		getJsonFromNet();
		mVideo = (VideoView) findViewById(R.id.video);
		shooll_GK=(ImageView) findViewById(R.id.shooll_GK);
		shooll_GK.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ShoolFM.this.finish();
			}
		});
		
		iButton = (ImageButton) findViewById(R.id.play);
		iButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mVideo.start(); 
				iButton.setVisibility(View.GONE);
			}
		});
		
	}
	private void getJsonFromNet() {
		// TODO Auto-generated method stub
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;
			

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						JSONObject jsonObject = new JSONObject(result);
						JSONObject jsonData = jsonObject.getJSONObject("data");
						jsonUrl = jsonData.getString("url");
						setVideoUrl();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				String result = Tools.sendGet(Constant.schoolStyle, params);
				Message message = new Message();
				message.what = 7;
				message.obj = result;
				handler.sendMessage(message);
			};
		}.start();
	}
	protected void setVideoUrl() {
		// TODO Auto-generated method stub
		Uri uri = Uri.parse(jsonUrl);
		mVideo.setMediaController(new MediaController(this));  
		mVideo.setVideoURI(uri);  
		mVideo.requestFocus();  
	}
}
