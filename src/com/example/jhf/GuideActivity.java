package com.example.jhf;

import java.util.ArrayList;

import com.imooc.baseadapter.utils.PrefUtils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

public class GuideActivity extends Activity implements OnPageChangeListener{
	private ViewPager mViewPager;
	private int[] mImageIds = new int[] { R.drawable.guide_1,
			R.drawable.guide_2, R.drawable.guide_3,R.drawable.guide_4};
	private ArrayList<ImageView> mImageViews;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 去掉标题
		setContentView(R.layout.activity_guide);
		
		initView();
		initData();
	}
	/**
	 * 初始化布局
	 */
	private void initView() {
		mViewPager = (ViewPager) findViewById(R.id.vp_guide);

	}

	/**
	 * 初始化数据
	 */
	private void initData() {
		// 初始化3张引导图片数据
		mImageViews = new ArrayList<ImageView>();
		for (int i = 0; i < mImageIds.length; i++) {
			ImageView image = new ImageView(this);
			image.setBackgroundResource(mImageIds[i]);
			mImageViews.add(image);
		}
		
		GuideAdapter adapter = new GuideAdapter();
		mViewPager.setAdapter(adapter);// viewpager设置数据

		mViewPager.setOnPageChangeListener(this);// 设置滑动监听

	}

	class GuideAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return mImageViews.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		// 初始化界面数据,类似getView
		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			ImageView imageView = mImageViews.get(position);
			imageView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					switch (position) {
					case 3:
						PrefUtils.putBoolean(GuideActivity.this, "is_guide_show", true);// 表示新手引导已经展示过了,下次不再展示
						// 跳到主页面
						Intent intent = new Intent(GuideActivity.this,MainActivity.class);
						startActivity(intent);
						finish();
						break;

					default:
						break;
					}
				}
			});
			container.addView(imageView);
			return imageView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}


	}

	// 滑动事件
	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {
	}

	// 选中事件
	@Override
	public void onPageSelected(int position) {
		System.out.println("选中:" + position);
	}

	// 状态
	@Override
	public void onPageScrollStateChanged(int state) {

	}
}
