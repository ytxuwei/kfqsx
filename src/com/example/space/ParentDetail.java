package com.example.space;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.bean.ParentListBean;
import com.example.global.Constant;
import com.example.jhf.R;
import com.example.space.ParentList.MyAdapter;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.BitmapUtils;
import com.imooc.baseadapter.utils.SingleRequestQueue;
import com.imooc.baseadapter.utils.Tools;

public class ParentDetail extends Activity implements OnClickListener {
	private ImageView ivBack;
	private TextView tvPhone;
	private TextView tvParentName;
	private TextView tvParentRole;
	private TextView tvStudentName;
	private TextView tvStudenGender;
	private TextView tvStudenClass;
	private NetworkImageView img;
	private RequestQueue mQueue;
	private ImageLoader imageLoader;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.parent_detail);
		mQueue = SingleRequestQueue.getRequestQueue(getApplicationContext());
		imageLoader = new ImageLoader(mQueue, new BitmapCache());
		ivBack = (ImageView) findViewById(R.id.iv_back);
		ivBack.setOnClickListener(this);

		tvPhone = (TextView) findViewById(R.id.tv_parent_phone);
		tvPhone.setOnClickListener(this);

		tvParentName = (TextView) findViewById(R.id.tv_parent_detail_name);
		tvParentRole = (TextView) findViewById(R.id.tv_parent_detail_role);
		tvStudentName = (TextView) findViewById(R.id.tv_parent_detail_student_name);
		tvStudenGender = (TextView) findViewById(R.id.tv_parent_detail_gender);
		tvStudenClass = (TextView) findViewById(R.id.tv_parent_detail_student_class);
		img = (NetworkImageView) findViewById(R.id.iv_parent_detail_img);

		getJsonText();
	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.iv_back:
			finish();
			break;
		case R.id.tv_parent_phone:
			String mobile = tvPhone.getText().toString();
			Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mobile));
			startActivity(intent);
		default:
			break;
		}

	}

	private void getJsonText() {
		final Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				String result = msg.obj.toString();
				try {
					JSONObject jsonObject = new JSONObject(result);
					JSONObject jsonObject2 = jsonObject.getJSONObject("data");

					String parentRole = jsonObject2.getString("parent_role");
					int parentId = jsonObject2.getInt("parent_id");
					String parentName = jsonObject2.getString("parent_name");
					String studentClass = jsonObject2.getString("student_class");
					String parentImg = jsonObject2.getString("portrait");
					String studentName = jsonObject2.getString("student_name");
					String parentPhone = jsonObject2.getString("parent_phone");
					System.out.println("电话"+parentPhone);
					String studentGender = jsonObject2.getString("student_gender");

					tvParentRole.setText(parentRole);
					tvParentName.setText(parentName);
					tvStudenClass.setText(studentClass);
					//img.setImageBitmap(parentImg);
					img.setImageUrl(parentImg, imageLoader);
					tvStudentName.setText(studentName);
					tvPhone.setText(parentPhone + "");
					tvStudenGender.setText(studentGender);
					

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
				String uid = sp.getString("uid", "0");

				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // uid
																// getIntent().getIntExtra("classId",
																// 0)+""
				params.add(new BasicNameValuePair("line_id", getIntent().getIntExtra("lineId", 1) + "")); // 这里获取class_id有问题
				String result = Tools.sendGet(Constant.parentMsg, params);
				Message message = new Message();
				message.what = 7;
				message.obj = result;
				handler.sendMessage(message);
			};
		}.start();
	}
}
