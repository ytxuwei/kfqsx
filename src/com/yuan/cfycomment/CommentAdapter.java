package com.yuan.cfycomment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.example.jhf.R;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.SingleRequestQueue;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 濞戞搫鎷风紒鐙欏棛妲戦悹浣哄劋閻楀崬顕ｉ敓锟�
 * 
 * @author Administrator
 * 
 */
public class CommentAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater inflater;
	private ViewHolder viewHolder;
	private ArrayList<HashMap<String, String>> list_comment; // 濞戞搫鎷风紒鐙欏棛妲戦悹浣哄劋閺嗙喖骞戦敓锟�
	private ImageLoader imageLoader;
	private RequestQueue mQueue;

	public CommentAdapter(Context context,
			ArrayList<HashMap<String, String>> list_comment,
			List<List<HashMap<String, String>>> list_comment_child,
			OnClickListener myOnitemcListener) {
		this.inflater = LayoutInflater.from(context);
		this.list_comment=list_comment;
//		this.list_comment = new ArrayList<HashMap<String, String>>();
//		this.list_comment.addAll(list_comment);
		this.context=context;
		mQueue = SingleRequestQueue.getRequestQueue(context);
		imageLoader = new ImageLoader(mQueue, new BitmapCache());
	}

	public void clearList() {
		this.list_comment.clear();// 缂佸顭峰▍搴㈡交濞嗗酣鍤嬮柛鎺擃殙閵嗗啯绋夐鐐插笚缂佽京濯寸槐婵堟媼閳猴拷缁剚绋夐搹鍏夋晞
	}

	/**
	 * 濞ｅ浂鍠楅弫鑲╂嫚閸曨噮鍟�
	 * 
	 * @param list_comment
	 * @param list_comment_child
	 */
	public void updateList(List<HashMap<String, String>> list_comment,
			List<List<HashMap<String, String>>> list_comment_child) {
		this.list_comment.addAll(list_comment);
	}

	@Override
	public int getCount() {
		return list_comment.size();
	}

	@Override
	public Object getItem(int position) {
		return list_comment.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			
			convertView = inflater.inflate(R.layout.item_comment, null);
			viewHolder = new ViewHolder();
			viewHolder.linearLayout = (LinearLayout) convertView
					.findViewById(R.id.school_linearyout);
			viewHolder.iv_user_photo = (NetworkImageView) convertView
					.findViewById(R.id.iv_user_photo);
			viewHolder.tv_user_name = (TextView) convertView
					.findViewById(R.id.tv_user_name);

			viewHolder.tv_user_comment = (TextView) convertView
					.findViewById(R.id.tv_user_comment);
			viewHolder.tv_user_tiem_s = (TextView) convertView
					.findViewById(R.id.tv_user_tiem_s);
			viewHolder.tv_userHome_name = (TextView) convertView
					.findViewById(R.id.tv_userHome_name);
			viewHolder.tv_answer_school = (TextView) convertView
					.findViewById(R.id.tv_user_reply);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		String strhome = list_comment.get(position).get("content").toString();
		String strschool = list_comment.get(position).get("schoolanswer");
		String studentportrait = list_comment.get(position).get(
				"studentportrait");
		// stringtoBitmap(studentportrait);
//		viewHolder.iv_user_photo
//				.setImageBitmap(stringtoBitmap(studentportrait));
		viewHolder.iv_user_photo.setImageUrl(
				list_comment.get(position).get("studentportrait"), imageLoader);// 杞寲涓哄浘鐗�
		// 婵繐绲介崹顖滄偘閵娿劍褰х�殿喖绻愰獮鎾诲箳婵夌櫝ml<p></P>
		String regEx = "</?[p|P][^>]*>";
		if (strschool != null) {
			Pattern pa = Pattern.compile(regEx);
			Matcher matcher = pa.matcher(strschool);
			String mschool = matcher.replaceAll("").trim();
			viewHolder.tv_answer_school.setText(mschool);
		} else {

		}
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(strhome);
		String rue = m.replaceAll("").trim();
		viewHolder.tv_user_name.setText(list_comment.get(position)
				.get("user").toString());
		viewHolder.tv_user_tiem_s.setText(list_comment.get(position)
				.get("TiemIssueStr").toString());
		viewHolder.tv_userHome_name.setText(list_comment.get(position)
				.get("homename").toString());
		viewHolder.tv_user_comment.setText(rue);

		if (list_comment.get(position).get("schoolanswer") != null) {
			viewHolder.linearLayout.setVisibility(View.VISIBLE);
		} else {
			viewHolder.linearLayout.setVisibility(View.GONE);
		}
		return convertView;
	}

	public class ViewHolder {
		private NetworkImageView iv_user_photo; // 闁告繀娴囬鎴︽嚀閿燂拷 濠㈣埖娼欓崕锟�
		private TextView tv_user_name; // 闁告繀娴囬鎴︽嚀閿燂拷 闁哄嫮鏁歌ⅷ
		private TextView tv_user_comment; // 闁告繀娴囬鎴︽嚀閿燂拷 濞戞搫鎷风紒鐙欏啯鎯傞悹浣告惈閸炲锟界櫢鎷�
		private TextView tv_user_tiem_s;// 闁告瑦鍨电粩鐑藉籍閸洘锛�
		private TextView tv_userHome_name;// 閻庣懓鐖奸弳閬嶅触瀹ュ懐鎽�
		private TextView tv_answer_school;// 閻庢冻闄勯悧搴ㄥ炊閻愮數鎽�
		private LinearLayout linearLayout;
	}
}
