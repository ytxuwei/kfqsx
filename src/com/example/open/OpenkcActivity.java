package com.example.open;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bean.CourseBean;
import com.example.global.Constant;
import com.example.jhf.MyContentDetailActivity;
import com.example.jhf.R;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.imooc.baseadapter.utils.Tools;

/**
 * 课程
 * 
 * @author Administrator
 * 
 */
public class OpenkcActivity extends Activity implements OnClickListener,
		OnItemClickListener {
	private ImageView shooll_GK;
	private TextView tvTitle;
	private GridView mGridView; // 课程列表
	private MyAdapter adapter;
	private ArrayList<CourseBean> datas;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.openkc);

		datas = new ArrayList<CourseBean>();
		initView();
		getJsonText();
		shooll_GK.setOnClickListener(this);
		mGridView.setOnItemClickListener(this);
		
	}

	private void initView() {
		tvTitle = (TextView) findViewById(R.id.tv_openkc_title);
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		mGridView = (GridView) findViewById(R.id.gv_openkc);

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.shooll_GK:
			finish();
			break;

		default:
			break;
		}
		/*if(arg0.getId()!=R.id.shooll_GK){
			Intent intent = new Intent(OpenkcActivity.this,
					MyContentDetailActivity.class);
			intent.putExtra("title", ((Button) arg0).getText());
			startActivity(intent);
		}*/
	}

	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private String contentName;// 每条item的标题
	private ObjectMapper om = new ObjectMapper();
	private String code;
	private int contentId;
	private String result;
	private ImageView ivBack;

	private void getJsonText() {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						jsonObject = new JSONObject(result);
						jsonArray = jsonObject.getJSONArray("data");
						for (int i = 0; i < jsonArray.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							contentId = (Integer) jsonObject2.get("id");
							contentName = (String) jsonObject2.get("name");
							datas.add(new CourseBean(contentId, contentName));
						}

						adapter = new MyAdapter();

						mGridView.setAdapter(adapter);

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else if (msg.what == 6) {
					Toast.makeText(OpenkcActivity.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(OpenkcActivity.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid",uid)); // 密码
				result = Tools.sendGet(Constant.course, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

	class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return datas.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return datas.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			View view = View.inflate(getApplicationContext(),
					R.layout.item_grid_open, null);
			TextView btn = (TextView) view
					.findViewById(R.id.btn_item_grid_open);
			btn.setText(datas.get(arg0).name);
			return view;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2,
			long arg3) {
			Intent intent = new Intent(OpenkcActivity.this,
					CourseItemActivity.class);
			intent.putExtra("datas", datas.get(arg2).id);
			startActivity(intent);
		}
				

}
