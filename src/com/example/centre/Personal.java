package com.example.centre;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.global.Constant;
import com.example.jhf.R;
import com.example.notice.Notices;
import com.example.open.OpenksActivityAdapter;
import com.example.open.OpenqmcsActivity;
import com.fasterxml.jackson.databind.JsonNode;
import com.imooc.baseadapter.utils.Tools;

/**
 * 个人资料
 * 
 * @author Administrator
 * 
 */
public class Personal extends Activity implements OnClickListener {
	private ImageView shooll_GK;
	private LinearLayout student1;
	private LinearLayout student2;
	private TextView tvName1;
	private TextView tvName2;
	private TextView tvPhone; // 电话
	private TextView tvParent;
	private TextView tvGendar;
	private Bundle bundle;
	private Bundle bundle2;
	private ImageView ivLine;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.personal);

		bundle = new Bundle();
		bundle2 = new Bundle();
		getJsonText();
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		shooll_GK.setOnClickListener(this);
		student1 = (LinearLayout) findViewById(R.id.ll_personal_student1);
		student2 = (LinearLayout) findViewById(R.id.ll_personal_student2);
		student1.setOnClickListener(this);
		student2.setOnClickListener(this);
		tvName1 = (TextView) findViewById(R.id.tv_student_name1);
		tvName2 = (TextView) findViewById(R.id.tv_student_name2);
		tvPhone = (TextView) findViewById(R.id.tv_personal_phone);
		tvParent = (TextView) findViewById(R.id.tv_personal_parent_name);
		tvGendar = (TextView) findViewById(R.id.tv_personal_gender);
		ivLine = (ImageView) findViewById(R.id.iv_line_of_last);

		// 验证权限是否验证成功，未验证的不能显示学生,老师也不能显示学生
		SharedPreferences sharedPreferences = getSharedPreferences("user", 0);
		String isConfirm = sharedPreferences.getString("isConfirm", "false");
		SharedPreferences confirm = getSharedPreferences("confirm", 0);
		String ifLoadConfirm = confirm.getString("isConfirm", "false");
		SharedPreferences spChmod = getSharedPreferences("chmod", MODE_PRIVATE);
		
		if (isConfirm.equals("true")||ifLoadConfirm.equals("true")) {
			student1.setVisibility(View.VISIBLE);
		}
		if(spChmod.getBoolean("isDirector", false)||spChmod.getBoolean("isOffice", false)||spChmod.getBoolean("isPresident", false)||spChmod.getBoolean("isTeacher", false)){
			student1.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.shooll_GK:
			finish();
			break;
		case R.id.ll_personal_student1:
			Intent intent1 = new Intent(Personal.this, PersonalStudentDetail.class);
			intent1.putExtra("studentName", tvName1.getText());
			intent1.putExtra("studentClass", bundle.getString("class"));
			intent1.putExtra("studentGender", bundle.getString("gender"));
			startActivity(intent1);
			break;
		case R.id.ll_personal_student2:
			Intent intent2 = new Intent(Personal.this, PersonalStudentDetail.class);
			intent2.putExtra("studentName", tvName2.getText());
			intent2.putExtra("studentGender", false);
			startActivity(intent2);
			break;
		default:
			break;
		}
	}

	private void getJsonText() {
		final Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				String result = msg.obj.toString();
				try {
					JSONObject jsonObject = new JSONObject(result);
					JSONObject objData = jsonObject.getJSONObject("data");
					String mPhone = objData.getString("phone");
					String mName = objData.getString("name");
					String mGender = objData.getString("gender");
					String mClass = objData.getString("class");
					JSONArray jsonArray = objData.getJSONArray("student_list");
					if (jsonArray.length() > 1) {
						student2.setVisibility(View.VISIBLE);
						ivLine.setVisibility(View.VISIBLE);
						
					}
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject student = (JSONObject) jsonArray.opt(i);
						String mStudentId = student.getString("student_id");
						String mStudentClass = student.getString("student_class");
						String mStudentGender = student.getString("student_gender");
						String mStudentName = student.getString("student_name");
						if (i == 0) {
							tvName1.setText(mStudentName);
							bundle.putString("id", mStudentId);
							bundle.putString("class", mStudentClass);
							bundle.putString("gender", mStudentGender);
						} else if (i == 1) {
							tvName2.setText(mStudentName);
							bundle2.putString("id", mStudentId);
							bundle2.putString("class", mStudentClass);
							bundle2.putString("gender", mStudentGender);
						}

					}
					tvPhone.setText(mPhone);
					tvParent.setText(mName);
					tvGendar.setText(mGender);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				String result = Tools.sendGet(Constant.getProfile, params);
				Message message = new Message();
				message.obj = result;
				handler.sendMessage(message);
			};
		}.start();
	}
}
