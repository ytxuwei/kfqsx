package com.example.space;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.day.event.ItemEveryYearEvent;
import com.example.global.Constant;
import com.example.index.RefreshableView;
import com.example.index.RefreshableView.PullToRefreshListener;
import com.example.jhf.R;
import com.example.monthstar.OneDatyPerson;
import com.example.monthstar.OneDay;
import com.example.monthstar.OneDayAdper;
import com.example.studentwork.StudentWorkadapter;
import com.example.view.ListViewIndicatorUtil;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.imooc.baseadapter.utils.Tools;

import android.R.integer;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

/**
 * 创建日期: 2015/9/18 创建者： 刘永华 功能说明：学生作业，用于每个班级的作业 修改履历： VER 修改日2015/10/26 修改者liu
 * 修改内容／对也面布局有了新的要求理由对界面有了新的要求
 * ──────────────────────────────────────────────────────────────
 */
public class ClassHomeWork extends Activity {
	private ImageView shooll_GK;
	private ArrayList<OneDatyPerson> data;
	private SchoolWorkAdapter shoolDAdater;
	private ObjectMapper om = new ObjectMapper();
	private String onedayconten;// 每条item的标题
	private String worktiem;// 每条item的标题
	private String code;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private int i = 2;// 从大二页开始加载更多
	private PullToRefreshListView lvAnswer;// 加载更多
	private int p = 0;// 取回的页数

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.classhomework);
		data = new ArrayList<OneDatyPerson>();
		getJsonText(1);
		init();
		back();

	}

	private void init() {
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		lvAnswer = (PullToRefreshListView) findViewById(R.id.list);
		View headerView = View.inflate(getApplicationContext(), R.layout.header_shoolfm, null);
		ListView refreshableView = lvAnswer.getRefreshableView();
		refreshableView.addHeaderView(headerView);
		shoolDAdater = new SchoolWorkAdapter(ClassHomeWork.this, data);
		// 将添加的每条item放到Lstview中
		lvAnswer.setAdapter(shoolDAdater);
		ListViewIndicatorUtil.initIndicator(lvAnswer);// 下拉改变字体
		// 刷新
		lvAnswer.setOnRefreshListener(new com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						data.clear();
						getJsonText(1);
						lvAnswer.onRefreshComplete();
					}
				}, 3000);
			}

			// 加载更多
			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullUpToRefresh");
				// 这里写上拉加载更多的任务
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						getJsonText(i);
						i++;
						lvAnswer.onRefreshComplete();
					}
				}, 3000);

			}

		});
	}

	/**
	 * 返回按钮
	 */

	private void back() {
		shooll_GK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ClassHomeWork.this.finish();
			}
		});
	}

	/**
	 * 获取学校简介的JSON数据
	 */
	@SuppressLint("HandlerLeak")
	private void getJsonText(final int i) {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						jsonObject = new JSONObject(result);
						jsonArray = jsonObject.getJSONArray("data");
						p = jsonObject.getInt("page_count");
						for (int i = 0; i < jsonArray.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							onedayconten = (String) jsonObject2.get("title");
							worktiem = (String) jsonObject2.get("date");
							OneDatyPerson shoolLITools = new OneDatyPerson();
							shoolLITools.setStrriqi(onedayconten);
							shoolLITools.setStudentytiem(worktiem);
							data.add(shoolLITools);
						}

						shoolDAdater.notifyDataSetChanged();
						lvAnswer.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
								try {
									Intent intent = new Intent(ClassHomeWork.this, StudentWorkParticular.class);
									JSONObject jsonObject3 = (JSONObject) jsonArray.opt(arg2 - 2);
									String title = jsonObject3.get("title").toString();
									String start_id = jsonObject3.get("id").toString();
									intent.putExtra("id_studentwork", start_id);
									intent.putExtra("title_HomeWork", title);
									Log.v("OneDay", "start_id" + start_id);
									startActivity(intent);
								} catch (Exception e) {
									// TODO: handle exceptions
								}

							}
						});
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (msg.what == 6) {
					Toast.makeText(ClassHomeWork.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(ClassHomeWork.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("page", i + ""));// 当前页数
				params.add(new BasicNameValuePair("page_count", p + ""));
				String result = Tools.sendGet(Constant.homework, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}
}
