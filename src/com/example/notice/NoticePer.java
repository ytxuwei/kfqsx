package com.example.notice;

public class NoticePer {
	private String read;
	private String content;
	private String read_unmber;
	private String unmber;
	private String open;

	public String getRead() {
		return read;
	}

	public void setRead(String read) {
		this.read = read;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRead_unmber() {
		return read_unmber;
	}

	public void setRead_unmber(String read_unmber) {
		this.read_unmber = read_unmber;
	}

	public String getUnmber() {
		return unmber;
	}

	public void setUnmber(String unmber) {
		this.unmber = unmber;
	}

	public String getOpen() {
		return open;
	}

	public void setOpen(String open) {
		this.open = open;
	}

	@Override
	public String toString() {
		return "NoticePer [read=" + read + ", content=" + content
				+ ", read_unmber=" + read_unmber + ", unmber=" + unmber
				+ ", open=" + open + "]";
	}
	
}