package com.yuan.cfycomment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.global.Constant;
import com.example.jhf.R;
import com.example.view.ListViewIndicatorUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.imooc.baseadapter.utils.Tools;

/**
 * 创建日期: 2015/10/26 创建者： 刘永华 功能说明：家长留言界面，用于记录家长留言和学校方面的返回 修改履历： VER 修改日 修改者
 * 修改内容／理由 ──────────────────────────────────────────────────────────────
 */
public class Leave_word extends Activity implements OnClickListener {
	// private ListView lv_user_comments;
	private TextView btn_comment;
	private CommentAdapter commentAdapter;
	private ArrayList<HashMap<String, String>> datas; // 一级评论数据
	private List<List<HashMap<String, String>>> list_comment_child; // 二级评论数据
	public static final int REQUSET = 1;
	private ObjectMapper om = new ObjectMapper();
	private String problemConten;// 提出问题
	private String date;// 日期
	private String className;// 提出问题
	private String studentName;// 学生姓名
	private String studentportrait;// 头像
	private String code;
	private TextView tV;
	private JSONArray jsonArray;// 关于家长问题
	private JSONObject jsonObject;
	private ImageView ivBack;
	private PullToRefreshListView mPullRefreshListView;
	private int page = 2; // 加载更多第几页
	private int pageCount; // 请求总页数

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.lymain);
		ivBack = (ImageView) findViewById(R.id.shooll_GK);
		tV = (TextView) findViewById(R.id.btn_main_send);
		ivBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Leave_word.this.finish();
			}
		});

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("action.refreshLeaveMessage");
		registerReceiver(mRefreshBroadcastReceiver, intentFilter);
		// judgud();
		// initList();

		datas = new ArrayList<HashMap<String, String>>();
		map = new HashMap<String, String>();
		list_comment_child = new ArrayList<List<HashMap<String, String>>>();
		getJsonText(1);
		initView();
		
		commentAdapter = new CommentAdapter(Leave_word.this, datas, list_comment_child, myOnitemcListener);
		mPullRefreshListView.setAdapter(commentAdapter);
	}


	/**
	 * 实例化工具
	 */
	private void initView() {
		// TODO Auto-generated method stub
		mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.leave_message_list);
		mPullRefreshListView.setMode(Mode.BOTH);
		ListViewIndicatorUtil.initIndicator(mPullRefreshListView);
		btn_comment = (TextView) this.findViewById(R.id.btn_main_send);// 我要提问
		btn_comment.setOnClickListener(this);

		mPullRefreshListView.setOnRefreshListener(new com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullDownToRefresh");

				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						datas.clear();
						getJsonText(1);
						page=2;
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullUpToRefresh");
				// 这里写上拉加载更多的任务

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						getJsonText(page);
						page++;
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);

			}

		});

	}

	private OnClickListener myOnitemcListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int position = (Integer) v.getTag();
			showDialog(position);

		}
	};
	private HashMap<String, String> map;

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_main_send:
			Intent intent = new Intent(Leave_word.this, te.class);
			startActivity(intent);
			//finish();
			break;

		default:
			break;
		}
	}

	/**
	 * 获取学校简介的JSON数据
	 */
	@SuppressLint("HandlerLeak")
	private void getJsonText(final int i) {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;
			private String schoolanswer;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						//ArrayList<HashMap<String, String>> datas =new ArrayList<HashMap<String,String>>();
						
						jsonObject = new JSONObject(result);
						pageCount = jsonObject.getInt("page_count");
						jsonArray = jsonObject.getJSONArray("data");// 关于家长数据
						for (int i = 0; i < jsonArray.length(); i++) {
							HashMap<String, String> map=new HashMap<String, String>();
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							String problemConten = (String) jsonObject2.get("note");
							String date = (String) jsonObject2.get("date");
							String studentportrait = jsonObject2.getString("user_image");// 图片头像
							// tiemDifferece(date);
							className = (String) jsonObject2.get("student_class");
							studentName = jsonObject2.getString("student_name");
							String userOfSend = jsonObject2.getString("user");
							JSONArray jsonArray = (JSONArray) jsonObject2.get("comment");
							for (int j = 0; j < jsonArray.length(); j++) {
								JSONObject object = (JSONObject) jsonArray.opt(j);
								schoolanswer = (String) object.get("comment");
							}

							map.put("content", problemConten); // 内容
							map.put("classNameStr", className); // 班级
							map.put("TiemIssueStr", date); // 发表日期
							map.put("user", userOfSend);
							map.put("homename", studentName + "的家长"); // 学生姓名
							map.put("schoolanswer", schoolanswer); // 评论
							map.put("studentportrait", studentportrait); // 头像
							datas.add(map);
							list_comment_child.add(new ArrayList<HashMap<String, String>>());
						}
						
						commentAdapter.notifyDataSetChanged();

					} catch (JSONException e) {
						e.printStackTrace();
					}

				} else if (msg.what == 6) {
					Toast.makeText(Leave_word.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(Leave_word.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("page", i + ""));
				params.add(new BasicNameValuePair("page_count", pageCount + ""));
				String result = Tools.sendGet(Constant.leaveMessage, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

	/**
	 * 计算时间
	 * 
	 * @param tiem
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	private String tiemDifferece(String tiem) {
		String strtiem = null;
		StringBuffer sb = null;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date curDate = new Date(System.currentTimeMillis());// 获取当前时间
		String str = formatter.format(curDate);
		try {
			Date d1 = df.parse(str);
			Date d2 = df.parse(tiem);
			long diff = d1.getTime() - d2.getTime();// 这样得到的差值是微秒级别

			long days = diff / (1000 * 60 * 60 * 24);
			long hours = (diff - days * (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
			long minutes = (diff - days * (1000 * 60 * 60 * 24) - hours * (1000 * 60 * 60)) / (1000 * 60);
			sb = new StringBuffer();
			// sb.append("发表于：");
			if (days > 0) {
				sb.append(days + "天前");
			} else if (hours > 0) {
				sb.append(hours + "小时前");
			} else if (minutes > 0) {
				sb.append(minutes + "分钟前");
			} else {
				sb.append("刚刚");
			}
			strtiem = "" + days + "天" + hours + "小时" + minutes + "分前";
			// System.out.println("" + days + "天" + hours + "小时" + minutes +
			// "分");
			// textView.setText(string + "当然前系统时间" + str);
		} catch (Exception e) {
		}
		return sb.toString();
	}

	/*
	 * @Override protected void onActivityResult(int requestCode, int
	 * resultCode, Intent data) { // TODO Auto-generated method stub
	 * super.onActivityResult(requestCode, resultCode, data);
	 * 
	 * 
	 * map.put("content", data.getStringExtra("heheString"));
	 * map.put("classNameStr", data.getStringExtra("student_class_answer"));
	 * //班级 map.put("TiemIssueStr", data.getStringExtra("date_answer")); //发表日期
	 * map.put("homename", data.getStringExtra("student_name_answer") + "的家长");
	 * //学生姓名 map.put("schoolanswer", null); //评论 map.put("studentportrait",
	 * data.getStringExtra("student_image_answer")); //头像 datas.add(map);
	 * commentAdapter.notifyDataSetChanged(); }
	 */
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(mRefreshBroadcastReceiver);
	}

	private BroadcastReceiver mRefreshBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals("action.refreshLeaveMessage")) {
				// reFreshFrinedList();
				// getJsonText(1);
				map.put("content", intent.getStringExtra("heheString"));
				//map.put("classNameStr", intent.getStringExtra("student_class_answer")); // 班级
				map.put("TiemIssueStr", intent.getStringExtra("date_answer")); // 发表日期
				//map.put("homename", intent.getStringExtra("student_name_answer") + "的家长"); // 学生姓名
				map.put("user", intent.getStringExtra("user"));
				map.put("schoolanswer", null); // 评论
				map.put("studentportrait", intent.getStringExtra("student_image_answer")); // 头像
				//datas.add(map);
				//commentAdapter.notifyDataSetChanged();
			}
		}
	};

}
