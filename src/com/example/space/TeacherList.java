package com.example.space;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.example.bean.TeacherListBean;
import com.example.global.Constant;
import com.example.jhf.R;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.Tools;

public class TeacherList extends Activity implements OnItemClickListener,
		OnClickListener {
	private ListView mListView;
	private ArrayList<TeacherListBean> datas;
	private LinearLayout llInfo;
	private ImageView ivBack;
	private MyAdapter adapter;
	private RequestQueue mQueue;
	private ImageLoader imageLoader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.teacher_list);
		
		
		mQueue = Volley.newRequestQueue(TeacherList.this);
		imageLoader = new ImageLoader(mQueue, new BitmapCache()); 
		llInfo = (LinearLayout) findViewById(R.id.ll_teacher_list_info);
		/**
		 * 查看权限
		 */
		SharedPreferences chmod = getSharedPreferences("chmod", MODE_PRIVATE);
		boolean isPresident = chmod.getBoolean("isPresident", true);
		boolean isTeacher = chmod.getBoolean("isTeacher", true);
		boolean isOffice = chmod.getBoolean("isOffice", true);
		boolean isDirector = chmod.getBoolean("isDirector", true);
		if(isPresident||isTeacher||isOffice||isDirector){
			llInfo.setVisibility(View.VISIBLE);
		}
		
		
		mListView = (ListView) findViewById(R.id.lv_teacher_list);
		ivBack = (ImageView) findViewById(R.id.iv_back);
		ivBack.setOnClickListener(this);

		datas = new ArrayList<TeacherListBean>();
		getJsonText();
		adapter = new MyAdapter();
		mListView.setAdapter(adapter);

		llInfo.setOnClickListener(this);

		mListView.setOnItemClickListener(this);
	}

	class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return datas.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return datas.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stubiv_teacher_list
			View view = View.inflate(getApplicationContext(),
					R.layout.teacher_list_item, null);
			NetworkImageView image = (NetworkImageView) view
					.findViewById(R.id.iv_teacher_list);
			TextView tvName = (TextView) view
					.findViewById(R.id.tv_teacher_list_name);
			TextView tvSubject = (TextView) view
					.findViewById(R.id.tv_teacher_list_subject);
			TextView tvPost = (TextView) view
					.findViewById(R.id.tv_teacher_list_post);
			TextView tvPhone = (TextView) view
					.findViewById(R.id.tv_teacher_list_phone);
			//image.setImageBitmap(datas.get(arg0).img);
			image.setImageUrl(datas.get(arg0).img, imageLoader);
			
			tvName.setText(datas.get(arg0).name);
			tvSubject.setText(datas.get(arg0).subject);
			tvPost.setText(datas.get(arg0).post);
			tvPhone.setText(datas.get(arg0).phone);

			return view;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(TeacherList.this, TeacherDetail.class);
		intent.putExtra("teacherId", datas.get(arg2).id);
		startActivity(intent);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.iv_back:
			this.finish();
			break;
		case R.id.ll_teacher_list_info:
			Intent intent = new Intent(TeacherList.this, ParentList.class);
			intent.putExtra("classId", getIntent().getIntExtra("classId", 0));
			startActivity(intent);
			break;
		default:
			break;
		}
	}

	private void getJsonText() {

		final Handler handler = new Handler() {

			public void handleMessage(Message msg) {
				String result = msg.obj.toString();
				try {
					JSONObject jsonObject = new JSONObject(result);
					JSONArray jsonArray = jsonObject.getJSONArray("data");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject2 = (JSONObject) jsonArray.opt(i);
						String mSubject = (String) jsonObject2.get("course");
						String mPost = jsonObject2.getString("job");
						String mName = jsonObject2.getString("name");
						String mPhone = jsonObject2.getString("phone");
						String mImg = jsonObject2.getString("portrait");
						int mId = (Integer) jsonObject2.get("id");
						datas.add(new TeacherListBean(mImg, mName, mSubject,
								mPost, mPhone, mId));
						// datas.add(new CourseBean(contentId, contentName));
						// datas.add(new Notices(contentName, contentId));
					}
					adapter = new MyAdapter();
					// 将添加的每条item放到Lstview中
					mListView.setAdapter(adapter);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid",uid)); // uid
				params.add(new BasicNameValuePair("class_id", getIntent()
						.getIntExtra("classId", 0) + ""));
				String result = Tools
						.sendGet(Constant.memberLineString, params);
				Message message = new Message();
				message.what = 7;
				message.obj = result;
				handler.sendMessage(message);
			};
		}.start();
	}

	private Bitmap stringtoBitmap(String string) {
		// 将字符串转换成Bitmap类型
		Bitmap bitmap = null;
		try {
			byte[] bitmapArray;
			bitmapArray = Base64.decode(string, Base64.DEFAULT);
			bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0,
					bitmapArray.length);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return bitmap;
	}
}
