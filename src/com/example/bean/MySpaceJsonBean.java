package com.example.bean;

import java.util.ArrayList;

public class MySpaceJsonBean {
	public String code;
	public String space_user_portrait;	
	public String page_count;
	public String space_user_name;
	public String space_user_id;
	public ArrayList<SpaceData> data;
	
	public class SpaceData{
		public String space_date;
		public ArrayList<SpaceItemData> space_data;
		
		public class SpaceItemData{
			public String note;
			public String image_count;
			public String id;
			public ArrayList<SpaceImage> data_image;
			public class SpaceImage{
				public String image_id;
				public String image;
			}
			@Override
			public String toString() {
				return "SpaceItemData [note=" + note + ", image_count="
						+ image_count + ", id=" + id + ", data_image="
						+ data_image + "]";
			}
			
		}

		@Override
		public String toString() {
			return "SpaceData [space_date=" + space_date + ", space_data="
					+ space_data + "]";
		}
		
		
	}
}
