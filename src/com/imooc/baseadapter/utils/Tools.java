package com.imooc.baseadapter.utils;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

public class Tools {
	// 发送post请求
	public static String sendPost(String baseUrl,
			List<BasicNameValuePair> params) {
		HttpPost postMethod = new HttpPost(baseUrl);
		String result = null;

		HttpResponse httpResponse = null;
		try {
			// 设置httpPost请求参数
			postMethod.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
			httpResponse = new DefaultHttpClient().execute(postMethod);
			// System.out.println(httpResponse.getStatusLine().getStatusCode());
			if (httpResponse.getStatusLine().getStatusCode() == 200) {
				// 第三步，使用getEntity方法活得返回结果
				result = EntityUtils.toString(httpResponse.getEntity());
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}

	// 发送get请求
	public static String sendGet(final String baseUrl,
			List<BasicNameValuePair> params) {
		String param = URLEncodedUtils.format(params, "UTF-8");
		HttpGet getMethod = new HttpGet(baseUrl + "?" + param);
		HttpClient httpClient = new DefaultHttpClient();
		HttpResponse response;
		String result = null;
		try {
			response = httpClient.execute(getMethod);
			int responseCode = response.getStatusLine().getStatusCode();
			if (responseCode == 200) {
				result = EntityUtils.toString(response.getEntity(), "utf-8");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}
}
