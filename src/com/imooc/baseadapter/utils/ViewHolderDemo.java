/*
 * @Title ViewHolder.java
 * @Copyright Copyright 2010-2015 Yann Software Co,.Ltd All Rights Reserved.
 * @Description：
 * @author Yann
 * @date 2015-8-5 下午9:08:31
 * @version 1.0
 */
package com.imooc.baseadapter.utils;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Shader.TileMode;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 类注释
 * 
 * @author Yann
 * @date 2015-8-5 下午9:08:31
 */
public class ViewHolderDemo {
	private SparseArray<View> mViews;
	private int mPosition;
	private View mConvertView;

	public View getConvertView() {
		return mConvertView;
	}

	public ViewHolderDemo(Context context, ViewGroup parent, int layoutId,
			int position) {
		this.mViews = new SparseArray<View>();
		this.mPosition = position;
		this.mConvertView = LayoutInflater.from(context).inflate(layoutId,
				parent, false);
		this.mConvertView.setTag(this);
	}

	public static ViewHolderDemo get(Context context, View convertView,
			ViewGroup parent, int layoutId, int position) {
		// 用于保持选中状态
		SparseArray<View> vmap = new SparseArray<View>();
		if (vmap.get(position) == null) {
			vmap.put(position, convertView);
			return new ViewHolderDemo(context, parent, layoutId, position);
		} else {
			ViewHolderDemo holder = (ViewHolderDemo) convertView.getTag();
			holder.mPosition = position;// 更新数据
			convertView = vmap.get(position);
			return holder;
		}
	}

	/**
	 * 通过viewId获取控件
	 * 
	 * @param viewId
	 * @return
	 * @return T
	 * @author Yann
	 * @date 2015-8-5 下午9:38:39
	 */
	@SuppressWarnings("unchecked")
	public <T extends View> T getView(int viewId) {
		View view = mViews.get(viewId);

		if (null == view) {
			view = mConvertView.findViewById(viewId);
			mViews.put(viewId, view);
		}

		return (T) view;
	}

	/**
	 * 给ID为viewId的TextView设置文字text，并返回this
	 * 
	 * @param viewId
	 * @param text
	 * @return
	 * @return ViewHolder
	 * @author Yann
	 * @date 2015-8-5 下午11:05:17
	 */
	public ViewHolderDemo setText(int viewId, String text) {
		TextView tv = getView(viewId);
		tv.setText(text);
		return this;
	}

	/**
	 * 给ID为viewId的TextView设置文字text，并返回this
	 * 
	 * @param viewId
	 * @param text
	 * @return
	 * @return ViewHolder
	 * @author Yann
	 * @date 2015-8-5 下午11:05:17
	 */
	public ViewHolderDemo setTextS(int viewId, String text) {
		TextView tv = getView(viewId);
		tv.setText(text);
		tv.setTextSize(23);
		return this;
	}

	public ViewHolderDemo setImageVIew(int viewId, String text) {
		TextView tv = getView(viewId);
		tv.setText(text);
		return this;
	}

	public ViewHolderDemo setImageView(int viewId, Bitmap view) {
		ImageView iv = getView(viewId);
		iv.setImageBitmap(view);
		return this;
	};

//	// URL图片
//	public ViewHolderDemo setNetworkImageView(int viewId, NetworkImageView view) {
//		NetworkImageView iv = getView(viewId);
//		iv.setImageBitmap(view);
//		//iv.setImageBitmap(view);
////		iv.setImageUrl(null, imageLoader);
//		return this;
//	};
}
