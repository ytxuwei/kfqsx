package com.example.centre;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.jhf.IndexActivity;
import com.example.jhf.MainActivity;
import com.example.jhf.R;
import com.example.monthstar.OneDatyPerson;

/**
 * 个人设置选项内同
 * @author Administrator
 * 
 */
public class SetUp extends Activity {
	private ArrayList<OneDatyPerson> data;
	private String inc[] = { "帮助", "常见问题", "关于" };
	private ListView lvshool;
	private SetAdapter shoolDAdater;
	private OneDatyPerson oneDatyPerson;
	private ImageView shooll_GK;
	private Button btnExit;
	private ActivityManager activityMgr;

	@SuppressLint("ShowToast")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.set_of_news);
		lvshool = (ListView) findViewById(R.id.setup_ls);
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		shooll_GK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				SetUp.this.finish();
			}
		});
		
		data = new ArrayList<OneDatyPerson>();
		// 循环添加数据
		for (int i = 0; i < 3; i++) {
			oneDatyPerson = new OneDatyPerson();
			oneDatyPerson.setStrriqi(inc[i]);
			data.add(oneDatyPerson);
		}
		// 绑定adapter
		shoolDAdater = new SetAdapter(SetUp.this, data);
		lvshool.setAdapter(shoolDAdater); 
		lvshool.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				switch (arg2) {
				case 0:
					startActivity(new Intent(SetUp.this, Help.class));
					break;
				case 1:
					startActivity(new Intent(SetUp.this,CommonQuestion.class));
					break;
				case 2:
					startActivity(new Intent(SetUp.this, AboutActivty.class));
				default:
					break;
				}
			}
		});

		btnExit = (Button) findViewById(R.id.btn_exit);
		btnExit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				getSharedPreferences("userInfo", Context.MODE_PRIVATE).edit().clear().commit();
				getSharedPreferences("UserId", MODE_PRIVATE).edit().clear().commit();
				getSharedPreferences("chmod", MODE_PRIVATE).edit().clear().commit();
				
				Intent intent = new Intent(SetUp.this,MainActivity.class);
				startActivity(intent);
				finish();
			}
		});
		
	}

}
