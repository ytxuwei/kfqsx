package com.example.global;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.facebook.common.internal.Supplier;
import com.facebook.common.memory.MemoryTrimmable;
import com.facebook.common.memory.MemoryTrimmableRegistry;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.cache.MemoryCacheParams;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.SingleRequestQueue;

public class MyApplication extends Application {
	//public RequestQueue queue;
	public ImageLoader imageLoader;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		//Fresco.initialize(getApplicationContext());
		ImagePipelineConfig config = ImagePipelineConfig.newBuilder(getApplicationContext()).setMemoryTrimmableRegistry(new MemoryTrimmableRegistry() {
			
			@Override
			public void unregisterMemoryTrimmable(MemoryTrimmable arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void registerMemoryTrimmable(MemoryTrimmable arg0) {
				// TODO Auto-generated method stub
			}
		}).build();
		Fresco.initialize(getApplicationContext(), config);
		
		imageLoader = new ImageLoader(SingleRequestQueue.getRequestQueue(getApplicationContext()), new BitmapCache());
		
	}
}
