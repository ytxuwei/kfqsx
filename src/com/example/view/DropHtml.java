package com.example.view;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 然而只能去除p标签
 * @author Administrator
 *
 */
public class DropHtml {
	public static String drop(String str){
		String regEx = "</?[p|P][^>]*>";
		if (str != null) {
			Pattern pa = Pattern.compile(regEx);
			Matcher matcher = pa.matcher(str);
			str = matcher.replaceAll("").trim();
		} 
		return str;
	}
}
