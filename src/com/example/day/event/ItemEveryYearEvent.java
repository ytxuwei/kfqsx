package com.example.day.event;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.global.Constant;
import com.example.index.RefreshableView;
import com.example.jhf.ContentDetailActivity;
import com.example.jhf.R;
import com.example.monthstar.OneDatyPerson;
import com.example.monthstar.OneDay;
import com.example.monthstar.OneDayAdper;
import com.example.monthstar.OneDayAdpterChange;
import com.example.monthstar.OneDayStudent;
import com.example.view.ListViewIndicatorUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.imooc.baseadapter.utils.Tools;

public class ItemEveryYearEvent extends Activity {
	private ImageView ivBack;
	/**
	 * 每日白星
	 */
	private ArrayList<OneDatyPerson> data;
	private ListView lv;
	private TextView title, tv;
	private Handler handler = new Handler();
	private RefreshableView refreshableView;
	private ObjectMapper om = new ObjectMapper();
	private String onedayconten;// 每条item的标题
	private String code, str;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private OneDayAdpterChange shoolDAdater;
	private int p = 0;// 取回的页数
	private int i = 2;// 从大二页开始加载更多
	private PullToRefreshListView lvAnswer;// 加载更多

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.update_item_everyyear);
		init();

		getJsonText(1);
		ivBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				ItemEveryYearEvent.this.finish();
			}
		});
	}

	private void init() {
		ivBack = (ImageView) findViewById(R.id.iv_back);
		title = (TextView) findViewById(R.id.tv_everyyear);
		lvAnswer = (PullToRefreshListView) findViewById(R.id.lv_everyyearup);
		ListViewIndicatorUtil.initIndicator(lvAnswer);// 下拉改变字体
		// 刷新
		lvAnswer.setOnRefreshListener(new com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(
					PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullDownToRefresh");

				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						data.clear();
						getJsonText(1);
						lvAnswer.onRefreshComplete();
					}
				}, 3000);
			}

			// 加载更多
			@Override
			public void onPullUpToRefresh(
					PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullUpToRefresh");
				// 这里写上拉加载更多的任务

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						getJsonText(i);
						i++;
						lvAnswer.onRefreshComplete();
					}
				}, 3000);

			}

		});
	}

	/**
	 * 获取
	 */
	private void getJsonText(final int i) {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						data = new ArrayList<OneDatyPerson>();
						jsonObject = new JSONObject(result);
						jsonArray = jsonObject.getJSONArray("data");
						p = jsonObject.getInt("page_count");
						for (int i = 0; i < jsonArray.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							onedayconten = (String) jsonObject2.get("name");
							OneDatyPerson shoolLITools = new OneDatyPerson();
							shoolLITools.setStrriqi(onedayconten);
							data.add(shoolLITools);
						}
						shoolDAdater = new OneDayAdpterChange(
								ItemEveryYearEvent.this, data);
						lvAnswer.setAdapter(shoolDAdater);
						lvAnswer.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0,
									View arg1, int arg2, long arg3) {
								try {
									Intent intent = new Intent(
											ItemEveryYearEvent.this,
											OneDayStudent.class);
									JSONObject jsonObject3 = (JSONObject) jsonArray
											.opt(arg2-1);
									Integer int1 = jsonObject3
											.getInt("line_id");
									// int start_id =(Integer)
									// jsonObject3.get("line_id");
									String str_id = int1.toString();

									intent.putExtra("sta_line_id", str_id);
									// Log.v("OneDay", "start_id"+start_id);
									// SharedPreferences
									// sp=getSharedPreferences("star_id",
									// MODE_PRIVATE);
									// Editor edit = sp.edit();
									// edit.putString("star_id", start_id);
									// edit.commit();
									startActivity(intent);
								} catch (Exception e) {
									// TODO: handle exceptions
									e.printStackTrace();
								}

							}
						});
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// Intent intent = new Intent();
					// intent.setClass(ShoolJS.this, IndexActivity.class);
					// startActivity(intent);
				} else if (msg.what == 6) {
					Toast.makeText(ItemEveryYearEvent.this, "帐号或密码错误", 0)
							.show();
				} else if (msg.what == 10) {
					Toast.makeText(ItemEveryYearEvent.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				Intent intent = getIntent();
				String star_id = intent.getStringExtra("star_id");
				params.add(new BasicNameValuePair("page", i + ""));// 当前页数
				params.add(new BasicNameValuePair("page_count", p + ""));
				params.add(new BasicNameValuePair("star_id", star_id));
				params.add(new BasicNameValuePair("db", Constant.db));
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				String result = Tools.sendGet(Constant.starTitle, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}
}
