package com.example.transceiver;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jhf.R;
import com.example.space.ClassAlbumPer;

public class Shool_TransceiverAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private ArrayList<Shool_TransceiverPer> data;
	private int layoutID;

	public Shool_TransceiverAdapter(Context context,  ArrayList<Shool_TransceiverPer> data) {
		this.context = context;
		this.layoutInflater = LayoutInflater.from(context);
		this.layoutID = layoutID;
		this.data=data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("ResourceAsColor") @Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		final ViewHolder viewHolder;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.shool_transcetiem, null);
			viewHolder = new ViewHolder();
			
			viewHolder.title_ST_id = (TextView) convertView.findViewById(R.id.title_ST_id);
			viewHolder.content_ST_id = (TextView) convertView.findViewById(R.id.content_ST_id);
			viewHolder.content_ST_tiem = (TextView) convertView.findViewById(R.id.text_Nameid);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		 viewHolder.title_ST_id.setText(data.get(position).getTileStr());
		 viewHolder.content_ST_id.setText(data.get(position).getContentStr());
		 viewHolder.content_ST_tiem.setText(data.get(position).getTime());

		 if (position == selectItem) {  
			 viewHolder.title_ST_id.setTextColor(android.graphics.Color.parseColor("#49b2d0"));
			 viewHolder.content_ST_id.setTextColor(android.graphics.Color.parseColor("#49b2d0"));
			 viewHolder.content_ST_tiem.setTextColor(android.graphics.Color.parseColor("#49b2d0"));
         }   
         else {  
        	 viewHolder.title_ST_id.setTextColor(android.graphics.Color.parseColor("#000000"));  
        	 viewHolder.content_ST_id.setTextColor(android.graphics.Color.parseColor("#000000"));  
        	 viewHolder.content_ST_tiem.setTextColor(android.graphics.Color.parseColor("#000000"));  
         }     
		return convertView;
	}
	public  void setSelectItem(int selectItem) {  
        this.selectItem = selectItem;  
   }  
   private int  selectItem=0;  

	public static class ViewHolder {
		public TextView title_ST_id;
		public TextView content_ST_id;
		public TextView content_ST_tiem;
	}
}

