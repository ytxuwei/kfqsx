 package com.example.jhf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.space.ClassAlbum;
import com.example.space.ClassBlogActivity;
import com.example.space.ClassHomeWork;
import com.example.space.ClassNumber;
import com.example.space.ClassQuestionActivity;

/**
 * 班级空间界面 创建时间2015.9.10 创建者：刘永华
 * 
 * @author Administrator
 * 
 */
public class TwoFragment extends Fragment {
	private String[] str = { "班级博客", "教师成员", "班级相册", "作业", "知识回答" };
	private int[] inc = { 1, 2, 3, 4, 5 };
	private int[] intL = { R.drawable.class_xye };
	private Context context;
	private ArrayList<Map<String, Object>> data;
	private CPAdapter cpAdapter;
	private ListView mListView;
	private String ifConfirm;
	private String ifLoadConfirm;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		context = getActivity();
		View view = inflater.inflate(R.layout.classspace, container, false);
		mListView = (ListView) view.findViewById(R.id.lvcp);
		cpAdapter = new CPAdapter(context, intn(), R.layout.classpaceitem);
		mListView.setAdapter(cpAdapter);

		SharedPreferences mShared = getActivity().getSharedPreferences("user", 0);
		ifConfirm = mShared.getString("isConfirm", "false");
		SharedPreferences confirm = getActivity().getSharedPreferences(
				"confirm", 0);
		ifLoadConfirm = confirm.getString("isConfirm", "false");
		
		/*if(ifConfirm.equals("false")&&ifLoadConfirm.equals("false")){
			//未认证则显示提示，并隐藏listview以及禁止点 击
			AlertDialog dialog = new AlertDialog.Builder(context).setTitle("提示").setMessage("您还未认证，请去个人中心进行验证").show();
			dialog.getWindow().setLayout(600, 300);
			
			mListView.setVisibility(View.GONE);
			mListView.setEnabled(false);
		}*/
		OnlickTiem();
		return view;
	}

	private List<Map<String, Object>> intn() {
		data = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < str.length; i++) {
			Map<String, Object> list = new HashMap<String, Object>();
			list.put("图标", inc[i]);
			list.put("项目", intL[0]);
			list.put("进入", str[i]);
			data.add(list);
		}
		return data;
	}

	public void OnlickTiem() {
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				if(ifConfirm.equals("false")&&ifLoadConfirm.equals("false")){
					AlertDialog dialog = new AlertDialog.Builder(context).setTitle("提示").setMessage("您还未认证，请去个人中心进行验证").show();
					dialog.getWindow().setLayout(600, 300);
					
					mListView.setVisibility(View.GONE);
					mListView.setEnabled(false);
				}else{
					HashMap<String, String> mp = (HashMap<String, String>) mListView
							.getItemAtPosition(arg2);
					String keyStr = mp.get("进入");
					if (keyStr.equals("班级博客")) {
						Intent intent = new Intent();
						intent.setClass(context, ClassBlogActivity.class);
						startActivity(intent);
					}
					if (keyStr.equals("教师成员")) {
						Intent intent = new Intent();
						intent.setClass(context, ClassNumber.class);
						startActivity(intent);
					}
					if (keyStr.equals("班级相册")) {
						Intent intent = new Intent();
						intent.setClass(context, ClassAlbum.class);
						startActivity(intent);
					}
					if (keyStr.equals("作业")) {
						Intent intent = new Intent();
						intent.setClass(context, ClassHomeWork.class);
						startActivity(intent);
					}
					if (keyStr.equals("知识回答")) {
						Intent intent = new Intent();
						intent.setClass(context, ClassQuestionActivity.class);
						startActivity(intent);
					}
				}
				
			}
		});
	}
}
