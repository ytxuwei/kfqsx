package com.example.studentwork;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.UserDictionary.Words;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.example.global.Constant;
import com.example.index.ShoolLIAdpter;
import com.example.index.ShoolLITools;
import com.example.jhf.ContentDetailActivity;
import com.example.jhf.R;
import com.imooc.baseadapter.utils.Tools;

/**
 * 学生作品
 * 
 * @author Administrator
 * 
 */
public class StudentWorksActivity extends Activity {
	private ImageView shooll_GK;
	private ObjectMapper om = new ObjectMapper();
	private String Words;// 每条item的标题
	private String code;
	private JSONArray jsonArray;
	private ArrayList<ShoolLITools> data;
	private StudentWorksAdapter shoolDAdater;
	private ListView lvshool;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.studentworks_update);
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		lvshool = (ListView) findViewById(R.id.list_update);
		back();
		getValue();
	}

	/**
	 * 返回
	 */
	public void back() {
		shooll_GK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				StudentWorksActivity.this.finish();
			}
		});
	}

	/**
	 * 获得JSON数据
	 */
	public void getValue() {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					data = new ArrayList<ShoolLITools>();
					JSONObject jsonObject;
					try {
						jsonObject = new JSONObject(result);
						jsonArray = jsonObject.getJSONArray("data");
						for (int i = 0; i < jsonArray.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							Words = (String) jsonObject2.get("name");
							ShoolLITools shoolLITools = new ShoolLITools();
							shoolLITools.setStr_shoolli(Words);
							data.add(shoolLITools);
						}
						shoolDAdater = new StudentWorksAdapter(
								StudentWorksActivity.this, data);
						// 将添加的每条item放到Lstview中
						lvshool.setAdapter(shoolDAdater);
						lvshool.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0,
									View arg1, int arg2, long arg3) {
								// TODO Auto-generated method stub

								try {
									Intent intent = new Intent(
											StudentWorksActivity.this,
											StudentWork.class);
									JSONObject jsonObject3 = (JSONObject) jsonArray
											.opt(arg2);
									intent.putExtra("work_id",
											jsonObject3.get("id").toString());
									intent.putExtra("TitleName", data.get(arg2).getStr_shoolli());
									Log.v("TitleName",Words+"item名字");
									startActivity(intent);
								} catch (Exception e) {
									// TODO: handle exception
								}

							}
						});
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}


					// Intent intent = new Intent();
					// intent.setClass(ShoolJS.this, IndexActivity.class);
					// startActivity(intent);
				} else if (msg.what == 6) {
					Toast.makeText(StudentWorksActivity.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(StudentWorksActivity.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				String result = Tools.sendGet(Constant.work, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}
}
