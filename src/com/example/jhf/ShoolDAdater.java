package com.example.jhf;

import java.util.List;
import java.util.Map;

import android.R.integer;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ShoolDAdater extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private List<Map<String, Object>> data;
	private int layoutID;

	public ShoolDAdater(Context context, List<Map<String, Object>> data,
			int layoutID) {
		this.context = context;
		this.layoutInflater = LayoutInflater.from(context);
		this.layoutID = layoutID;
		this.data=data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		final ViewHolder viewHolder;
		SharedPreferences mShared = context.getSharedPreferences("user",0);
		String ifConfirm = mShared.getString("isConfirm", "false");
		
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.shooldstitem, null);
			viewHolder = new ViewHolder();
			viewHolder.shoolIVcpI = (ImageView) convertView
					.findViewById(R.id.shoolIVcpI_id);
			viewHolder.shoolTVcp = (TextView) convertView.findViewById(R.id.shoolTVcp_id);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		Map<String, Object> map = data.get(position);
		int img= Integer.parseInt(map.get("图标").toString()); 

		switch (img) {
		case 1:
			viewHolder.shoolIVcpI.setImageResource(R.drawable.shooldst);
			break;
		case 2:
			viewHolder.shoolIVcpI.setImageResource(R.drawable.shoolgb);
			break;
		case 3:
			viewHolder.shoolIVcpI.setImageResource(R.drawable.shooldt);
			break;
		case 4:
			viewHolder.shoolIVcpI.setImageResource(R.drawable.shoolbb);
			break;		
		case 5:	
			viewHolder.shoolIVcpI.setImageResource(R.drawable.shooldst);
			break;
		}
	
		switch (img) {
		case 1:
			viewHolder.shoolTVcp.setText("校园广播");
			break;
		case 2:
			viewHolder.shoolTVcp.setText("校园电视台");
			break;
		case 3:
			viewHolder.shoolTVcp.setText("班级广播");
			break;
		case 4:
			viewHolder.shoolTVcp.setText("班级电视台");
			break;	
		case 5:
			viewHolder.shoolTVcp.setText("视频直播");
			break;
		}
		if(ifConfirm.equals("false")){
			if(img ==3||img==4||img==5){
				convertView.setVisibility(View.GONE);
			}
		}
		return convertView;
	}

	public static class ViewHolder {
		public ImageView shoolIVcpI;
		public TextView shoolTVcp;
	}
}
