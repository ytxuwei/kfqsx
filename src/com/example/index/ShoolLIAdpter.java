package com.example.index;
 

  

import java.util.List;

import com.example.jhf.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * 班级空间baseAdapter
 * @author Administrator
 *
 */
@SuppressLint("ResourceAsColor")
public class ShoolLIAdpter extends BaseAdapter {
	private Context context;
	private List<ShoolLITools> data;
	//private ImageView imageView;
	private ShoolLITools schoolLiTools =new ShoolLITools();
	public ShoolLIAdpter(Context context, List<ShoolLITools> data
			) {
		this.context = context;
		this.data=data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	// 用于保持选中状态
		private SparseArray<View> vmap = new SparseArray<View>();
	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		final ViewHolder viewHolder;
		if (vmap.get(position)==null) {
			convertView=LayoutInflater.from(context).inflate(R.layout.homechange, null);
			viewHolder = new ViewHolder();
			viewHolder.TVshoolli=(TextView) convertView.findViewById(R.id.TV_tiemoneday_id);
			//imageView=(ImageView) convertView.findViewById(R.id.xiala_IV);
			convertView.setTag(viewHolder);
			vmap.put(position, convertView);
		} else {
			convertView = vmap.get(position);
			viewHolder = (ViewHolder) convertView.getTag();
		}
//		if (position==0) {
//			imageView.setVisibility(View.VISIBLE);
//		} else {
//			imageView.setVisibility(View.GONE);
//		}
     viewHolder.TVshoolli.setText(data.get(position).getStr_shoolli().toString());
		return convertView;
	}

	public final class ViewHolder {
		private TextView TVshoolli;
	}
	//添加数据
	public void addItem(String value){
		schoolLiTools.setStr_shoolli(value);
		data.add(schoolLiTools);
	}
}


