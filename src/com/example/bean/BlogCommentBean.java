package com.example.bean;

import android.graphics.Bitmap;

public class BlogCommentBean {
	public String comment_note;
	public String comment_id;
	public String comment_date;
	public String comment_portrait;
	public String comment_user;
	public BlogCommentBean(String comment_note, String comment_id,
			String comment_date, String comment_portrait, String comment_user) {
		super();
		this.comment_note = comment_note;
		this.comment_id = comment_id;
		this.comment_date = comment_date;
		this.comment_portrait = comment_portrait;
		this.comment_user = comment_user;
	}
	
	
}
