package com.example.jhf;

import org.apache.http.util.EncodingUtils;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;


public class MyContentDetailActivity extends Activity {
	private TextView tvContent, tvTitle, tvTitle2;
	private ImageView ivBack;
	private TextView tvDate;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.content_detail);
		ivBack = (ImageView) findViewById(R.id.shooll_GK);
		tvTitle = (TextView) findViewById(R.id.tv_content_title);
		tvTitle2 = (TextView) findViewById(R.id.tv_content_detail);
		tvDate = (TextView) findViewById(R.id.tv_content_detail_time);
		String title = getIntent().getStringExtra("title");
		String note = getIntent().getStringExtra("note");
		String year = getIntent().getStringExtra("year");
		String date = getIntent().getStringExtra("date").substring(0,10);

		tvTitle.setText(title);
		tvTitle2.setText(title);
		tvDate.setText(date);
		//tvContent = (TextView) findViewById(R.id.tv_detail_content);
		//tvContent.setText(note);
		
		back();
		
		WebView webView = (WebView) findViewById(R.id.wv_content);
		//webView.loadData(note, "text/html; charset=UTF-8", null);
		//WebSettings settings = webView.getSettings();
		//settings.setTextSize(TextSize.SMALLER);
		
		int a =14;	//字体大小
		String strUrl= "<html> \n" +
	              "<head> \n" +
	              "<style type=\"text/css\"> \n" +
	              "body {text-align:justify; font-size: "+a+"px; line-height: "+(a+6)+"px}\n" +
	              "</style> \n" +
	              "</head> \n" +
	              "<body>"+EncodingUtils.getString(note.getBytes(), "UTF-8")+"</body> \n </html>";
	       webView.loadData(strUrl, "text/html; charset=UTF-8", null);
	}

	/**
	 * 返回图标事件
	 */
	private void back() {
		ivBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}
	
}
