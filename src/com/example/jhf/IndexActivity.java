package com.example.jhf;

import java.util.Timer;
import java.util.TimerTask;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.Toast;

/**
 * 娑撳銆�
 * 
 * @author JiangJie
 * 
 */
public class IndexActivity extends FragmentActivity implements OnClickListener {
	private Context context;
	private RadioButton ra_home;
	private RadioButton ra_class;
	private RadioButton ra_shcool;
	private RadioButton ra_person;
	private OneFragment oneFragment;
	private TwoFragment twoFragment;
	private ThreeFragment threeFragment;
	private FourFragment fourFragment;
	private static Boolean isExit = false;
	private FragmentManager fm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.bottom_menu);
		
		init();
		

	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	private void init() {
		context = this;
		ra_home = (RadioButton) findViewById(R.id.homepages);
		//ra_home.setOnCheckedChangeListener(this);
		ra_home.setOnClickListener(this);
		ra_class = (RadioButton) findViewById(R.id.bjkj);
		//ra_class.setOnCheckedChangeListener(this);
		ra_class.setOnClickListener(this);
		ra_shcool = (RadioButton) findViewById(R.id.xydt);
		//ra_shcool.setOnCheckedChangeListener(this);
		ra_shcool.setOnClickListener(this);
		ra_person = (RadioButton) findViewById(R.id.grzx);
		//ra_person.setOnCheckedChangeListener(this);
		ra_person.setOnClickListener(this);
		//setDefaultFragment();
		showFragment(1);
	}

	private void setDefaultFragment() {// 鐠佸墽鐤嗘妯款吇妞わ拷
		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction transaction = manager.beginTransaction();
		transaction.add(R.id.fragmentRoot, oneFragment, "home");
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
		transaction.commit();
	}

	/*@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			// FragmentManager manager = getSupportFragmentManager();
			// FragmentTransaction transaction = manager.beginTransaction();
			switch (buttonView.getId()) {
			case R.id.homepages:
				ra_class.setChecked(false);
				ra_shcool.setChecked(false);
				ra_person.setChecked(false);
				// transaction.replace(R.id.fragmentRoot, oneFragment,"home");
				// transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
				showFragment(1);
				break;
			case R.id.bjkj:
				ra_home.setChecked(false);
				ra_shcool.setChecked(false);
				ra_person.setChecked(false);
				// transaction.replace(R.id.fragmentRoot,
				// twoFragment,"simulation");
				// transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
				showFragment(2);
				break;
			case R.id.xydt:
				ra_home.setChecked(false);
				ra_class.setChecked(false);
				ra_person.setChecked(false);
				// transaction.replace(R.id.fragmentRoot, threeFragment,"user");
				// transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
				break;
			case R.id.grzx:
				ra_home.setChecked(false);
				ra_shcool.setChecked(false);
				ra_class.setChecked(false);
				// transaction.replace(R.id.fragmentRoot, fourFragment,"user");
				// transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
				break;
			}
			// transaction.commit();
		}
	}*/

	public void showFragment(int index) {
		
		fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();

		// 想要显示一个fragment,先隐藏所有fragment，防止重叠
		hideFragments(ft);

		switch (index) {
		case 1:
			// 如果fragment1已经存在则将其显示出来
			if (oneFragment != null)
				ft.show(oneFragment);
			// 否则是第一次切换则添加fragment1，注意添加后是会显示出来的，replace方法也是先remove后add
			else {
				oneFragment = new OneFragment();
				ft.add(R.id.fragmentRoot, oneFragment,"home");
				//ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
			}
			break;
		case 2:
			if (twoFragment != null){
				ft.show(twoFragment);
			}else {
				twoFragment = new TwoFragment();
				ft.add(R.id.fragmentRoot, twoFragment);
				//ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
			}
			break;
		case 3:
			if (threeFragment != null)
				ft.show(threeFragment);
			else {
				threeFragment = new ThreeFragment();
				ft.add(R.id.fragmentRoot, threeFragment);
				//ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
			}
			break;
		case 4:
			if (fourFragment != null)
				ft.show(fourFragment);
			else {
				fourFragment = new FourFragment();
				ft.add(R.id.fragmentRoot, fourFragment);
				//ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
			}
			break;
		}
		ft.commit();
	}

	// 当fragment已被实例化，就隐藏起来
	public void hideFragments(FragmentTransaction ft) {
		if (oneFragment != null)
			ft.hide(oneFragment);
		if (twoFragment != null)
			ft.hide(twoFragment);
		if (threeFragment != null)
			ft.hide(threeFragment);
		if (fourFragment != null)
			ft.hide(fourFragment);
	}

	/**
	 * 鑿滃崟銆佽繑鍥為敭鐩稿簲
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			exitBy2Click(); // 璋冪敤鍙屽嚮閫�鍑哄嚱鏁�
		}
		return false;
	}

	/**
	 * 鍙屽嚮閫�鍑虹▼搴�
	 */
	

	private void exitBy2Click() {
		Timer tExit = null;
		if (isExit == false) {
			isExit = true; // 鍑嗗閫�鍑�
			Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
			tExit = new Timer();
			tExit.schedule(new TimerTask() {
				@Override
				public void run() {
					isExit = false; // 鍙栨秷閫�鍑�
				}
			}, 2000); // 濡傛灉2绉掑唴娌℃湁鎸変笅杩斿洖閿紝鍒欏惎鍔ㄥ畾鏃跺櫒鍙栨秷鎺夊垰鎵嶆墽琛岀殑

		} else {
			int currentVersion = android.os.Build.VERSION.SDK_INT;
			if (currentVersion > android.os.Build.VERSION_CODES.ECLAIR_MR1) {
				Intent startMain = new Intent(Intent.ACTION_MAIN);
				startMain.addCategory(Intent.CATEGORY_HOME);
				startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(startMain);
				System.exit(0);
				finish();
			} else {// android2.1
				ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
				am.restartPackage(getPackageName());
			}
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.homepages:
			ra_class.setChecked(false);
			ra_shcool.setChecked(false);
			ra_person.setChecked(false);
			// transaction.replace(R.id.fragmentRoot, oneFragment,"home");
			// transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
			showFragment(1);
			break;
		case R.id.bjkj:
			ra_home.setChecked(false);
			ra_shcool.setChecked(false);
			ra_person.setChecked(false);
			// transaction.replace(R.id.fragmentRoot,
			// twoFragment,"simulation");
			// transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
			showFragment(2);
			break;
		case R.id.xydt:
			ra_home.setChecked(false);
			ra_class.setChecked(false);
			ra_person.setChecked(false);
			// transaction.replace(R.id.fragmentRoot, threeFragment,"user");
			// transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
			showFragment(3);
			break;
		case R.id.grzx:
			ra_home.setChecked(false);
			ra_shcool.setChecked(false);
			ra_class.setChecked(false);
			showFragment(4);
			// transaction.replace(R.id.fragmentRoot, fourFragment,"user");
			// transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
			break;
		}
	}

}
