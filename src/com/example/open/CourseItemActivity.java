package com.example.open;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.bean.CourseBean;
import com.example.global.Constant;
import com.example.jhf.MyContentDetailActivity;
import com.example.jhf.R;
import com.example.view.ListViewIndicatorUtil;
import com.example.view.RefreshLayout;
import com.example.view.RefreshLayout.OnLoadListener;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.imooc.baseadapter.utils.Tools;

public class CourseItemActivity extends Activity {
	private int itemId;
	private ArrayList<CourseBean> datas;
	private ObjectMapper om = new ObjectMapper();
	private String note;
	private TextView tvTitle;
	private ImageView ivBack;
	private int courseId;
	private CourseItemAdapter adapter;
	private PullToRefreshListView mPullRefreshListView;
	private int i = 2; // 加载更多第几页
	private int pageCount;	//请求总页数
	private String mDate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.openjjr);
		datas = new ArrayList<CourseBean>();
		// mListView = (ListView) findViewById(R.id.jjr_list);
		mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);
		mPullRefreshListView.setMode(Mode.BOTH);
		ListViewIndicatorUtil.initIndicator(mPullRefreshListView);
		mPullRefreshListView.setOnRefreshListener(new com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullDownToRefresh");
				
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						datas.clear();
						getDataFromNet(1);
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullUpToRefresh");
				// 这里写上拉加载更多的任务
				
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						getDataFromNet(i);
						i++;
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);
				
			}

		});

		tvTitle = (TextView) findViewById(R.id.tv_everyyear);
		tvTitle.setText("课程");
		itemId = getIntent().getIntExtra("title", 1);
		courseId = getIntent().getIntExtra("datas", 1);
		ivBack = (ImageView) findViewById(R.id.shooll_GK);
		ivBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		getDataFromNet(1);

		mPullRefreshListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
				new Thread() {
					public void run() {
						// 设置参数
						SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
						String uid = sp.getString("uid", "1");
						// 设置参数
						List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
						params.add(new BasicNameValuePair("db", Constant.db));// 数据库
						params.add(new BasicNameValuePair("uid", uid));
						params.add(new BasicNameValuePair("course_line_id", datas.get(arg2-1).id + ""));
						String homeworkDetail = Tools.sendGet(Constant.courseMsg, params);
						processDetailData(homeworkDetail);

						Intent intent = new Intent(CourseItemActivity.this, MyContentDetailActivity.class);
						intent.putExtra("title", datas.get(arg2-1).name);
						intent.putExtra("note", note);
						intent.putExtra("date", mDate);
						startActivity(intent);

					}
				}.start();

			}
		});

		adapter = new CourseItemAdapter(CourseItemActivity.this, datas);
		mPullRefreshListView.setAdapter(adapter);
	}

	private void processDetailData(String json) {
		try {
			JSONObject jsonObj = new JSONObject(json);
			JSONObject array = jsonObj.getJSONObject("data");
			note = (String) array.get("note");
			mDate = array.getString("date");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void getDataFromNet(final int i) {
		// TODO Auto-generated method stub
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;
			private JSONArray jsonArray;
			private JSONObject jsonObject;
			

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						jsonObject = new JSONObject(result);
						jsonArray = jsonObject.getJSONArray("data");
						pageCount = jsonObject.getInt("page_count");	//总页数
						for (int i = 0; i < jsonArray.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							String title = (String) jsonObject2.get("title");
							int id = jsonObject2.getInt("id");
							datas.add(new CourseBean(id, title));
						}
						adapter.notifyDataSetChanged();

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
				String uid = sp.getString("uid", "1");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("course_id", courseId + ""));
				params.add(new BasicNameValuePair("page", i + ""));
				params.add(new BasicNameValuePair("page_count", pageCount + ""));
				String result = Tools.sendGet(Constant.courseLine, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						String code = jsonNode.get("code").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

}
