package com.example.notice;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.bean.NoticeBean;
import com.example.global.Constant;
import com.example.index.RefreshableView;
import com.example.index.RefreshableView.PullToRefreshListener;
import com.example.jhf.MyContentDetailActivity;
import com.example.jhf.R;
import com.example.view.ListViewIndicatorUtil;
import com.example.view.RefreshLayout;
import com.example.view.RefreshLayout.OnLoadListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.imooc.baseadapter.utils.Tools;

/**
 * 
 * 通知公告
 * 
 * @author Administrator
 */
public class Notice extends ListActivity implements OnItemClickListener {
	private ImageView iView, headImg;
	private ImageView shooll_GK;
	private TextView tv, tvTitle;

	private String result; // json数据

	private String resultDetail; // json数据

	private MyAdapter myAdapter;

	private String jsonNote;
	private String note;
	private ArrayList<NoticeBean> datas;
	private String uid;
	private PullToRefreshListView mPullRefreshListView;
	private int page = 2; // 加载更多第几页
	private int pageCount; // 请求总页数

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.notice);

		SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
		uid = sp.getString("uid", "0");
		datas = new ArrayList<NoticeBean>();

		// 刷新页面
		// onRefresh();
		getJsonText(1);
		initView();
		myAdapter = new MyAdapter(Notice.this, datas);
		mPullRefreshListView.setAdapter(myAdapter);
		mPullRefreshListView.setOnItemClickListener(this);
	}

	private void initView() {
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		tvTitle = (TextView) findViewById(R.id.tidgfsdtleTV);
		shooll_GK.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Notice.this.finish();
			}
		});
		mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);
		mPullRefreshListView.setMode(Mode.BOTH);
		ListViewIndicatorUtil.initIndicator(mPullRefreshListView);
		ListView refreshableView = mPullRefreshListView.getRefreshableView();
		View header = getLayoutInflater().inflate(R.layout.header_notice, null);
		//refreshableView.addHeaderView(header);
		mPullRefreshListView.setOnRefreshListener(new com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullDownToRefresh");

				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						datas.clear();
						getJsonText(1);
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullUpToRefresh");
				// 这里写上拉加载更多的任务

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						getJsonText(page);
						page++;
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);

			}

		});

	}

	private void getJsonText(final int i) {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						JSONObject jsonObject = new JSONObject(result);
						JSONArray jsonArray = jsonObject.getJSONArray("data");
						pageCount = jsonObject.getInt("page_count");
						for (int i = 0; i < jsonArray.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							int noticeId = (Integer) jsonObject2.get("notice_id");
							String noticeTitle = (String) jsonObject2.get("name");
							String noticeState = jsonObject2.getString("state");
							String noticeDate = jsonObject2.getString("date");
							datas.add(new NoticeBean(noticeId, noticeTitle, noticeState,noticeDate));
						}

						// myAdapter = new MyAdapter(Notice.this, datas);
						myAdapter.notifyDataSetChanged();

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("page", i + "")); // 密码
				params.add(new BasicNameValuePair("page_count", pageCount+"")); // 密码

				result = Tools.sendGet(Constant.noticeList, params);
				Message message = new Message();
				message.what = 7;
				message.obj = result;
				handler.sendMessage(message);
			};
		}.start();
	}

	class MyAdapter extends BaseAdapter {
		private Context context;
		private List<NoticeBean> data;

		public MyAdapter(Context context, List<NoticeBean> data) {
			this.context = context;
			this.data = data;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub

			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub

			return position;
		}

		// 用于保持选中状态

		private SparseArray<View> vmap = new SparseArray<View>();
		private ImageView readImageView;

		@Override
		public View getView(final int position, View convertView, ViewGroup arg2) {
			final ViewHolder viewHolder;
			if (vmap.get(position) == null) {
				convertView = LayoutInflater.from(context).inflate(R.layout.ll_notice, null);
				viewHolder = new ViewHolder();
				viewHolder.title = (TextView) convertView.findViewById(R.id.content);
				if (datas.get(position).state.equals("read")) {
					viewHolder.title.setTextColor(Color.GRAY);
				} else {
					viewHolder.title.setTextColor(Color.BLACK);
				}

				convertView.setTag(viewHolder);
				vmap.put(position, convertView);
			} else {
				convertView = vmap.get(position);
				viewHolder = (ViewHolder) convertView.getTag();
			}

			viewHolder.title.setText(data.get(position).title);
			/*
			 * // 点击变色 if (position == selectItem) { //
			 * convertView.setBackgroundColor(Color.GRAY);
			 * viewHolder.title.setTextColor(Color.YELLOW); }else{
			 * viewHolder.title.setTextColor(Color.BLUE); }
			 */
			return convertView;
		}

		public void setSelectItem(int selectItem) {
			this.selectItem = selectItem;
		}

		private int selectItem = -1;

		public final class ViewHolder {
			private TextView title;
		}

	}

	/**
	 * 
	 * 点击跳转到内容页面 db,uid,notice_id
	 */
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
		if(arg2>1){
			TextView view = (TextView) arg0.getChildAt(arg2-1).findViewById(R.id.content);
			view.setTextColor(Color.GRAY);
		}
		// myAdapter.setSelectItem(arg2 - 1);
		new Thread() {
			public void run() {
				// 设置参数
				if (arg2 >0) {
					List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
					params.add(new BasicNameValuePair("db", Constant.db));// 数据库
					params.add(new BasicNameValuePair("uid", uid));
					params.add(new BasicNameValuePair("notice_id", datas.get(arg2-1).code + ""));
					resultDetail = Tools.sendGet(Constant.noticeContent, params);
					processDetailData(resultDetail);
					Intent intent = new Intent(Notice.this, MyContentDetailActivity.class);
					intent.putExtra("title", datas.get(arg2-1).title);
					intent.putExtra("note", note);
					intent.putExtra("date", datas.get(arg2-1).date);
					startActivity(intent);

				}
			}
		}.start();

		myAdapter.notifyDataSetInvalidated();

	}

	protected void processDetailData(String json) {
		try {
			JSONObject jsonObj = new JSONObject(json);
			int cccode = jsonObj.getInt("code");

			JSONObject array = jsonObj.getJSONObject("data");
			note = (String) array.get("note");
		} catch (JSONException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
	}

}