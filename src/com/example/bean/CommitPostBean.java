package com.example.bean;

/**
 * 评论博客返回数据
 * {"comment_id":1,
 * "comment_user":"评论人",
 * "comment_portrait":"头像",
 * "comment_date":"时间","
 * comment_note":评论内容}}
 * @author Administrator
 *
 */
public class CommitPostBean {
	public String mId;
	public String mUser;
	public String mPortrait;
	public String mDate;
	public String mNote;
	public CommitPostBean(String mId, String mUser, String mPortrait,
			String mDate, String mNote) {
		super();
		this.mId = mId;
		this.mUser = mUser;
		this.mPortrait = mPortrait;
		this.mDate = mDate;
		this.mNote = mNote;
	}
	
}
