package com.example.centre;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.jhf.R;

public class ChangeNickName extends Activity{

	private EditText etNickName;
	private Button btnSave;
	private ImageView ivBack;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.change_nickname);
		 
		etNickName = (EditText) findViewById(R.id.et_change_nickname);
		SharedPreferences sp = getSharedPreferences("pickName", MODE_PRIVATE);
		String pickName = sp.getString("pickname", "");
		etNickName.setText(pickName);		//显示EditText内容
		
		btnSave = (Button) findViewById(R.id.btn_save_nick);
		
		/**
		 * 保存更改的昵称
		 */
		btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				SharedPreferences spSave = getSharedPreferences("savePick", MODE_PRIVATE);
				Editor edit = spSave.edit();
				edit.putString("save", etNickName.getText().toString());
				edit.commit();
				Toast.makeText(ChangeNickName.this, "保存成功", Toast.LENGTH_SHORT).show();
				
				finish();
				Intent intent = new Intent(ChangeNickName.this,Personal.class);
				startActivity(intent);
			}
		});
		
		
		ivBack = (ImageView) findViewById(R.id.iv_back);
		ivBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}
	
	
}
