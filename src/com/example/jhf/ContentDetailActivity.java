package com.example.jhf;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EncodingUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.day.event.ItemEveryYearEvent;
import com.example.global.Constant;
import com.example.monthstar.OneDatyPerson;
import com.example.monthstar.OneDayAdper;
import com.imooc.baseadapter.utils.Tools;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

/**
 * @author Administrator
 * 
 */
public class ContentDetailActivity extends Activity {
	private TextView tvTitle, tvTitle2;
	private ImageView ivBack;
	private ObjectMapper om = new ObjectMapper();
	private String textStr;// 每条item的标题
	private String code, str;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private OneDayAdper shoolDAdater;
	private WebView webView;
	private TextView tvDate;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.content_detail);
		ivBack = (ImageView) findViewById(R.id.shooll_GK);
		tvTitle = (TextView) findViewById(R.id.tv_content_title);
		tvTitle2 = (TextView) findViewById(R.id.tv_content_detail);
		tvDate = (TextView) findViewById(R.id.tv_content_detail_time);
		//Intent intent = getIntent();
		// String note = getIntent().getStringExtra("note");
		// String year = getIntent().getStringExtra("year");
		String stringExtra = getIntent().getStringExtra("onedaycontenid");
		if(!TextUtils.isEmpty(getIntent().getStringExtra("date"))){
			tvDate.setText(getIntent().getStringExtra("date").substring(0,10));
		}
		//tvTitle.setText(stringExtra);
		tvTitle.setText(stringExtra);
		tvTitle2.setText(stringExtra);
		//tvTitle.setText(title);
		//tvTitle2.setText(title);

		back();
		getJsonText();
		getStudentJSON();
	}

	/**
	 * 返回图标事件
	 */
	private void back() {
		ivBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}

	/**
	 * 每月百星
	 */
	private void getJsonText() {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();

					try {

						JSONObject jsonObject = new JSONObject(result);
						String str = jsonObject.get("data").toString();
						JSONObject jsonObject2 = new JSONObject(str);
						String noteStr = jsonObject2.get("note").toString();
						String mDate = jsonObject2.getString("date").substring(0,10);
						tvDate.setText(mDate);
						webView = (WebView) findViewById(R.id.wv_content);
						int a = 14; // 字体大小
						String strUrl = "<html> \n"
								+ "<head> \n"
								+ "<style type=\"text/css\"> \n"
								+ "body {text-align:justify; font-size: "
								+ a
								+ "px; line-height: "
								+ (a + 6)
								+ "px}\n"
								+ "</style> \n"
								+ "</head> \n"
								+ "<body>"
								+ EncodingUtils.getString(noteStr.getBytes(),
										"UTF-8") + "</body> \n </html>";
						webView.loadData(strUrl, "text/html; charset=UTF-8",
								null);

						jsonObject = new JSONObject(result);

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (msg.what == 6) {
					Toast.makeText(ContentDetailActivity.this, "帐号或密码错误", 0)
							.show();
				} else if (msg.what == 10) {
					Toast.makeText(ContentDetailActivity.this, "忘了联网了吧", 0)
							.show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				
				String sta_line_id = getIntent().getStringExtra("sta_line_id");
			
				// Log.v("star_id", sta_line_id);
				
				params.add(new BasicNameValuePair("note_id", sta_line_id));
				params.add(new BasicNameValuePair("db", Constant.db));
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				String result = Tools.sendGet(Constant.startMsg, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

	/**
	 * 获得来自学生作品信息的数据
	 */
	private void getStudentJSON() {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {

						JSONObject jsonObject = new JSONObject(result);
						String str = jsonObject.get("data").toString();
						JSONObject jsonObject2 = new JSONObject(str);
						String noteStr = jsonObject2.get("note").toString();
						// tvContent.setText(noteStr);
						webView = (WebView) findViewById(R.id.wv_content);
						int a = 14; // 字体大小
						String strUrl = "<html> \n"
								+ "<head> \n"
								+ "<style type=\"text/css\"> \n"
								+ "body {text-align:justify; font-size: "
								+ a
								+ "px; line-height: "
								+ (a + 6)
								+ "px}\n"
								+ "</style> \n"
								+ "</head> \n"
								+ "<body>"
								+ EncodingUtils.getString(noteStr.getBytes(),
										"UTF-8") + "</body> \n </html>";
						webView.loadData(strUrl, "text/html; charset=UTF-8",
								null);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (msg.what == 6) {
					Toast.makeText(ContentDetailActivity.this, "帐号或密码错误", 0)
							.show();
				} else if (msg.what == 10) {
					Toast.makeText(ContentDetailActivity.this, "忘了联网了吧", 0)
							.show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				Intent intent = getIntent();
				String work_id = intent.getStringExtra("id");
				
			
//				tvTitle.setText(stringExtra3);
//				tvTitle2.setText(stringExtra3);
				params.add(new BasicNameValuePair("db", Constant.db));
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("work_line_id", work_id));
				String result = Tools.sendGet(Constant.workLineMsg, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

	/**
	 * 获得来自nei的数据
	 */
	private void getDayStudentJSON() {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {

						JSONObject jsonObject = new JSONObject(result);
						String str = jsonObject.get("data").toString();
						JSONObject jsonObject2 = new JSONObject(str);
						String noteStr = jsonObject2.get("note").toString();
						// tvContent.setText(noteStr);
						webView = (WebView) findViewById(R.id.wv_content);
						int a = 14; // 字体大小
						String strUrl = "<html> \n"
								+ "<head> \n"
								+ "<style type=\"text/css\"> \n"
								+ "body {text-align:justify; font-size: "
								+ a
								+ "px; line-height: "
								+ (a + 6)
								+ "px}\n"
								+ "</style> \n"
								+ "</head> \n"
								+ "<body>"
								+ EncodingUtils.getString(noteStr.getBytes(),
										"UTF-8") + "</body> \n </html>";
						webView.loadData(strUrl, "text/html; charset=UTF-8",
								null);

						// data=new ArrayList<OneDatyPerson>();
						// jsonObject = new JSONObject(result);
						// jsonArray = jsonObject.getJSONArray("data");
						// for (int i = 0; i < jsonArray.length(); i++) {
						// jsonObject2 = (JSONObject) jsonArray.opt(i);
						// textStr = (String) jsonObject2.get("note");
						// System.out.println("textStr"+textStr);
						// tvContent.setText("\t\t\t\t" + textStr);
						// // OneDatyPerson shoolLITools = new OneDatyPerson();
						// // shoolLITools.setStrriqi(onedayconten);
						// // data.add(shoolLITools);
						// //tv.setText(onedayconten);
						// // OneDatyPerson shoolLITools = new OneDatyPerson();
						// // shoolLITools.setStrriqi(onedayconten);
						// // data.add(shoolLITools);
						// }
						// shoolDAdater = new
						// OneDayAdper(ItemEveryYearEvent.this, data);
						// lv.setAdapter(shoolDAdater);
						// shoolDAdater = new OneDayAdper(OneDay.this, data);
						// // 将添加的每条item放到Lstview中
						// lvshool.setAdapter(shoolDAdater);
						// lv.setOnItemClickListener(new OnItemClickListener() {
						//
						// @Override
						// public void onItemClick(AdapterView<?> arg0,
						// View arg1, int arg2, long arg3) {
						// try {
						// Intent intent = new Intent(ItemEveryYearEvent.this,
						// ContentDetailActivity.class);
						// JSONObject jsonObject3 = (JSONObject) jsonArray
						// .opt(arg2-1);
						// String sta_line_id = jsonObject3
						// .get("sta_line_id").toString();
						// intent.putExtra("sta_line_id", sta_line_id);
						// // SharedPreferences
						// // sp=getSharedPreferences("star_id",
						// // MODE_PRIVATE);
						// // Editor edit = sp.edit();
						// // edit.putString("star_id", start_id);
						// // edit.commit();
						// startActivity(intent);
						// } catch (Exception e) {
						// // TODO: handle exceptions
						// }
						//
						// }
						// });
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//
					// Toast.makeText(ItemEveryYearEvent.this, "登陆成功" +
					// onedayconten,
					// Toast.LENGTH_SHORT).show();

					// Intent intent = new Intent();
					// intent.setClass(ShoolJS.this, IndexActivity.class);
					// startActivity(intent);
				} else if (msg.what == 6) {
					Toast.makeText(ContentDetailActivity.this, "帐号或密码错误", 0)
							.show();
				} else if (msg.what == 10) {
					Toast.makeText(ContentDetailActivity.this, "忘了联网了吧", 0)
							.show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				Intent intent = getIntent();
				String work_id = intent.getStringExtra("id");
//				String stringExtra3 = intent.getStringExtra("onedaycontenid");
//				tvTitle.setText(stringExtra3);
//				tvTitle2.setText(stringExtra3);
				// Log.v("star_id", sta_line_id);
				params.add(new BasicNameValuePair("db", Constant.db));
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("work_line_id", work_id));
				String result = Tools.sendGet(Constant.workLineMsg, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}
}
