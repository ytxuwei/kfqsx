package com.example.centre;

import java.util.ArrayList;

import com.example.jhf.R;
import com.example.monthstar.OneDatyPerson;
import com.example.monthstar.OneDayAdper;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
/**
 * 最新消息通知
 * @author Administrator
 *
 */
public class MessageNew extends Activity{
	private ArrayList<OneDatyPerson> data;
	private String inc[]={"声音","通知显示消息内容","锁屏显示小时弹框","通知时指示灯闪烁","与我相关的状态"};
	private ListView lvshool;
	private OneDayAdper shoolDAdater;
	private OneDatyPerson oneDatyPerson;
	private ImageView shooll_GK;
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	requestWindowFeature(Window.FEATURE_NO_TITLE);
	setContentView(R.layout.xinxiaoxi);
	shooll_GK=(ImageView) findViewById(R.id.shooll_GK);
	shooll_GK.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			MessageNew.this.finish();
		}
	}); 
	lvshool=(ListView) findViewById(R.id.setup_XX);
	lvshool.setDividerHeight(0);
	data=new ArrayList<OneDatyPerson>();
	//循环添加数据
	 for (int i = 0; i < 5; i++) {
		  oneDatyPerson=new OneDatyPerson();
		 oneDatyPerson.setStrriqi(inc[i]);
		 data.add(oneDatyPerson);
	}
	//绑定adapter
	 shoolDAdater=new OneDayAdper(MessageNew.this, data);
	 lvshool.setAdapter(shoolDAdater);
}
}
