package com.example.monthstar;

import java.util.List;

import android.content.Context;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jhf.R;
import com.imooc.baseadapter.utils.CommonAdapter;
import com.imooc.baseadapter.utils.ViewHolderDemo;

/**
 * 每日百星baseAdapter
 * 
 * @author Administrator
 * 
 */
public class OneDayAdpterChange extends CommonAdapter<OneDatyPerson> {

	public OneDayAdpterChange(Context context, List<OneDatyPerson> datas) {
		super(context, datas);
		// TODO Auto-generated constructor stub
	}


	// 用于保持选中状态
	private SparseArray<View> vmap = new SparseArray<View>();

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderDemo holder = ViewHolderDemo.get(mContext, convertView,
				parent, R.layout.onedayitemchangetwo, position);
		OneDatyPerson onedatyperson = mDatas.get(position);
		// 方式一
		TextView tv = holder.getView(R.id.TV_tiemoneday_id);
		tv.setText(onedatyperson.getStrriqi());
		return holder.getConvertView();
	}

	// 添加数据
	public void addItem_OneDatyPerson(String value) {
		OneDatyPerson one = new OneDatyPerson();
		one.setStrriqi(value);
		mDatas.add(one);
	}
}
