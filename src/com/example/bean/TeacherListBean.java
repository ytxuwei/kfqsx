package com.example.bean;

import android.graphics.Bitmap;

public class TeacherListBean {
	public String img;		//头像
	public String name;	//姓名
	public String subject;	//课程
	public String post;	//职位
	public String phone;	//电话
	public int id;
	public TeacherListBean(String img, String name, String subject, String post,
			String phone,int id) {
		super();
		this.img = img;
		this.name = name;
		this.subject = subject;
		this.post = post;
		this.phone = phone;
		this.id = id;
	}
	
}
