package com.example.jhf;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.example.global.Constant;
import com.imooc.baseadapter.utils.Tools;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class ChangePassword extends Activity {
	private EditText signPassword;
	private EditText surePassword;
	private Button btnFinish;
	private ImageView ivBack;
	private String signPasswordStr = "";
	private String surePasswordStr = "";
	private ObjectMapper om = new ObjectMapper();
	private String code;
	private String phoneNumStr;// 电话

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.change_password);
		phoneNumStr = getIntent().getStringExtra("phoneNum_back");
		initView();

		/**
		 * 先判断两次输入的密码是否一致，如果一致并且不能低于6位，则将密码上传至服务器，跳转到主页
		 */
		btnFinish.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (isPasswordRight() && isPasswordAgr()) {
					if ((signPassword.getText().toString().trim())
							.equals(surePassword.getText().toString().trim())) {
						getJsonText();
						
					} else {
						Toast.makeText(getApplicationContext(), "两次密码输入不一致", 0)
								.show();
					}
				}
			}
		});

		ivBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}

	@SuppressLint("DefaultLocale")
	private boolean isPasswordAgr() {
		surePasswordStr = surePassword.getEditableText().toString().trim()
				.toLowerCase();
		if (!signPasswordStr.equalsIgnoreCase(surePasswordStr)) {
			surePassword.setError("两次输入的密码不一致！");
			return false;
		}
		return true;
		// TODO Auto-generated method stub

	}

	@SuppressLint("DefaultLocale")
	private boolean isPasswordRight() {
		// TODO Auto-generated method stub
		signPasswordStr = signPassword.getEditableText().toString().trim()
				.toLowerCase();
		Pattern p = Pattern.compile("^[0-9A-Za-z_]{6,16}$");
		Matcher m = p.matcher(signPasswordStr);
		if ("".equalsIgnoreCase(signPasswordStr)) {
			signPassword.setError("请输入密码");
			return false;
		} else if (!m.matches()) {// 如果密码中包含表达式中的特殊字符
			// Log.i("tag", "password:" + passwordStr);
			signPassword.setError("密码必须为6-16位字母、数字、下划线组成");
			return false;
		}
		return true;
	}

	private void initView() {
		// TODO Auto-generated method stub
		signPassword = (EditText) findViewById(R.id.et_new_pwd);
		surePassword = (EditText) findViewById(R.id.et_new_pwd2);
		btnFinish = (Button) findViewById(R.id.btn_finish);
		ivBack = (ImageView) findViewById(R.id.ver_mesBack);
	}

	/**
	 * 获取的JSON数据
	 */
	@SuppressLint("HandlerLeak")
	private void getJsonText() {
		final Handler handler = new Handler() {

			public void handleMessage(Message msg) {
				if ("200".equals(code)) {
					startActivity(new Intent(ChangePassword.this,
							MainActivity.class));
					Toast.makeText(ChangePassword.this, "修改成功", 0).show();
					finish();
				} else if ("300".equals(code)) {
					Toolss.disInfo(getApplicationContext(), "未注册");
				} else {
					Toolss.disInfo(getApplicationContext(), "修改失败");
				}

			}

		};
		new Thread() {
			public void run() {
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("login", phoneNumStr));// 手机号
				params.add(new BasicNameValuePair("password", surePasswordStr));
				String result = Tools.sendGet(Constant.changePasspword, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else if (code.equals("300")) {
							message.what = 11;
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}
}
