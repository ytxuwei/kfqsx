package com.example.bean;

/**
 * 发博客
 * @author Administrator
 *
 */
public class ShowBlogBean {
	public String parent_name;
	public String image;
	public String space_name;	//发表博客用户
	public String note;
	public String role;
	public String student_name;
	public String date;	
	public String portrait;
	public String id;	
	public String comment_count;
	public ShowBlogBean(String parent_name, String image, String space_name,
			String note, String role, String student_name, String date,
			String portrait, String id, String comment_count) {
		super();
		this.parent_name = parent_name;
		this.image = image;
		this.space_name = space_name;
		this.note = note;
		this.role = role;
		this.student_name = student_name;
		this.date = date;
		this.portrait = portrait;
		this.id = id;
		this.comment_count = comment_count;
	}	
	
	
}
