package com.example.space;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher.OnPhotoTapListener;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.android.volley.toolbox.Volley;
import com.example.bean.BlogContentImageBean;
import com.example.space.ZoomTutorial.OnZoomListener;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.SingleRequestQueue;
import com.king.photo.zoom.PhotoViewAttacher;
import com.king.photo.zoom.PhotoViewAttacher.OnViewTapListener;



/**
 * @author:Jack Tony
 * @tips :viewpager��������
 * @date :2014-11-12
 */
public class ClassAlbumQuViewPagerAdapter extends PagerAdapter {

	private ArrayList<BlogContentImageBean> sDrawables;
	private Context mContext;
	private ClassAlbumQueZoomTutorial mZoomTutorial;
	private RequestQueue queue;
	private ImageLoader imageLoader;

	public ClassAlbumQuViewPagerAdapter(Context context, ArrayList<BlogContentImageBean> imgIds,
			ClassAlbumQueZoomTutorial zoomTutorial) {
		this.sDrawables = imgIds;
		this.mContext = context;
		this.mZoomTutorial = zoomTutorial;

		queue = SingleRequestQueue.getRequestQueue(mContext);
		imageLoader = new ImageLoader(queue, new BitmapCache());
	}


	@Override
	public int getCount() {
		return sDrawables.size();
	}

	@Override
	public View instantiateItem(ViewGroup container, final int position) {
		final PhotoView imageView = new PhotoView(mContext);
		imageView.setOnPhotoTapListener(new OnPhotoTapListener() {
			
			@Override
			public void onPhotoTap(View view, float x, float y) {
				// TODO Auto-generated method stub
				mZoomTutorial.closeZoomAnim(position);
			}
		});
		ImageListener listener = ImageLoader.getImageListener(imageView, 0, 0);
		imageLoader.get(sDrawables.get(position).image, listener);
		container.addView(imageView, LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		return imageView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}
	
}