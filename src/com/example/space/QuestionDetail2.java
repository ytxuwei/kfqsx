package com.example.space;

import com.example.jhf.R;
import com.example.space.QuestionDetail.MyAdapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class QuestionDetail2 extends Activity{
	private ListView lvAnswer;
	private MyAdapter adapter;
	private TextView tvQuick;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.question_detail2);
		
		lvAnswer = (ListView) findViewById(R.id.lv_question_detail);
		tvQuick = (TextView) findViewById(R.id.tv_quick_question);
		tvQuick.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(QuestionDetail2.this,QuickQuestion.class);
				startActivity(intent);
			}
		});
		
		adapter = new MyAdapter();
		lvAnswer.setAdapter(adapter);
		
		ImageView ivBack = (ImageView) findViewById(R.id.shooll_GK);
		ivBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
	}
	
	class MyAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 2;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			View view = View.inflate(getApplicationContext(), R.layout.question_detail_item, null);
			return view;
		}
		
	}
}
