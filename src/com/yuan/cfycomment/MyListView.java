package com.yuan.cfycomment;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * 自定义控件
 * @since July 4, 2013
 * @author kzhang
 */
public class MyListView extends ListView {

	public MyListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public MyListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public MyListView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
//自定义子控件的大小
	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//长度模式
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}
}
