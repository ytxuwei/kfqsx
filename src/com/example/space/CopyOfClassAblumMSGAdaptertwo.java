package com.example.space;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.example.jhf.R;
import com.example.monthstar.OneDatyPerson;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.BitmapUtils;
import com.imooc.baseadapter.utils.CommonAdapter;
import com.imooc.baseadapter.utils.ViewHolderDemo;

public class CopyOfClassAblumMSGAdaptertwo extends CommonAdapter<OneDatyPerson> {
	private Context context;

	public CopyOfClassAblumMSGAdaptertwo(Context context,
			List<OneDatyPerson> datas) {
		super(context, datas);
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RequestQueue mQueue = Volley.newRequestQueue(context);

		ImageLoader imageLoader = new ImageLoader(mQueue, new BitmapCache());
		ViewHolderDemo viewHolder = ViewHolderDemo.get(mContext, convertView,
				parent, R.layout.classablummasgadapteritemtwo, position);
		OneDatyPerson oneDatyPerson = mDatas.get(position);
		NetworkImageView iView = viewHolder.getView(R.id.classblum_ivid);
		// iView.setBackground(BitmapUtils.convertBitmap2Drawable(oneDatyPerson
		// .getImage_View()));// 每张图片控制其大小
		iView.setImageUrl(oneDatyPerson.getStudentytiem(), imageLoader);
		if (selectPic == position) {
			iView.setScaleType(ScaleType.CENTER_CROP);
		} else {
			iView.setScaleType(ScaleType.CENTER_INSIDE);
		}
		return viewHolder.getConvertView();
	}
}
