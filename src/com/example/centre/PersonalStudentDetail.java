package com.example.centre;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jhf.R;

public class PersonalStudentDetail extends Activity{
	private TextView tvName;
	private TextView tvGender;
	private ImageView ivBack;
	private TextView tvClass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.personal_stduent_detail);
		tvName = (TextView) findViewById(R.id.tv_personal_student_name);
		tvGender = (TextView) findViewById(R.id.tv_personal_student_gender);
		tvClass = (TextView) findViewById(R.id.tv_personal_student_class);
		String name = getIntent().getStringExtra("studentName");
		tvName.setText(name);
		tvGender.setText(getIntent().getStringExtra("studentGender"));
		tvClass.setText(getIntent().getStringExtra("studentClass"));
		
		ivBack = (ImageView) findViewById(R.id.shooll_GK);
		ivBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}
}
