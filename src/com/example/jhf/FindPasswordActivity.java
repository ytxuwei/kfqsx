package com.example.jhf;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.global.Constant;
import com.imooc.baseadapter.utils.Tools;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * 更改密码
 * 
 * @author Administrator
 * 
 */
public class FindPasswordActivity extends Activity implements OnClickListener {

	private EditText etFindPwd;
	private Button btnFindPwd;
	private ImageView ivBack;
	private ObjectMapper om = new ObjectMapper();
	private String code;
	private String phoneNumStr = "";// 手机号
	private String str;// 修改是否成功；
	private String verCodeStrback;// 返回验证

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.find_password);

		initView();

		btnFindPwd.setOnClickListener(this);
		ivBack.setOnClickListener(this);
	}

	private void initView() {
		// TODO Auto-generated method stub
		etFindPwd = (EditText) findViewById(R.id.et_find_pwd);
		btnFindPwd = (Button) findViewById(R.id.btn_find_pwd);
		ivBack = (ImageView) findViewById(R.id.ver_mesBack);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.btn_find_pwd:
			if (isPhoneRight()) {
				getJsonText();

			} else {
				Toolss.disInfo(getApplicationContext(), "手机号有无吴");
			}

			break;
		case R.id.ver_mesBack:
			finish();
			break;
		default:
			break;
		}
	}

	/**
	 * 判断手机号
	 * 
	 * @return
	 */
	private boolean isPhoneRight() {
		phoneNumStr = etFindPwd.getEditableText().toString().trim();
		Pattern p = Pattern
				.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
		Matcher m = p.matcher(phoneNumStr);
		if ("".equalsIgnoreCase(phoneNumStr)) {
			etFindPwd.setError("请先输入您的手机号！");
			return false;
		} else if (!m.matches()) {
			etFindPwd.setError("请确认您输入了正确的手机号！");
			return false;
		} else {
			return true;
		}

		// TODO Auto-generated method stub

	}

	/**
	 * 获取的JSON数据
	 */
	@SuppressLint("HandlerLeak")
	private void getJsonText() {
		final Handler handler = new Handler() {

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						JSONObject jsonObject = new JSONObject(result);
						JSONObject jsonObject3 = jsonObject
								.getJSONObject("data");
						verCodeStrback = (String) jsonObject3
								.get("verificationCode");
						Intent intent = new Intent(FindPasswordActivity.this,
								ResetPassword.class);
						intent.putExtra("ihone_idback", verCodeStrback);
						intent.putExtra("ihone_Nab", phoneNumStr);
						startActivity(intent);
						finish();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (msg.what == 11) {
					String result = msg.obj.toString();
					try {
						JSONObject jsonObject = new JSONObject(result);
						JSONObject jsonObject3 = jsonObject
								.getJSONObject("data");
						verCodeStrback = (String) jsonObject3.get("error");

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Toast.makeText(FindPasswordActivity.this, verCodeStrback, 0)
							.show();
				} else if (msg.what == 10) {
					Toast.makeText(FindPasswordActivity.this, "忘了联网了吧", 0)
							.show();
				}

			}

		};
		new Thread() {
			public void run() {
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("phone", phoneNumStr));// 手机号
				params.add(new BasicNameValuePair("type", "changePwd"));
				String result = Tools.sendGet(Constant.verificationCodechange,
						params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else if (code.equals("400")) {
							message.what = 11;
							message.obj = result;
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}
}
