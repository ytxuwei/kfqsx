package com.example.jhf;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.centre.SetUp;
import com.example.global.Constant;
import com.example.space.Show;
import com.imooc.baseadapter.utils.Tools;

/**
 * 登录页面
 * 
 * @author Administrator
 * 
 */
@SuppressLint("HandlerLeak")
public class MainActivity extends ActionBarActivity {
	private Button mybutton, indexButton;
	private TextView tv;
	public String code;
	private EditText etPhone, etPassword;
	private String uid; // 用户uid

	private ObjectMapper om = new ObjectMapper();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		mybutton = (Button) findViewById(R.id.log_signUp);
		indexButton = (Button) findViewById(R.id.login);
		tv = (TextView) findViewById(R.id.forgetPassword);
		etPhone = (EditText) findViewById(R.id.log_phone);
		etPassword = (EditText) findViewById(R.id.log_password);

		tv.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);// 下划线
		mybutton.setOnClickListener(new OnClickListener() { // 注册

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent();
				intent.setClass(getApplicationContext(), yanzheng1.class);
				startActivity(intent);
			}
		});

		indexButton.setOnClickListener(new OnClickListener() { // 登录

					@Override
					public void onClick(View arg0) {
						/** 
						 * 得到输入的信息，判断是否为空
						 */
						final ProgressDialog progressDialog = ProgressDialog.show(MainActivity.this, "请稍等...", "正在登录...", true, true);
						// 点击不会消失
						progressDialog.setCanceledOnTouchOutside(false);
						if (TextUtils.isEmpty(etPhone.getText().toString()) || TextUtils.isEmpty(etPassword.getText().toString())) {
							Toast.makeText(MainActivity.this, "亲，忘了填写帐号密码了哦", 0).show();
						} else {
							final Handler handler = new Handler() {
								public void handleMessage(Message msg) {
									if (msg.what == 7) {
										String string = msg.obj.toString();
										Toast.makeText(MainActivity.this, "登陆成功", Toast.LENGTH_SHORT).show();
										Intent intent = new Intent();
										intent.setClass(MainActivity.this, IndexActivity.class);
										
										SharedPreferences userInfo = getSharedPreferences("userInfo", MODE_PRIVATE);
										Editor editorRember = userInfo.edit();
										editorRember.putString("username", etPhone.getText().toString());
										editorRember.putString("password", etPassword.getText().toString());
										editorRember.commit();
										
										SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
										Editor editor = sp.edit();
										editor.putString("uid", uid);
										editor.commit();

//										startActivity(intent);
										try {
											JSONObject obj = new JSONObject(string);
											// 这里还得解析用户的角色
											SharedPreferences spChmod = getSharedPreferences("chmod", MODE_PRIVATE);
											Editor chmodEdit = spChmod.edit();
											chmodEdit.putBoolean("isOrdinary", obj.getBoolean("is_ordinary")); // 普通用户
											chmodEdit.putBoolean("isDirector", obj.getBoolean("is_director")); // 级部主任
											chmodEdit.putBoolean("isOffice", obj.getBoolean("is_office")); // 教务处
											chmodEdit.putBoolean("isParent", obj.getBoolean("is_parent")); // 家长
											chmodEdit.putBoolean("isPresident", obj.getBoolean("is_president"));// 校长
											chmodEdit.putBoolean("isStudent", obj.getBoolean("is_student")); // 学生
											chmodEdit.putBoolean("isTeacher", obj.getBoolean("is_teacher")); // 老师
											chmodEdit.commit();
											SharedPreferences sp2 = getSharedPreferences("user", MODE_PRIVATE);
											Editor editor2 = sp2.edit();
											editor2.putString("isConfirm", obj.getString("is_confirm"));
											editor2.putString("userName", obj.getString("user_name"));
											editor2.putString("portrait", obj.getString("user_portrait"));
											editor2.commit();
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										startActivity(intent);
										finish();

									} 
									else if (msg.what == 6) {
										Toast.makeText(MainActivity.this, "登录失败", 0).show();
									}else if (msg.what == 8) {
										Toast.makeText(MainActivity.this, "您未注册！", 0).show();
									} else if (msg.what == 9) {
										Toast.makeText(MainActivity.this, "网络中断", 0).show();
									} else if (msg.what == 10) {
										Toast.makeText(MainActivity.this, "登录异常，请联系校方", 0).show();
									}

									progressDialog.dismiss();
								}
							};
							new Thread() {

								public void run() {
									// 设置参数
									List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
									params.add(new BasicNameValuePair("login", etPhone.getText().toString())); // 用户名
									params.add(new BasicNameValuePair("password", etPassword.getText().toString())); // 密码
									params.add(new BasicNameValuePair("db", Constant.db));// 数据库
									// 登陆
									String result = Tools.sendPost(Constant.login, params);
									Message message = new Message();
									try {
										if (result != null) {
											JsonNode jsonNode = om.readTree(result);
											code = jsonNode.get("code").asText();
											uid = jsonNode.get("uid").asText();

											if (code.equals("200")) {
												message.what = 7;
												message.obj = result;
												

											} else if (code.equals("300")) {
												message.what = 8;// 未注册
											} else {
												message.what = 6;// 登陆失败
											}
										} else {
											message.what = 9;// 无网络
										}
										handler.sendMessage(message);
									} catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
										message.what = 10;
										handler.sendMessage(message);
									}
								};
							}.start();
						}
					}
				});
		tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(getApplicationContext(), FindPasswordActivity.class);
				startActivity(intent);

			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		getSharedPreferences("userInfo", Context.MODE_PRIVATE).edit().clear().commit();
		getSharedPreferences("UserId", MODE_PRIVATE).edit().clear().commit();
		getSharedPreferences("chmod", MODE_PRIVATE).edit().clear().commit();
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);
			//System.exit(0);
			//finish();
			//Intent home = new Intent(Intent.ACTION_MAIN);
			//home.addCategory(Intent.CATEGORY_HOME);
			//startActivity(home);
			break;

		default:
			break;
		}

		return true;
	}

}
