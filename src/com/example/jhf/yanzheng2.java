package com.example.jhf;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.global.Constant;
import com.example.index.ShoolBroad;
import com.example.index.ShoolJS;
import com.imooc.baseadapter.utils.Tools;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * 设置密码
 * 
 * @author Administrator
 * 
 */
@SuppressLint("DefaultLocale")
public class yanzheng2 extends Activity {// OnFocusChangeListener
	private ImageView back;
	private EditText signPassword;
	private EditText surePassword;
	private Button complete;
	private String phoneNumStr;
	private String resultStr;
	private String signPasswordStr = "";
	private String surePasswordStr = "";
	private ObjectMapper om = new ObjectMapper();
	private String code;
	private String verCodePone;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.yanzhengtwo);
		back = (ImageView) findViewById(R.id.setP_mesBack);
		signPassword = (EditText) findViewById(R.id.sign_password);
		surePassword = (EditText) findViewById(R.id.sure_password);
		complete = (Button) findViewById(R.id.sign_complete);
		//phoneNumStr = getIntent().getStringExtra("phoneNum");
		//phoneNumStr = getIntent().getStringExtra("ihone_idback");
		phoneNumStr = getIntent().getStringExtra("ihone_Nab");//手机号
		complete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (isPasswordRight() && isPasswordAgr()) {
					getJsonText();
				}

			}
		});
		// back.setOnClickListener(this);
		// complete.setOnClickListener(this);
		// signPassword.setOnFocusChangeListener(this);
		// surePassword.setOnFocusChangeListener(this);
		// mybButton.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View arg0) {
		// Intent intent = new Intent();
		// intent.setClass(getApplicationContext(),
		// IndexActivity.class);
		// startActivity(intent);
		// }
		// });

	}

	// /**
	// * 注册与返回事件
	// */
	// @Override
	// public void onClick(View view) {
	// // TODO Auto-generated method stub
	// if (view.getId() == R.id.setP_mesBack) {// 返回
	// finish();
	// } else if (view.getId() == R.id.sign_complete)// 注册
	// {
	// signUp();
	// }
	// }

	/**
	 * 组成判断
	 */
	private void signUp() {
		// TODO Auto-generated method stub
		// resultStr = "注册失败";
		if (!isPasswordRight())
			return;
		if (!isPasswordAgr())
			return;
		getJsonText();
		if ("200".equals(code)) {
			startActivity(new Intent(yanzheng2.this, MainActivity.class));
			Toast.makeText(yanzheng2.this, "注册成功", 0).show();
		} else if ("300".equals(code)) {
			Toolss.disInfo(getApplicationContext(), "改账号已经被注册");
		} else {
			Toolss.disInfo(getApplicationContext(), "注册失败");
		}
		// JSONObject userJSON = new JSONObject();
		// try {
		// userJSON.put("userName", phoneNumStr);
		// userJSON.put("password", surePasswordStr);
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// resultStr = "注册成功";
		// if ("注册成功".equalsIgnoreCase(resultStr)) {
		// Intent intent = new Intent();
		// intent.putExtra("phoneNum", phoneNumStr);
		// // startActivity(yanzheng2.this,IndexActivity.class);
		// intent.setClass(getApplicationContext(), IndexActivity.class);
		// // startActivity(intent);
		// setResult(RESULT_OK, intent);
		// Log.i("tag", "注册成功！");
		// Toolss.disInfo(this, "注册成功");
		// finish();
		// } else {
		// // Toolss.disInfo(this, "测试失败");
		// }

	}

	// @Override
	// public void onFocusChange(View view, boolean hasFocus) {
	// // TODO Auto-generated method stub
	//
	// if (view.getId() == R.id.sign_password) {
	//
	// if (!hasFocus) {
	// isPasswordRight();
	//
	// }
	// } else {
	// if (hasFocus) {
	// if ("".equalsIgnoreCase(signPasswordStr)) {// 判断密码是否为空
	// signPassword.setError("请输入密码！");
	// }
	// } else if (!hasFocus) {// 失去焦点是判断两次输入的密码是否一致
	// isPasswordAgr();
	//
	// }
	// }
	// }
	@SuppressLint("DefaultLocale")
	private boolean isPasswordAgr() {
		surePasswordStr = surePassword.getEditableText().toString().trim()
				.toLowerCase();
		if (!signPasswordStr.equalsIgnoreCase(surePasswordStr)) {
			surePassword.setError("两次输入的密码不一致！");
			return false;
		}
		return true;
		// TODO Auto-generated method stub

	}

	@SuppressLint("DefaultLocale")
	private boolean isPasswordRight() {
		// TODO Auto-generated method stub
		signPasswordStr = signPassword.getEditableText().toString().trim()
				.toLowerCase();
		Pattern p = Pattern.compile("^[0-9A-Za-z_]{6,16}$");
		Matcher m = p.matcher(signPasswordStr);
		if ("".equalsIgnoreCase(signPasswordStr)) {
			signPassword.setError("请输入密码");
			return false;
		} else if (!m.matches()) {// 如果密码中包含表达式中的特殊字符
			// Log.i("tag", "password:" + passwordStr);
			signPassword.setError("密码必须为6-16位字母、数字、下划线组成");
			return false;
		}
		return true;
	}

	/**
	 * 获取的JSON数据
	 */
	@SuppressLint("HandlerLeak")
	private void getJsonText() {
		final Handler handler = new Handler() {

			public void handleMessage(Message msg) {
				if ("200".equals(code)) {
					startActivity(new Intent(yanzheng2.this,
							IndexActivity.class));
					Toast.makeText(yanzheng2.this, "注册成功", 0).show();
					finish();
				} else if ("300".equals(code)) {
					Toolss.disInfo(getApplicationContext(), "改账号已经被注册");
				} else {
					Toolss.disInfo(getApplicationContext(), "注册失败");
				}

			}

		};
		new Thread() {
			public void run() {
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("login", phoneNumStr));// 手机号
				params.add(new BasicNameValuePair("password", surePasswordStr));
				String result = Tools.sendGet(Constant.register, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else if (code.equals("300")) {
							message.what = 11;
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}
}
