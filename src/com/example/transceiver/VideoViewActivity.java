package com.example.transceiver;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.widget.MediaController;

import com.example.jhf.R;
import com.example.view.FullScreenVideoView;

/**
 * У԰��Ƶ
 * 
 * @author Administrator
 * 
 */
public class VideoViewActivity extends Activity {

	// private String url =
	// "http://flv2.bn.netease.com/videolib3/1509/25/aAtUd1296/SD/aAtUd1296-mobile.mp4";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.vv_activity);
		FullScreenVideoView mVideo = (FullScreenVideoView) findViewById(R.id.vv_act);
		String url = getIntent().getStringExtra("URLStr_School_TV_msg");
		Uri uri = Uri.parse(url);
		mVideo.setMediaController(new MediaController(this));
		mVideo.setVideoURI(uri);
		mVideo.start();
		mVideo.requestFocus();

	}

}
