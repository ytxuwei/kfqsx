package com.king.photo.activity;

import java.util.ArrayList;
import java.util.List;

import com.example.jhf.R;
import com.example.view.MyGridView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdpterSpinner2 extends BaseAdapter {
	private ArrayList<CItem> data;
	private Context context;
	private LayoutInflater layoutInflater;

	public AdpterSpinner2(Context context, ArrayList<CItem> adta) {
		this.context = context;
		layoutInflater = LayoutInflater.from(context);
		this.data = adta;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.spinner_item, null);
			viewHolder = new ViewHolder();
			viewHolder.kemu = (TextView) convertView
					.findViewById(R.id.tv_sperinner);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.kemu.setText(data.get(position).getValue());
		return convertView;
		
	}

	public class ViewHolder {
		// public TextView answerName;
		public TextView kemu;

	}
}
