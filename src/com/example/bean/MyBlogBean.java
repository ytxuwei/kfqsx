package com.example.bean;

import java.util.ArrayList;

import android.graphics.Bitmap;

/**
 * @author Administrator
 *
 */
public class MyBlogBean {
	public String mImgId;		//图片
	public String mContent;	//内容
	public String mCount;		//数量
	public String mDate;		//日期
	public String id;			//space_id
	public MyBlogBean(String mImgId, String mContent,
			String mCount, String id, String mDate) {
		super();
		this.mImgId = mImgId;
		this.mContent = mContent;
		this.mCount = mCount;
		this.mDate = mDate;
		this.id = id;
	}
	@Override
	public String toString() {
		return "MyBlogBean [mImgId=" + mImgId + ", mContent=" + mContent
				+ ", mCount=" + mCount + ", mDate=" + mDate + "]";
	}
	
	
}
