package com.example.jhf;

import org.codehaus.jackson.map.ObjectMapper;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.RelativeLayout;

import com.imooc.baseadapter.utils.PrefUtils;

public class SplashActivity extends Activity {
	private RelativeLayout rlRoot;
	private String username;
	private String password;
	private ObjectMapper om = new ObjectMapper();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash_acitivity);
		rlRoot = (RelativeLayout) findViewById(R.id.rl_root);
		initAnim();
	}

	/**
	 * 初始化动画
	 */
	private void initAnim() {
		AnimationSet set = new AnimationSet(false);

		// 旋转动画
		/*RotateAnimation rotate = new RotateAnimation(0, 360,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		rotate.setDuration(1000);
		rotate.setFillAfter(true);*/

		// 缩放动画
		ScaleAnimation scale = new ScaleAnimation(0, 1, 0, 1,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		scale.setDuration(1000);
		scale.setFillAfter(true);

		// 透明度
		AlphaAnimation alpha = new AlphaAnimation(0, 1);
		alpha.setDuration(2000);
		alpha.setFillAfter(true);

		//set.addAnimation(rotate);
		set.addAnimation(scale);
		set.addAnimation(alpha);

		set.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			// 监听动画结束
			@Override
			public void onAnimationEnd(Animation animation) {
				jumpToNextPage();
			}
		});

		rlRoot.startAnimation(set);
	}

	/**
	 * 跳转到下一个页面
	 */
	private void jumpToNextPage() {
		SharedPreferences userInfo = getSharedPreferences("userInfo",
				MODE_PRIVATE);
		username = userInfo.getString("username", "");
		password = userInfo.getString("password", "");
		boolean isGuideShow = PrefUtils
				.getBoolean(this, "is_guide_show", false);

		Intent intent = new Intent();
		if (!isGuideShow) {// 如果没有展现过新手引导,跳引导页
			intent.setClass(this, GuideActivity.class);
		} else {// 跳主页面
			// 验证username，password能否都登录成功
			if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
				intent.setClass(SplashActivity.this, MainActivity.class);
			} else {
				intent.setClass(SplashActivity.this,IndexActivity.class);
			}
		}
		startActivity(intent);

		finish();

	}
}
