package com.imooc.baseadapter.utils;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageCache;
import com.android.volley.toolbox.Volley;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

public class SingleRequestQueue {
	private static RequestQueue mQueue;
	public static ImageLoader imageLoader;
	private SingleRequestQueue(Context context){
		mQueue = Volley.newRequestQueue(context);
		imageLoader = new ImageLoader(mQueue,new BitmapCache());
		
	}
	public static synchronized RequestQueue getRequestQueue(Context context){
		if(mQueue == null){
			new SingleRequestQueue(context.getApplicationContext());
		}
		return mQueue;
	}
	public static synchronized ImageLoader getImageLoader(Context context){
		if(imageLoader == null){
			new SingleRequestQueue(context.getApplicationContext());
		}
		return imageLoader;
	}
}
