package com.example.space;

import java.util.ArrayList;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.example.jhf.R;
import com.facebook.drawee.view.SimpleDraweeView;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.SingleRequestQueue;

public class ClassAlbumAdapter extends BaseAdapter {
	private ArrayList<ClassAlbumPer> data;
	private Context context;
	private LayoutInflater inflater;

	public ClassAlbumAdapter(Context context, ArrayList<ClassAlbumPer> data) {
		this.context = context;
		this.inflater = LayoutInflater.from(context);
		this.data = data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		final ViewHolder viewHolder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.classalbumitem, null);
			viewHolder = new ViewHolder();
			viewHolder.classtuBitmap = (SimpleDraweeView) convertView.findViewById(R.id.classxc_idid);
			viewHolder.classtubiao = (TextView) convertView.findViewById(R.id.classtutm_idid);
			viewHolder.classzhuozhe = (TextView) convertView.findViewById(R.id.classzz_idid);
			viewHolder.classtutiem = (TextView) convertView.findViewById(R.id.classtiem_id);
			viewHolder.classtuNum = (TextView) convertView.findViewById(R.id.classcs_id);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		// 为了防止错位，固定其大小
		viewHolder.classtuBitmap.setScaleType(ImageView.ScaleType.FIT_XY);
		// viewHolder.classtuBitmap.setDefaultImageResId(R.drawable.default_image);
		// viewHolder.classtuBitmap.setErrorImageResId(R.drawable.failed_image);
		/*if (data.get(position).getUrlStr().equals("")) {
			viewHolder.classtuBitmap.setBackgroundResource(R.drawable.ic_launcher);
			// viewHolder.classtuBitmap.setImageDrawable();
			// viewHolder.classtuBitmap.setDefaultImageResId(R.drawable.ic_launcher);
			// viewHolder.classtuBitmap.setErrorImageResId(R.drawable.ic_launcher);
		} else {
			viewHolder.classtuBitmap.setImageUrl(data.get(position).getUrlStr(), imageLoader);// 转化为图片
		}*/
		//viewHolder.classtuBitmap.setImageUrl(data.get(position).getUrlStr(), imageLoader);// 转化为图片
		viewHolder.classtuBitmap.setImageURI(Uri.parse(data.get(position).getUrlStr()));
		// viewHolder.classtuBitmap.setImageUrl(
		// data.get(position).getUrlStr(), imageLoader);// 转化为图片
		viewHolder.classtubiao.setText(data.get(position).getClasstubiao());
		viewHolder.classzhuozhe.setText(data.get(position).getClasszhuozhe());
		viewHolder.classtutiem.setText(data.get(position).getClasstutiem());
		viewHolder.classtuNum.setText(data.get(position).getClasstuNum());
		// viewHolder.classtuBitmap.setImageBitmap(data.get(position)
		// .getImageView());
		return convertView;
	}

	public static class ViewHolder {
		SimpleDraweeView classtuBitmap;
		TextView classtubiao;
		TextView classzhuozhe;
		TextView classtutiem;
		TextView classtuNum;
	}
}
