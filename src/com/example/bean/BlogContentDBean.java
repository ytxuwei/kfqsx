package com.example.bean;

import java.util.ArrayList;

import android.graphics.Bitmap;

/**
 * ��������
 * @author Administrator
 *
 */
public class BlogContentDBean {
	public String parent_name;
	public String student_name;
	public String date;
	public String note;
	public String role;
	public String portrait;
	public String id;
	public String comment_count;
	public String space_name;
	public String image;
	public ArrayList<BlogContentCommitBean> commit;
	
	
	public BlogContentDBean(String parent_name, String student_name,
			String date, String note, String role,String portrait, String id,
			String comment_count, String space_name,String image,
			ArrayList<BlogContentCommitBean> commit) {
		super();
		this.parent_name = parent_name;
		this.student_name = student_name;
		this.date = date;
		this.note = note;
		this.role = role;
		this.portrait = portrait;
		this.id = id;
		this.comment_count = comment_count;
		this.image = image;
		this.space_name = space_name;
		this.commit = commit;
	}

}
