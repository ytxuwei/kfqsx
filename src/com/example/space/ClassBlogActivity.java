package com.example.space;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AbsListView.RecyclerListener;

import com.android.volley.RequestQueue;
import com.example.bean.BlogContentCommitBean;
import com.example.bean.BlogContentDBean;
import com.example.bean.BlogContentImageBean;
import com.example.bean.BlogJsonBean;
import com.example.bean.BlogJsonBean.Data;
import com.example.bean.BlogJsonBean.Data.DataComment;
import com.example.bean.BlogJsonBean.Data.DataImage;
import com.example.bean.BlogUserBean;
import com.example.global.Constant;
import com.example.global.MyApplication;
import com.example.jhf.R;
import com.example.view.DropHtml;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.imooc.baseadapter.utils.Tools;

public class ClassBlogActivity extends Activity implements OnRefreshListener2<ListView> {
	private ImageView shooll_GK;
	// private ListView mListView;
	private TextView tvCount;
	private SimpleDraweeView mImgView;
	private TextView userName;

	private ArrayList<BlogUserBean> blogUserDatas; // 博客用户
	private ArrayList<BlogContentDBean> blogDatas; // 博客内容
	private ArrayList<BlogContentImageBean> blogImages; // 博客图片
	private ArrayList<BlogContentCommitBean> blogCommits; // 博客回复
	private MyAdapter adapter;
	// private RefreshLayout swipeLayout;
	private PullToRefreshListView mListView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.classblong);

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("action.refreshFriend");
		registerReceiver(mRefreshBroadcastReceiver, intentFilter);
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		shooll_GK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ClassBlogActivity.this.finish();
			}
		});

		blogUserDatas = new ArrayList<BlogUserBean>();
		blogDatas = new ArrayList<BlogContentDBean>();

		getJsonText(1);

		initView();

		mImgView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ClassBlogActivity.this, MyBlog.class);
				startActivity(intent);
			}
		});

		adapter = new MyAdapter();
		mListView.setAdapter(adapter);
	}

	/*
	 * private void setListener() { swipeLayout.setOnRefreshListener(this);
	 * swipeLayout.setOnLoadListener(this); }
	 */

	private void initView() {
		// TODO Auto-generated method stub
		// mListView = (ListView) findViewById(R.id.lv_blog);
		mListView = (PullToRefreshListView) findViewById(R.id.lv_blog);

		tvCount = (TextView) findViewById(R.id.tv_leave_msg_count);
		View headerView = View.inflate(getApplicationContext(), R.layout.header_dynamic, null);
		userName = (TextView) headerView.findViewById(R.id.tv_header_dynamic_username);
		mImgView = (SimpleDraweeView) headerView.findViewById(R.id.civ_header_dynamic);

		refreshableView = mListView.getRefreshableView();
		refreshableView.addHeaderView(headerView, null, false);

		ImageView camera = (ImageView) findViewById(R.id.iv_camera_list);
		camera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ClassBlogActivity.this, Show.class);
				// intent.setAction("action.refresh");
				startActivity(intent);
				// startActivityForResult(intent, 1);
			}
		});

		mListView.setOnRefreshListener(this);

	}

	class MyAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return blogDatas.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return blogDatas.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {

			ViewHolder holder = null;
			if (convertView == null) {
				convertView = View.inflate(ClassBlogActivity.this, R.layout.item_blog, null);
				holder = new ViewHolder();
				holder.parentImg = (SimpleDraweeView) convertView.findViewById(R.id.civ_item_blog);
				// 标题
				holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_blog);
				// 內容
				holder.tvContent = (TextView) convertView.findViewById(R.id.tv_blog_cont);
				holder.llContent = (LinearLayout) convertView.findViewById(R.id.ll_blog_content);
				// 学生姓名
				holder.tvStudent = (TextView) convertView.findViewById(R.id.tv_item_blog_student_name);
				// 日期
				holder.tvDate = (TextView) convertView.findViewById(R.id.tv_item_blog_date);
				// 留言条数
				holder.tvMsg = (TextView) convertView.findViewById(R.id.tv_leave_msg_count);
				// 图片
				holder.ivBlog = (SimpleDraweeView) convertView.findViewById(R.id.iv_item_blog_photo);
				// 留言内容
				holder.tvLeaveMsg = (TextView) convertView.findViewById(R.id.tv_item_blog_leave_msg);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			// parentImg.buildDrawingCache();
			// parentImg.setDrawingCacheEnabled(false);

			// ImageRequest request =
			// ImageRequestBuilder.newBuilderWithSource(Uri.parse(blogDatas.get(arg0).portrait)).setProgressiveRenderingEnabled(true).build();
			// DraweeController controller =
			// Fresco.newDraweeControllerBuilder().setImageRequest(request).build();
			// PipelineDraweeController controller = (PipelineDraweeController)
			// Fresco.newDraweeControllerBuilder().setImageRequest(request).setOldController(parentImg.getController()).build();
			// parentImg.setController(controller);

			holder.parentImg.setImageURI(Uri.parse(blogDatas.get(position).portrait));

			holder.tvTitle.setText(blogDatas.get(position).space_name);

			String content = blogDatas.get(position).note.toString();
			holder.tvContent.setText(Html.fromHtml(DropHtml.drop(content)));

			holder.llContent.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					// TODO Auto-generated method stub

					Intent intent = new Intent(ClassBlogActivity.this, ClassBlogDetail.class);
					intent.putExtra("spaceId", blogDatas.get(position).id);
					startActivity(intent);
				}
			});

			holder.tvStudent.setText(blogDatas.get(position).student_name + blogDatas.get(position).role);

			holder.tvDate.setText(blogDatas.get(position).date);

			holder.tvMsg.setText(blogDatas.get(position).comment_count);

			if (!TextUtils.isEmpty(blogDatas.get(position).image)) {
				holder.ivBlog.setImageURI(Uri.parse(blogDatas.get(position).image));
				holder.ivBlog.setVisibility(View.VISIBLE);
			}

			StringBuilder sb = new StringBuilder();

			if (blogDatas.get(position).commit.get(0).comment_note.equals("")) {
				holder.tvLeaveMsg.setVisibility(View.GONE);
			}
			for (int i = 0; i < blogDatas.get(position).commit.size(); i++) {
				sb.append(DropHtml.drop(blogDatas.get(position).commit.get(i).comment_user) + ":" + DropHtml.drop(blogDatas.get(position).commit.get(i).comment_note));
				if (i >= 0 && i < blogDatas.get(position).commit.size() - 1) {
					sb.append("\n");
				}
			}

			holder.tvLeaveMsg.setText(sb);

			adapter.notifyDataSetChanged();
			return convertView;
		}

		public class ViewHolder {
			SimpleDraweeView parentImg;
			TextView tvTitle;
			TextView tvContent;
			LinearLayout llContent;
			TextView tvStudent;
			TextView tvDate;
			TextView tvMsg;
			SimpleDraweeView ivBlog;
			// 留言内容
			TextView tvLeaveMsg;
		}

	}

	private void getJsonText(final int i) {
		final Handler handler = new Handler() {

			private String image;
			private String commentNote;
			private String commentId;
			private String commentUser;
			private int imageId;

			public void handleMessage(Message msg) {
				String result = msg.obj.toString();

				Gson gson = new Gson();
				BlogJsonBean json = gson.fromJson(result, BlogJsonBean.class);
				String userPortrait = json.space_user_portrait;
				String pageCount = json.page_count;
				String spaceUserName = json.space_user_name;
				String spaceUserId = json.space_user_id;
				/*
				 * Bitmap userPortraitBitmap = BitmapUtils
				 * .stringtoBitmap(userPortrait);
				 */
				if (i == 1) {
					blogUserDatas.add(new BlogUserBean(userPortrait, pageCount, spaceUserName, spaceUserId));
					userName.setText(blogUserDatas.get(0).space_user_name);
					// mImgView.setImageUrl(userPortrait, imageLoader);
					mImgView.setImageURI(Uri.parse(userPortrait));
				}
				ArrayList<Data> data = json.data;
				for (int i = 0; i < data.size(); i++) {
					String parentName = data.get(i).parent_name;
					blogImages = new ArrayList<BlogContentImageBean>();
					ArrayList<DataImage> dataImage = data.get(i).data_image;
					if (dataImage.size() != 0) {
						for (int j = 0; j < dataImage.size(); j++) {
							imageId = dataImage.get(j).image_id;
							image = dataImage.get(0).image;
							// Bitmap blogImage =
							// BitmapUtils.stringtoBitmap(image);
							blogImages.add(new BlogContentImageBean(imageId, image));
						}
					} else {
						imageId = 0;
						image = "";
						blogImages.add(new BlogContentImageBean(imageId, image));
					}
					String spaceName = data.get(i).space_name;
					String studentName = data.get(i).student_name;
					String date = data.get(i).date;
					String note = data.get(i).note;
					String role = data.get(i).role;
					String portrait = data.get(i).portrait;
					String id = data.get(i).id;
					String commentCount = data.get(i).comment_count;
					ArrayList<DataComment> dataComment = data.get(i).data_comment;
					blogCommits = new ArrayList<BlogContentCommitBean>();
					if (dataComment.size() != 0) {
						for (int k = 0; k < dataComment.size(); k++) {
							commentNote = dataComment.get(k).comment_note;
							commentId = dataComment.get(k).comment_id;
							commentUser = dataComment.get(k).comment_user;
							blogCommits.add(new BlogContentCommitBean(commentNote, commentId, commentUser));
						}
					} else {
						commentNote = "";
						commentId = "";
						commentUser = "";
						blogCommits.add(new BlogContentCommitBean(commentNote, commentId, commentUser));
					}

					blogDatas.add(new BlogContentDBean(parentName, studentName, date, note, role, portrait, id, commentCount, spaceName, image, blogCommits));
				}

				adapter.notifyDataSetChanged();
				// mQueue.add(imagePortrait);
			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
				String uid = sp.getString("uid", "0");

				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // uid
				params.add(new BasicNameValuePair("page", i + ""));
				params.add(new BasicNameValuePair("page_count", "5"));
				String result = Tools.sendGet(Constant.classBlogList, params);
				Message message = new Message();
				message.what = 7;
				message.obj = result;
				handler.sendMessage(message);
			};
		}.start();
	}

	int i = 2;

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(mRefreshBroadcastReceiver);
	}

	private BroadcastReceiver mRefreshBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals("action.refreshFriend")) {
				blogDatas.clear();
				getJsonText(1);
				i = 2;
				mListView.onRefreshComplete();
			}

		}
	};
	private MyApplication myApp;
	private ListView refreshableView;

	@Override
	public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				blogDatas.clear();
				getJsonText(1);
				i = 2;
				mListView.onRefreshComplete();
			}
		}, 3000);
	}

	@Override
	public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				getJsonText(i);
				i++;
				mListView.onRefreshComplete();
			}
		}, 3000);
	}

}
