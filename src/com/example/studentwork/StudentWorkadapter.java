package com.example.studentwork;

import java.util.List;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.index.ShoolLITools;
import com.example.index.ShoolLIAdpter.ViewHolder;
import com.example.jhf.R;

public class StudentWorkadapter extends BaseAdapter {
	private Context context;
	private List<ShoolLITools> data;
	//private ImageView imageView;
	private ShoolLITools schoolLiTools =new ShoolLITools();
	public StudentWorkadapter(Context context, List<ShoolLITools> data
			) {
		this.context = context;
		this.data=data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	// 用于保持选中状态
		private SparseArray<View> vmap = new SparseArray<View>();
	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		final ViewHolder viewHolder;
		if (vmap.get(position)==null) {
			convertView=LayoutInflater.from(context).inflate(R.layout.shoollsitem, null);
			viewHolder = new ViewHolder();
			viewHolder.TVshoolli=(TextView) convertView.findViewById(R.id.TVshoolli_shoolsitem);
			viewHolder.tvDate = (TextView) convertView.findViewById(R.id.TVshoolli_texw);
			//imageView=(ImageView) convertView.findViewById(R.id.xiala_IV);
			convertView.setTag(viewHolder);
			vmap.put(position, convertView);
		} else {
			convertView = vmap.get(position);
			viewHolder = (ViewHolder) convertView.getTag();
		}
//		if (position==0) {
//			imageView.setVisibility(View.VISIBLE);
//		} else {
//			imageView.setVisibility(View.GONE);
//		}
     viewHolder.TVshoolli.setText(data.get(position).getStr_shoolli().toString());
     viewHolder.tvDate.setText(data.get(position).getmDate());
		return convertView;
	}

	public final class ViewHolder {
		private TextView TVshoolli;
		private TextView tvDate;
	}
	
}
 