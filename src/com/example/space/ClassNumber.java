package com.example.space;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.bean.CourseBean;
import com.example.global.Constant;
import com.example.jhf.R;
import com.example.view.ListViewIndicatorUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.imooc.baseadapter.utils.Tools;

public class ClassNumber extends Activity implements OnItemClickListener {
	private TextView tvTitle; // 标题
	private ArrayList<CourseBean> datas;
	private MyAdapter adapter; // listView的适配器
	private ImageView ivBack;
	private PullToRefreshListView mPullRefreshListView;
	private int page;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.item_everyyear);

		initView();
		datas = new ArrayList<CourseBean>();
		getJsonText(1);
		/*datas.add("五年级一班");
		datas.add("五年级二班");
		datas.add("五年级三班");
*/
		adapter = new MyAdapter();
		mPullRefreshListView.setAdapter(adapter);

		mPullRefreshListView.setOnItemClickListener(this);

		ivBack = (ImageView) findViewById(R.id.iv_back);
		ivBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});

	}

	private void initView() {
		tvTitle = (TextView) findViewById(R.id.tv_everyyear);
		tvTitle.setText("班级列表");
		mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.class_number_list);
		mPullRefreshListView.setMode(Mode.BOTH);
		ListViewIndicatorUtil.initIndicator(mPullRefreshListView);
		
		mPullRefreshListView.setOnRefreshListener(new com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullDownToRefresh");

				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						datas.clear();
						getJsonText(1);
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullUpToRefresh");
				// 这里写上拉加载更多的任务

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						getJsonText(page);
						page++;
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);

			}

		});
	}

	/**
	 * listView的适配器
	 */
	public class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return datas.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return datas.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			View view = View.inflate(getApplicationContext(),
					R.layout.item_of_general, null);
			TextView tv = (TextView) view
					.findViewById(R.id.tv_item_general_title);
			tv.setText(datas.get(arg0).name);
			return view;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(ClassNumber.this, TeacherList.class);
		intent.putExtra("classId", datas.get(arg2-1).id);
		startActivity(intent);

	}

	private void getJsonText(final int i) {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				String result = msg.obj.toString();
				try {
					JSONObject jsonObject = new JSONObject(result);
					JSONArray jsonArray = jsonObject.getJSONArray("data");
					for (int i = 0; i < jsonArray.length(); i++) {
						jsonObject2 = (JSONObject) jsonArray.opt(i);
						String contentName = (String) jsonObject2.get("class_name");
						int contentId = (Integer) jsonObject2.get("id");
						datas.add(new CourseBean(contentId, contentName));
						// datas.add(new Notices(contentName, contentId));
					}
					adapter = new MyAdapter();
					// 将添加的每条item放到Lstview中
					mPullRefreshListView.setAdapter(adapter);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); //uid
				params.add(new BasicNameValuePair("page", i+""));
				params.add(new BasicNameValuePair("page_count", "5"));
				String result = Tools.sendGet(Constant.memberClass, params);
				Message message = new Message();
				message.what = 7;
				message.obj = result;
				handler.sendMessage(message);
			};
		}.start();
	}

}
