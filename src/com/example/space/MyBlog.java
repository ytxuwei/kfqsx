package com.example.space;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.example.bean.MyBlogBean;
import com.example.bean.MySpaceJsonBean;
import com.example.bean.MySpaceJsonBean.SpaceData;
import com.example.bean.MySpaceJsonBean.SpaceData.SpaceItemData;
import com.example.bean.MySpaceJsonBean.SpaceData.SpaceItemData.SpaceImage;
import com.example.global.Constant;
import com.example.global.MyApplication;
import com.example.jhf.R;
import com.example.view.CircleImageView;
import com.example.view.DropHtml;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.SingleRequestQueue;
import com.imooc.baseadapter.utils.Tools;

public class MyBlog extends Activity implements OnClickListener, OnItemClickListener {
	private ListView mListView;
	private MyAdapter adapter;
	private ImageView ivBack;
	private TextView tvSpaceName;
	// private CircleImageView ivSpaceImg;
	private SimpleDraweeView ivSpaceImg;
	private ArrayList<MyBlogBean> datas; // 条目数据
	// private ArrayList<String> dates; // 日期
	// private ArrayList<String> itemDatas; // 单纯的内容
	private ImageView ivCamera;
	private String uid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.my_blog);
		SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
		uid = sp.getString("uid", "0");

		datas = new ArrayList<MyBlogBean>();
		// dates = new ArrayList<String>();
		// itemDatas = new ArrayList<String>();
		initView();

		getJsonText();
		adapter = new MyAdapter();
		mListView.setAdapter(adapter);
	}

	private void initView() {
		mListView = (ListView) findViewById(R.id.lv_my_blog);
		ivBack = (ImageView) findViewById(R.id.iv_my_blog);
		View headerView = View.inflate(getApplicationContext(), R.layout.header_dynamic, null);
		ImageView ivHeader = (ImageView) headerView.findViewById(R.id.iv_header_dynamic);
		ivHeader.setBackgroundResource(R.drawable.header_dynamic2);

		tvSpaceName = (TextView) headerView.findViewById(R.id.tv_header_dynamic_username);
		ivSpaceImg = (SimpleDraweeView) headerView.findViewById(R.id.civ_header_dynamic);
		mListView.addHeaderView(headerView, null, false);
		View headerView2 = View.inflate(getApplicationContext(), R.layout.header_my_blog_item, null);
		mListView.addHeaderView(headerView2);
		ivCamera = (ImageView) headerView2.findViewById(R.id.iv_camera_blog);
		ivCamera.setOnClickListener(this);

		ivBack.setOnClickListener(this);
		mListView.setOnItemClickListener(this);
	}

	class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return datas.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return datas.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@SuppressLint("NewApi")
		@Override
		public View getView(int arg0, View convertView, ViewGroup arg2) {
			// TODO Auto-generated method stub
			ViewHolder holder = null;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = View.inflate(getApplicationContext(), R.layout.my_blog_item, null);
				holder.date = (TextView) convertView.findViewById(R.id.tv_my_blog_date);
				holder.imgId = (SimpleDraweeView) convertView.findViewById(R.id.iv_my_blog_img);
				holder.content = (TextView) convertView.findViewById(R.id.tv_my_blog_content);
				holder.imageCount = (TextView) convertView.findViewById(R.id.tv_my_blog_image_count);
				convertView.setTag(holder);
			}else{
				holder = (ViewHolder) convertView.getTag();
			}

			
			// 日期只能获取到日期的数量，不能用datas.size
			/*
			 * for (int i = 0; i < dates.size(); i++) { String mDate =
			 * datas.get(i).mDate.get(i).substring(0, 6); date.setText(mDate); }
			 */
			if (datas.get(arg0).mImgId.equals("")) {
				holder.imgId.setVisibility(View.GONE);
			}
			if ((arg0 > 0) && (datas.get(arg0).mDate.equals(datas.get(arg0 - 1).mDate))) {
				holder.date.setVisibility(View.INVISIBLE);
			}
			holder.date.setText(datas.get(arg0).mDate.substring(0, 6));

			// imgId.setBackground(BitmapUtils.convertBitmap2Drawable(BitmapUtils.stringtoBitmap(datas
			// .get(arg0).mImgId)));
			// imgId.setImageUrl(datas.get(arg0).mImgId, imageLoader);
			holder.imgId.setImageURI(Uri.parse(datas.get(arg0).mImgId));
			holder.content.setText(Html.fromHtml(DropHtml.drop(datas.get(arg0).mContent.toString())));

			if (!datas.get(arg0).mCount.equals("0")) {
				holder.imageCount.setText(datas.get(arg0).mCount + "张");
			}

			return convertView;
		}

		public class ViewHolder {
			TextView date;
			SimpleDraweeView imgId;
			TextView content;
			TextView imageCount;
		}
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.iv_my_blog:
			finish();
			break;
		case R.id.iv_camera_blog:
			Intent intent = new Intent(MyBlog.this, Show.class);
			startActivity(intent);
			break;
		default:
			break;
		}
	}

	private void getJsonText() {
		final Handler handler = new Handler() {
			private String image;
			private String count = "";
			private String spaceId;

			public void handleMessage(Message msg) {
				String result = msg.obj.toString();
				Gson gson = new Gson();
				MySpaceJsonBean json = gson.fromJson(result, MySpaceJsonBean.class);
				tvSpaceName.setText(json.space_user_name);
				// ivSpaceImg.setImageUrl(json.space_user_portrait,
				// imageLoader);
				ivSpaceImg.setImageURI(Uri.parse(json.space_user_portrait));

				ArrayList<SpaceData> data = json.data;
				for (int i = 0; i < data.size(); i++) {
					String spaceDate = data.get(i).space_date; // 日期
					ArrayList<SpaceItemData> spaceItemData = data.get(i).space_data;
					for (int j = 0; j < spaceItemData.size(); j++) {
						String note = spaceItemData.get(j).note;
						count = spaceItemData.get(j).image_count;
						spaceId = spaceItemData.get(j).id;
						// itemDatas.add(note);
						ArrayList<SpaceImage> dataImage = spaceItemData.get(j).data_image;
						if (dataImage.size() != 0) {
							image = dataImage.get(0).image;
						} else {
							image = "";
						}

						datas.add(new MyBlogBean(image, note, count, spaceId, spaceDate));
					}
				}

				adapter = new MyAdapter();
				mListView.setAdapter(adapter);

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // uid
				params.add(new BasicNameValuePair("page", "1"));
				params.add(new BasicNameValuePair("page_count", "5"));
				String result = Tools.sendGet(Constant.mySpace, params);
				Message message = new Message();
				message.what = 7;
				message.obj = result;
				handler.sendMessage(message);
			};
		}.start();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		if (arg2 != 1) {

			Intent intent = new Intent(MyBlog.this, ClassBlogDetail.class);
			intent.putExtra("spaceId", datas.get(arg2 - 2).id);
			startActivity(intent);
		}
	}

	/*
	 * 没有刷新的原因是没有请求网络,保存的listview没有刷新 (non-Javadoc)
	 * 
	 * @see android.app.Activity#onResume()
	 */
	/*
	 * @Override protected void onResume() { // TODO Auto-generated method stub
	 * super.onResume(); getJsonText(); adapter.notifyDataSetChanged(); }
	 */
	/*
	 * @Override protected void onPause() { // TODO Auto-generated method stub
	 * super.onPause(); //getJsonText(); adapter.notifyDataSetChanged(); }
	 */
	/*
	 * protected void onPause() { // TODO Auto-generated method stub
	 * super.onResume(); datas.clear(); getJsonText(); }
	 */

	/*
	 * @Override protected void onPause() { // TODO Auto-generated method stub
	 * super.onPause(); finish(); }
	 */
}
