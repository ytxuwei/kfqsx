package com.example.notice;

public class Notices {
	public String name;
	public int id;
	public String time;
	public Notices(String name, String time,int id) {
		super();
		this.name = name;
		this.time = time;
		this.id = id;
	}
	public Notices(String name,int id){
		this.name = name;
		this.id = id;
	}
	@Override
	public String toString() {
		return name+"id="+id;
	}
	
	
}
