package com.example.space;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.example.global.Constant;
import com.example.jhf.R;
import com.example.view.CircleImageView;
import com.imooc.baseadapter.utils.BitmapUtils;
import com.imooc.baseadapter.utils.Tools;

public class TeacherDetail extends Activity implements OnClickListener{
	private ImageView ivBack;
	private TextView tvPhone;
	private CircleImageView img;
	private TextView tvName;
	private TextView tvJob;
	private TextView tvCourse;
	private TextView tvStyle;
	private TextView tvSelf;
	private RequestQueue mQueue;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.teacher_detail);
		
		mQueue = Volley.newRequestQueue(TeacherDetail.this);
		ivBack = (ImageView) findViewById(R.id.iv_back);
		tvPhone = (TextView) findViewById(R.id.tv_teacher_phone);
		ivBack.setOnClickListener(this);
		tvPhone.setOnClickListener(this);
		
		img = (CircleImageView) findViewById(R.id.civ_teacher_detail_img);
		tvName = (TextView) findViewById(R.id.tv_teacher_detail_name);
		tvJob = (TextView) findViewById(R.id.tv_teacher_detail_job);
		tvCourse = (TextView) findViewById(R.id.tv_teacher_detail_course);
		tvStyle = (TextView) findViewById(R.id.tv_teacher_style);
		tvSelf = (TextView) findViewById(R.id.tv_teacher_self_style);
		
		getJsonText();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.iv_back:
			finish();
			break;
		case R.id.tv_teacher_phone:
			String mobile = tvPhone.getText().toString();
			Intent intent = new Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+mobile));
			startActivity(intent);
		default:
			break;
		}
	}
	
	private void getJsonText() {
		final Handler handler = new Handler() {

			public void handleMessage(Message msg) {
				String result = msg.obj.toString();
				try {
					JSONObject jsonObject = new JSONObject(result);
					JSONObject jsonObject2 = jsonObject.getJSONObject("data");
						
						String selfEvaluation = jsonObject2.getString("self_evaluation");
						String course = jsonObject2.getString("course");
						String job = jsonObject2.getString("job");
						String parentImg = jsonObject2.getString("portrait");
						String teachterName = jsonObject2.getString("teacher_name");
						String parentPhone = jsonObject2.getString("teacher_phone");
						String teachingStyle = jsonObject2.getString("teaching_style");
						
						//img.setImageBitmap(parentImg);
						ImageRequest imageRequest = new ImageRequest(parentImg, new Listener<Bitmap>() {

							@Override
							public void onResponse(Bitmap arg0) {
								// TODO Auto-generated method stub
								img.setImageBitmap(arg0);
							}
						}, 0, 0, Config.RGB_565, new ErrorListener() {

							@Override
							public void onErrorResponse(VolleyError arg0) {
								// TODO Auto-generated method stub
								
							}
						});
						mQueue.add(imageRequest);
						tvPhone.setText(parentPhone+"");
						tvName.setText(teachterName);
						tvJob.setText(job);
						tvCourse.setText(course);
						tvStyle.setText(teachingStyle);
						tvSelf.setText(selfEvaluation);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); //uid getIntent().getIntExtra("classId", 0)+""
				params.add(new BasicNameValuePair("teacher_id", getIntent().getIntExtra("teacherId", 0)+""));	//这里获取class_id有问题
				String result = Tools.sendGet(Constant.teacherMsg, params);
				Message message = new Message();
				message.what = 7;
				message.obj = result;
				handler.sendMessage(message);
			};
		}.start();
	}
}
