package com.example.open;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.global.Constant;
import com.example.jhf.MyContentDetailActivity;
import com.example.jhf.R;
import com.example.notice.Notices;
import com.example.view.ListViewIndicatorUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.imooc.baseadapter.utils.Tools;

/**
 * 创建日期: 2015/09/12 创建者： 刘永华 功能说明：作业公告 修改履历： VER 修改日 修改者 修改内容／理由
 * ──────────────────────────────────────────────────────────────
 */
public class OpenzygkActivity extends Activity implements OnItemClickListener {
	// private ListView lvshool;
	private OpenzygkAdapter shoolDAdater;
	private TextView tvTitle;
	private ImageView shooll_GK;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private String contentName;// 每条item的标题
	private ObjectMapper om = new ObjectMapper();
	private String code;
	private int contentId;
	private ArrayList<Notices> noticeList; // 作业公告列表

	// private RefreshLayout swipeLayout;
	private PullToRefreshListView mPullRefreshListView;
	private int i = 2; // 加载更多第几页

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.openzygk);
		SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
		uid = sp.getString("uid", "0");
		noticeList = new ArrayList<Notices>();
		mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);
		mPullRefreshListView.setMode(Mode.BOTH);
		ListViewIndicatorUtil.initIndicator(mPullRefreshListView);
		getJsonText(1);

		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		tvTitle = (TextView) findViewById(R.id.tv_open_title);
		shooll_GK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				OpenzygkActivity.this.finish();
			}
		});

		shoolDAdater = new OpenzygkAdapter(OpenzygkActivity.this, noticeList);
		// 将添加的每条item放到Lstview中
		mPullRefreshListView.setAdapter(shoolDAdater);
		mPullRefreshListView.setOnItemClickListener(this);
		mPullRefreshListView.setOnRefreshListener(new com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				// 这里写下拉刷新的任务
				// new GetDataTask().execute();
				
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						noticeList.clear();
						getJsonText(1);
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullUpToRefresh");
				// 这里写上拉加载更多的任务
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						getJsonText(i);
						i++;
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);
				
			}

		});
	}

	private int pageCount; // 总页数

	private void getJsonText(final int i) {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;
			private String jsonReslut;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					jsonReslut = msg.obj.toString();
					try {
						jsonObject = new JSONObject(jsonReslut);
						pageCount = jsonObject.getInt("page_count");
						jsonArray = jsonObject.getJSONArray("data");
						for (int i = 0; i < jsonArray.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							contentName = (String) jsonObject2.get("name");
							contentId = (Integer) jsonObject2.get("id");
							noticeList.add(new Notices(contentName, contentId));
						}
						shoolDAdater.notifyDataSetChanged();

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else if (msg.what == 6) {
					Toast.makeText(OpenzygkActivity.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(OpenzygkActivity.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("page", i + ""));
				params.add(new BasicNameValuePair("page_count", pageCount + ""));
				String result = Tools.sendGet(Constant.homeworkofFiveOpen, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}

			};
		}.start();
	}

	private String note;
	private String uid;

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
		// TODO Auto-generated method stub
		new Thread() {
			private String homeworkDetail;
			private String mDate;

			public void run() {
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid));
				params.add(new BasicNameValuePair("home_id", contentId + ""));

				homeworkDetail = Tools.sendGet(Constant.detailofHomework, params);
				try {
					JSONObject jsonObj = new JSONObject(homeworkDetail);
					int cccode = jsonObj.getInt("code");
					JSONObject array = jsonObj.getJSONObject("data");
					note = (String) array.get("note");
					mDate = array.getString("date");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Intent intent = new Intent(OpenzygkActivity.this, MyContentDetailActivity.class);
				intent.putExtra("title", noticeList.get(arg2-1).name);
				intent.putExtra("note", note);
				intent.putExtra("date", mDate);
				startActivity(intent);

			}
		}.start();
	}

}
