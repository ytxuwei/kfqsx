package com.example.space;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EncodingUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ext.JodaDeserializers;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.global.Constant;
import com.example.index.ShoolLITools;
import com.example.jhf.ContentDetailActivity;
import com.example.jhf.R;
import com.example.studentwork.StudentWork;
import com.example.studentwork.StudentWorkadapter;
import com.google.gson.JsonArray;
import com.imooc.baseadapter.utils.Tools;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

/**
 * 创建日期: 2015/10/26 创建者： liu 功能说明：学生作业最后一页，具体页，用于每个班级的作业 修改履历： VER 修改日 修改者liu
 * 修改内容／理由 ──────────────────────────────────────────────────────────────
 */
public class StudentWorkParticular extends Activity {
	private WebView tView;
	private ObjectMapper om = new ObjectMapper();
	private String textStr;// 每条item的标题
	private String code, stile;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private ArrayList<ShoolLITools> data;
	private StudentWorkadapter shoolDAdater;
	private TextView title;
	private TextView contenttitle;
	private ImageView ivBack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.studentworkparticular);
		init();
		getStudentJSON();
	}

	private void init() {
		tView = (WebView) findViewById(R.id.particual_id);
		title = (TextView) findViewById(R.id.title_id);
		contenttitle = (TextView) findViewById(R.id.title_idtletle);
		ivBack = (ImageView) findViewById(R.id.shooll_GK);
		ivBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}

	/**
	 * 获得来自学生作品的数据
	 */
	public void getStudentJSON() {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						JSONObject jsonObject = new JSONObject(result);
						String str = jsonObject.get("data").toString();
						JSONObject jsonObject2 = new JSONObject(str);
						String noteStr = jsonObject2.get("note").toString();

						int a = 14; // 字体大小
						String strUrl = "<html> \n"
								+ "<head> \n"
								+ "<style type=\"text/css\"> \n"
								+ "body {text-align:justify; font-size: "
								+ a
								+ "px; line-height: "
								+ (a + 6)
								+ "px}\n"
								+ "</style> \n"
								+ "</head> \n"
								+ "<body>"
								+ EncodingUtils.getString(noteStr.getBytes(),
										"UTF-8") + "</body> \n </html>";
						tView.loadData(strUrl, "text/html; charset=UTF-8", null);
					
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (msg.what == 6) {
					Toast.makeText(StudentWorkParticular.this, "帐号或密码错误", 0)
							.show();
				} else if (msg.what == 10) {
					Toast.makeText(StudentWorkParticular.this, "忘了联网了吧", 0)
							.show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				Intent intent = getIntent();
				String strtitle = intent.getStringExtra("title_HomeWork");
				title.setText(strtitle);
				contenttitle.setText(strtitle);
				String work_id = intent.getStringExtra("id_studentwork");
				params.add(new BasicNameValuePair("homework_id", work_id));
				params.add(new BasicNameValuePair("db", Constant.db));
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				String result = Tools.sendGet(Constant.homeworkMsg, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}
}
