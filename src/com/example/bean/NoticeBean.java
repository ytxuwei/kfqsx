package com.example.bean; 

public class NoticeBean {
	public int code;
	public String title;
	public String state;
	public String date;
	public NoticeBean(int code, String title, String state,String date) {
		super();
		this.code = code;
		this.title = title;
		this.state = state;
		this.date = date;
	}
	
	
}
