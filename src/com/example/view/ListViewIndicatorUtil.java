package com.example.view;

import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class ListViewIndicatorUtil {
	public static void initIndicator(PullToRefreshListView mPullRefreshListView) {
		// TODO Auto-generated method stub
		ILoadingLayout startLabels = mPullRefreshListView    
                .getLoadingLayoutProxy(true, false);    
        startLabels.setPullLabel("下拉刷新...");// 刚下拉时，显示的提示    
        startLabels.setRefreshingLabel("正在刷新...");// 刷新时    
        startLabels.setReleaseLabel("松手以刷新...");// 下来达到一定距离时，显示的提示    
    
        ILoadingLayout endLabels = mPullRefreshListView.getLoadingLayoutProxy(    
                false, true);    
        endLabels.setPullLabel("上拉加载...");// 刚下拉时，显示的提示    
        endLabels.setRefreshingLabel("正在加载...");// 刷新时    
        endLabels.setReleaseLabel("松手以加载...");// 下来达到一定距离时，显示的提示  
	}
}
