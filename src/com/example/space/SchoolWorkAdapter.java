package com.example.space;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jhf.R;
import com.example.monthstar.OneDatyPerson;
import com.imooc.baseadapter.utils.CommonAdapter;
import com.imooc.baseadapter.utils.ViewHolderDemo;

/**
 * 创建日期: 2015/10/26 创建者： liu 功能说明：学生作业最后一页的adpter，具体页，用于每个班级的作业 修改履历： VER 修改日
 * 修改者liu 修改内容／理由 ──────────────────────────────────────────────────────────────
 */
public class SchoolWorkAdapter extends CommonAdapter<OneDatyPerson> {

	public SchoolWorkAdapter(Context context, List<OneDatyPerson> datas) {
		super(context, datas);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderDemo holder = ViewHolderDemo.get(mContext, convertView,
				parent, R.layout.shoollsitem, position);
		OneDatyPerson onedatyperson = mDatas.get(position);
		// 方式一
		TextView tv = holder.getView(R.id.TVshoolli_shoolsitem);
		tv.setText(onedatyperson.getStrriqi());
		// 方式一
		TextView tvTiem = holder.getView(R.id.TVshoolli_texw);
		tvTiem.setText(onedatyperson.getStudentytiem());
		return holder.getConvertView();
	}

}
