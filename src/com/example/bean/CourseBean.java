package com.example.bean;

public class CourseBean {
	public int id;
	public String name;
	public CourseBean(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	@Override
	public String toString() {
		return "CourseBean [id=" + id + ", name=" + name + "]";
	}
	public CourseBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
