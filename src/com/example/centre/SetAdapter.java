package com.example.centre;

import java.util.List;

//import com.example.day.OneDayAdper.ViewHolder;
import com.example.jhf.R;
import com.example.monthstar.OneDatyPerson;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SetAdapter extends BaseAdapter {
	private Context context;
	private List<OneDatyPerson> data;
	private OneDatyPerson schoolLiTools =new OneDatyPerson();
	public SetAdapter(Context context, List<OneDatyPerson> data
			) {
		this.context = context;
		this.data=data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		final ViewHolder viewHolder;
		if (convertView == null) {
			 convertView = LayoutInflater.from(context).inflate(  
	                    R.layout.settem, null);
			viewHolder = new ViewHolder();
		viewHolder.TVOneDay=(TextView) convertView.findViewById(R.id.TV_oneday_id);
		viewHolder.IV_oneday_id=(ImageView) convertView.findViewById(R.id.IV_oneday_id);
		convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		if(data.get(position).getStrriqi().equals("�˳�")){
			viewHolder.TVOneDay.setTextColor(android.graphics.Color.RED);
		}else{
			viewHolder.TVOneDay.setTextColor(android.graphics.Color.BLACK);
		}
		
   viewHolder.TVOneDay.setText(data.get(position).getStrriqi().toString());
   viewHolder.IV_oneday_id.setImageResource(R.drawable.arrow_right);
		return convertView;
	}

	public final class ViewHolder {
		public  TextView TVOneDay;
		public  ImageView IV_oneday_id;
	}
	//��������
	public void addItem_OneDatyPerson(String value){
//		schoolLiTools.setStrriqi(value);
		OneDatyPerson one = new OneDatyPerson();
		one.setStrriqi(value);
		data.add(one);
	}
}
