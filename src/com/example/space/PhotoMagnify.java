package com.example.space;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.global.Constant;
import com.example.jhf.R;
import com.example.monthstar.OneDatyPerson;
import com.imooc.baseadapter.utils.Tools;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * 相册细明最后一页
 * 
 * @author Administrator
 * 
 */
public class PhotoMagnify extends Activity {
	private ImageView imageView;
	private ObjectMapper om = new ObjectMapper();
	private String onedayconten;// 每条item的标题
	private String code;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private JSONObject jsonObject2;

	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.photomagnify);
		itin();
		getJsonText();
	}

	private void itin() {
		imageView = (ImageView) findViewById(R.id.poh_id);
		
	}

	/**
	 * 获取相册细明的JSON数据
	 */
	@SuppressLint("HandlerLeak")
	private void getJsonText() {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			@SuppressLint("HandlerLeak")
			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						jsonObject = new JSONObject(result);
						JSONObject object =  (JSONObject) jsonObject.get("data");
						onedayconten = object.getString("image");
						imageView.setImageBitmap(stringtoBitmap(onedayconten));
						//JSONArray jsonArray2 = jsonObject.getJSONArray("data");
						
						// onedayconten= jsonArray2.getJSONObject("image_id");
						// jsonArray = (JSONArray) jsonObject.get("data");
						// jsonObject2=new JSONObject(object);
						//
						// onedayconten = (String) jsonObject2.get("image");
//						jsonArray = jsonObject.getJSONArray("data");
//						String data = jsonArray.getJSONObject("image");
						// for (int i = 0; i < jsonArray.length(); i++) {
						// jsonObject2 = (JSONObject) jsonArray.opt(i);
						// onedayconten = (String) jsonObject2.get("image");
						// }

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// Intent intent = new Intent();
					// intent.setClass(ShoolJS.this, IndexActivity.class);
					// startActivity(intent);
				} else if (msg.what == 6) {
					Toast.makeText(PhotoMagnify.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(PhotoMagnify.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				Intent intent = getIntent();
				String Image_id_intent = intent
						.getStringExtra("class_albume_id");

				params.add(new BasicNameValuePair("image_id", Image_id_intent));
				params.add(new BasicNameValuePair("db", Constant.db));
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				String result = Tools.sendGet(Constant.imageMsg, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

	private Bitmap stringtoBitmap(String string) {
		// 将字符串转换成Bitmap类型
		Bitmap bitmap = null;
		try {
			byte[] bitmapArray;
			bitmapArray = Base64.decode(string, Base64.DEFAULT);
			bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0,
					bitmapArray.length);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return bitmap;
	}
}
