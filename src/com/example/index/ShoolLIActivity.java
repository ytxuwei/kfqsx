package com.example.index;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.global.Constant;
import com.example.jhf.R;
import com.imooc.baseadapter.utils.Tools;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

/**
 * 创建日期: 2015/10/21 创建者： 刘永华 功能说明：用于介绍学校历史具体介绍 修改履历： VER 修改日 修改者 修改内容／理由
 * ──────────────────────────────────────────────────────────────
 */
public class ShoolLIActivity extends Activity {
	private ImageView shooll_GK;
	private WebView SAtext;
	private ObjectMapper om = new ObjectMapper();
	private String code;
	private JSONArray jsonArray;
	private String history;
	private String hstr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.shoolliactivity);
		init();
		back();
		getShoolLI();
	}

	/**
	 * 实例化
	 */
	private void init() {
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		SAtext = (WebView) findViewById(R.id.shoolLI_id);
	}

	/**
	 * 返回方法
	 */
	private void back() {
		shooll_GK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ShoolLIActivity.this.finish();
			}
		});
	}

	/**
	 * 获取来自学校历史的JSON数据
	 */
	private void getShoolLI() {
		final Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						JSONObject jsonObject = new JSONObject(result);
						String STR = jsonObject.get("data").toString();
						JSONObject jsonObject2 = new JSONObject(STR);
						String noString = jsonObject2.get("note").toString();
						//SAtext.setText(noString);
						SAtext.loadData(noString, "text/html; charset=UTF-8", null);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}


					// Intent intent = new Intent();
					// intent.setClass(ShoolJS.this, IndexActivity.class);
					// startActivity(intent);
				} else if (msg.what == 6) {
					Toast.makeText(ShoolLIActivity.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(ShoolLIActivity.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				Intent intent = getIntent();
				String id = intent.getStringExtra("id");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("uid",uid)); // 密码
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("history_id", id));
				String result = Tools.sendGet(Constant.historyMsg, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						String code = jsonNode.get("code").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

}
