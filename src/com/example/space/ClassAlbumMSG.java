package com.example.space;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.bean.BlogContentImageBean;
import com.example.global.Constant;
import com.example.jhf.R;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.CommonAdapter;
import com.imooc.baseadapter.utils.SingleRequestQueue;
import com.imooc.baseadapter.utils.Tools;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * 创建日期: 2015/10/28 创建者：刘永华 功能说明：班级相册细明，每个相册集的相片展示 修改履历： VER 修改日 修改者 修改内容／理由
 * ──────────────────────────────────────────────────────────────
 */
public class ClassAlbumMSG extends Activity implements OnRefreshListener2<GridView>{
	private ImageView ivBack, cr_id;
	private TextView tvtotle;
	private ObjectMapper om = new ObjectMapper();
	private String onedayconten;// 每条item的标题
	private String thumbnail;// 缩略图
	private int image_id;// id
	private String code;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	//private ArrayList<OneDatyPerson> data;
	private PullToRefreshGridView gridView;
	private String Image_id_intent;// 图片id
	private String titleName_idID;// 相册id
	private static final String FILE_NAME = "USER_NOTE";
	private ArrayList<String> images; // 大图
	private ArrayList<BlogContentImageBean> imageBean;// 存储
	private RequestQueue mQueue;
	private ImageLoader imageLoader;
	private CopyOfClassAblumMSGAdaptertwo_ClassAlbum cAdapter;
	private int pageCount = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.classalbummsg);
	//	data = new ArrayList<OneDatyPerson>();
		imageBean = new ArrayList<BlogContentImageBean>();
		mQueue = SingleRequestQueue.getRequestQueue(getApplicationContext());
		imageLoader = new ImageLoader(mQueue, new BitmapCache());
		IntentFilter intentFilter = new IntentFilter();  
	      intentFilter.addAction("action.refreshImage");  
	      registerReceiver(mRefreshBroadcastReceiver, intentFilter);  
		inint();
		back();
		getJsonText(1);
		String strTitle = getIntent().getStringExtra("titleName_id");
		tvtotle.setText(strTitle);
		images = new ArrayList<String>();
		cAdapter = new CopyOfClassAblumMSGAdaptertwo_ClassAlbum(
				ClassAlbumMSG.this, imageBean);
		gridView.setAdapter(cAdapter);
		gridView.setOnRefreshListener(this);
	}

	/**
	 * 实例化
	 */
	private void inint() {
		ivBack = (ImageView) findViewById(R.id.shooll_GK);
		gridView = (PullToRefreshGridView) findViewById(R.id.classgridview_id);
		cr_id = (ImageView) findViewById(R.id.cr_id);
		cr_id.setOnClickListener(new OnClickListener() {// 上传照片片

			@Override
			public void onClick(View v) {
				// startActivity(new Intent(getApplicationContext(),
				// ImageMainActivitypoth.class));
				Intent intent = new Intent(ClassAlbumMSG.this,
						AlbumMsgSendToNet.class);
				// intent.putExtra("Image_id_intentID_id", titleName_idID);
				SharedPreferences sharedPreferences = getSharedPreferences(
						FILE_NAME, Activity.MODE_PRIVATE);
				Editor edit = sharedPreferences.edit();
				edit.putString("image", titleName_idID);
				edit.commit();
				startActivity(intent);
			}
		});
		tvtotle = (TextView) findViewById(R.id.title_MSG_id);
	}

	private void back() {
		ivBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				ClassAlbumMSG.this.finish();
			}
		});

	}

	/**
	 * 
	 */
	private void getJsonText(final int i) {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			@SuppressLint("HandlerLeak")
			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						jsonObject = new JSONObject(result);
						jsonArray = jsonObject.getJSONArray("data");
						for (int i = 0; i < jsonArray.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							onedayconten = (String) jsonObject2.get("image");
							thumbnail = jsonObject2.getString("thumbnail");
							image_id = jsonObject2.getInt("id");
							// OneDatyPerson shoolLITools = new OneDatyPerson();
							// shoolLITools.setImage_View(BitmapUtils
							// .stringtoBitmap(onedayconten));
							// shoolLITools.setStudentytiem(onedayconten);
							// data.add(shoolLITools);
							imageBean.add(new BlogContentImageBean(image_id,
									onedayconten, thumbnail));
						}
						cAdapter.notifyDataSetChanged();

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// Intent intent = new Intent();
					// intent.setClass(ShoolJS.this, IndexActivity.class);
					// startActivity(intent);
				} else if (msg.what == 6) {
					Toast.makeText(ClassAlbumMSG.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(ClassAlbumMSG.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				Intent intent = getIntent();
				// Image_id_intent = intent.getStringExtra("class_albume_id");
				titleName_idID = getIntent().getStringExtra(
						"class_albume_id_UP");

				params.add(new BasicNameValuePair("photo_id", titleName_idID));
				params.add(new BasicNameValuePair("db", Constant.db));
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("page", i+"")); // 密码
				params.add(new BasicNameValuePair("page_count", pageCount+"")); // 密码
				String result = Tools.sendGet(Constant.line, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

	/**
	 * grewview图片点击放大和图片轮训
	 */
	public void setViewPagerAndZoom(View v, int position) {
		// 得到要放大展示的视图界面
		ViewPager expandedView = (ViewPager) (findViewById(R.id.detail_view_classalbum));
		// 最外层的容器，用来计算
		View containerView = (FrameLayout) (findViewById(R.id.container_classalbum));
		// 实现放大缩小类，传入当前的容器和要放大展示的对象
		// ZoomTutorial mZoomTutorial = new ZoomTutorial(containerView,
		// expandedView);
		ClassAlbumQueZoomTutorial classAlbumQueZoomTutorial = new ClassAlbumQueZoomTutorial(
				containerView, expandedView);
		ClassAlbumQuViewPagerAdapter classAlbumQuViewPagerAdapter = new ClassAlbumQuViewPagerAdapter(
				ClassAlbumMSG.this, imageBean, classAlbumQueZoomTutorial);
		// ViewPagerAdapter adapter = new ViewPagerAdapter(
		// getApplicationContext(), images, mZoomTutorial);
		expandedView.setAdapter(classAlbumQuViewPagerAdapter);
		expandedView.setCurrentItem(position);
		// 通过传入Id来从小图片扩展到大图，开始执行动画
		classAlbumQueZoomTutorial.zoomImageFromThumb(v);

	}

	public class CopyOfClassAblumMSGAdaptertwo_ClassAlbum extends
			CommonAdapter<BlogContentImageBean> {
		private Context context;
		private NetworkImageView iView;

		public CopyOfClassAblumMSGAdaptertwo_ClassAlbum(Context context,
				List<BlogContentImageBean> datas) {
			super(context, datas);
			this.context = context;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			
			String image = mDatas.get(position).image;
			if (convertView == null) {
				iView = new NetworkImageView(ClassAlbumMSG.this);
				WindowManager wm = (WindowManager)ClassAlbumMSG.this.getSystemService(Context.WINDOW_SERVICE);
				int width = wm.getDefaultDisplay().getWidth();
				int size = width/3;
				iView.setLayoutParams(new GridView.LayoutParams(size,size));// 设置ImageView对象布局
				iView.setAdjustViewBounds(false);// 设置边界对齐
				iView.setScaleType(ImageView.ScaleType.FIT_XY);// 设置刻度的类型
				iView.setPadding(4, 4, 4, 4);// 设置间距
			} else {
				iView = (NetworkImageView) convertView;
			}
			iView.setImageUrl(image, imageLoader);
			// 可放大缩小添加视图
			iView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					setViewPagerAndZoom(v, position);
				}
			});
			return iView;
		}
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(mRefreshBroadcastReceiver);  
		//mQueue.cancelAll(this);
	}

	int i=2;
	@Override
	public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				imageBean.clear();
				getJsonText(1);
				i=2;
				gridView.onRefreshComplete();
			}
		}, 3000);
	}

	@Override
	public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				getJsonText(i);
				i++;
				gridView.onRefreshComplete();
			}
		}, 3000);
	}
	
	
	private BroadcastReceiver mRefreshBroadcastReceiver = new BroadcastReceiver() {  
		  
	      @Override  
	      public void onReceive(Context context, Intent intent) {  
	          String action = intent.getAction();  
	          if (action.equals("action.refreshImage"))  
	          {  
	              //reFreshFrinedList();  
	        	  //getJsonText(1);
	        	  onPullDownToRefresh(gridView);
	          }  
	      }  
	  };
	  
}
