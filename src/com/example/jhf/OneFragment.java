package com.example.jhf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import krelve.view.Kanner;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.global.Constant;
import com.example.index.ShoolBroad;
import com.example.monthstar.OneDay;
import com.example.notice.Notice;
import com.example.notice.Notices;
import com.example.open.OpenActivtiv;
import com.example.studentwork.StudentWorksActivity;
import com.imooc.baseadapter.utils.Tools;
import com.yuan.cfycomment.Leave_word;

/*
 * 创建日期：2015/9/9
 * 创建者：刘永华
 * 功能说明：首页界面
 */
public class OneFragment extends Fragment implements OnItemClickListener {
	private int[] inc = { R.drawable.home_page1, R.drawable.home_page2, R.drawable.home_page3, R.drawable.home_page4, R.drawable.home_page5, R.drawable.home_page6 };
	private String[] str = { "学校概况", "每月百星", "通知公告", "五公开专栏", "学生作品", "家长留言" };
	private GridView mgGridView;
	private ArrayList<Map<String, Object>> data;
	private SimpleAdapter adapter;
	private Context context;
	private ImageView shoolbroad_id;
	private ListView mListView;
	private ArrayList<String> imgList;
	private ArrayList<ImageView> portImg;
	private ArrayList<Map<String, Object>> Str_data;
	private ArrayList<Notices> datas;
	private ObjectMapper om = new ObjectMapper();
	/**
	 * 存储上一个选择项的Index
	 */
	private ListViewAdapter adapter2;
	private String uid;
//	private FlashView flashView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		imgList = new ArrayList<String>();
		SharedPreferences sp = getActivity().getSharedPreferences("UserId", 0);
		uid = sp.getString("uid", "0");
		getFigureFromNet();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		context = getActivity();

		View view = inflater.inflate(R.layout.homepage_fragment, container, false);
		View view2 = inflater.inflate(R.layout.homepage_headview, null);
		mListView = (ListView) view.findViewById(R.id.LV_home);
		// lv_titem.setDividerHeight(1);
		// lv_titem.setDivider(getResources().getDrawable(R.drawable.xuxian));
		mgGridView = (GridView) view2.findViewById(R.id.homepagegridviewid);
		shoolbroad_id = (ImageView) view2.findViewById(R.drawable.home_page1);
		// ll_focus_indicator_container = (LinearLayout)
		// view2.findViewById(R.id.ll_focus_indicator_container);

		kanner = (Kanner) view2.findViewById(R.id.kanner);
		mListView.addHeaderView(view2);

		mgGridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
		mgGridView.setVerticalSpacing(15);

		name();
		initData();
		return view;
	}

	public void name() {
		adapter = new SimpleAdapter(context, getData(), R.layout.home_pageitem, new String[] { "icn", "icnName" }, new int[] { R.id.home_id, R.id.datuName_id });
		mgGridView.setAdapter(adapter);
		mgGridView.setOnItemClickListener(new ItemClickListener());
	}

	/**
	 * 加载数据
	 * 
	 * @return
	 */
	private List<Map<String, Object>> getData() {
		data = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < inc.length; i++) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("icn", inc[i]);
			map.put("icnName", str[i]);
			data.add(map);
		}
		return data;

	}

	class ItemClickListener implements OnItemClickListener {
		/**
		 * 点击项时触发事件
		 * 
		 * @param parent
		 *            发生点击动作的AdapterView
		 * @param view
		 *            在AdapterView中被点击的视图(它是由adapter提供的一个视图)。
		 * @param position
		 *            视图在adapter中的位置。
		 * @param rowid
		 *            被点击元素的行id。
		 */
		public void onItemClick(AdapterView<?> parent, View view, int position, long rowid) {
			HashMap<String, Object> item = (HashMap<String, Object>) parent.getItemAtPosition(position);
			// 根据图片进行相应的跳转
			switch (inc[position]) {
			case R.drawable.home_page1:
				startActivity(new Intent(context, ShoolBroad.class));// 启动另一个Activity
				// finish();//结束此Activity，可回收
				break;
			case R.drawable.home_page2:
				startActivity(new Intent(context, OneDay.class));// 启动另一个Activity
				// finish();//结束此Activity，可回收
				break;
			case R.drawable.home_page3:
				startActivity(new Intent(context, Notice.class));// 启动另一个Activity
				// finish();//结束此Activity，可回收
				break;
			case R.drawable.home_page4:
				startActivity(new Intent(context, OpenActivtiv.class));// 启动另一个Activity
				// finish();//结束此Activity，可回收
				break;
			case R.drawable.home_page5:
				startActivity(new Intent(context, StudentWorksActivity.class));// 启动另一个Activity
				// finish();//结束此Activity，可回收
				break;
			case R.drawable.home_page6:
				startActivity(new Intent(context, Leave_word.class));// 启动另一个Activity
				// finish();//结束此Activity，可回收
				break;
			}

		}
	}

	/**
	 * 实例化数据
	 */
	private void initData() {
		// imgList = new ArrayList<String>();
		datas = new ArrayList<Notices>();
		getJsonText();
		adapter2 = new ListViewAdapter();
		mListView.setAdapter(adapter2);

		/*
		 * flashView.setOnPageClickListener(new FlashViewListener() {
		 * 
		 * @Override public void onClick(int position) { // TODO Auto-generated
		 * Intent intent = new Intent(context, FlashViewDetail.class);
		 * intent.putExtra("position", position+1); startActivity(intent); } });
		 */
		mListView.setOnItemClickListener(this);

	}

	/**
	 * 获取轮播图
	 */
	private void getFigureFromNet() {
		// TODO Auto-generated method stub
		final Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						JSONObject jsonObject = new JSONObject(result);
						JSONArray jsonArray = jsonObject.getJSONArray("data");
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject opt = (JSONObject) jsonArray.opt(i);
							String img = opt.getString("image");
							// opt.getInt("id");
							imgList.add(img);
						}
						//flashView.setImageUris(imgList);
						//flashView.setEffect(EffectConstants.DEFAULT_EFFECT);
						if(imgList.size()>0){
							kanner.setImagesUrl((String[])imgList.toArray(new String[0]));
						}else{
							int[] imgs = {R.drawable.demo_img};
							kanner.setImagesRes(imgs);
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else if (msg.what == 6) {
					Toast.makeText(context, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(context, "忘了联网了吧", 0).show();
				}

			}
		};

		new Thread() {
			public void run() {
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				String result = Tools.sendGet(Constant.figure, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						String code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

	/**
	 * 最下面的listview
	 */
	private void getJsonText() {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						JSONObject jsonObject = new JSONObject(result);
						JSONArray jsonArray = jsonObject.getJSONArray("data");
						for (int i = 0; i < 3; i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							String contentName = (String) jsonObject2.get("name");
							String contentTime = jsonObject2.getString("date");
							int contentId = (Integer) jsonObject2.get("notice_id");
							datas.add(new Notices(contentName, contentTime,contentId));
						}
						adapter2 = new ListViewAdapter();
						// 将添加的每条item放到Lstview中
						mListView.setAdapter(adapter2);

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (msg.what == 6) {
					Toast.makeText(context, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(context, "忘了联网了吧", 0).show();
				}
			}
		};
		new Thread() {
			public void run() {
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("page", 1 + "")); // 密码
				params.add(new BasicNameValuePair("page_count", 5 + "")); // 密码
				String result = Tools.sendGet(Constant.noticeList, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						String code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

	class ListViewAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return datas.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return datas.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			View view = View.inflate(getActivity(), R.layout.item_single, null);
			TextView tv = (TextView) view.findViewById(R.id.tv_item_general_title);
			TextView tvTime = (TextView) view.findViewById(R.id.tv_notice_time);
			tvTime.setText(datas.get(arg0).time);
			tv.setText(datas.get(arg0).name);
			tv.setTextSize(14);
			/*ImageView ivLine = (ImageView) view.findViewById(R.id.index_line);
			if(arg0==2){
				ivLine.setVisibility(View.INVISIBLE);
			}*/
			return view;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
		// TODO Auto-generated method stub
		new Thread() {
			public void run() {
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid));
				params.add(new BasicNameValuePair("notice_id", datas.get(position-1).id + ""));
				String resultDetail = Tools.sendGet(Constant.noticeContent, params);
				processDetailData(resultDetail);
				Intent intent = new Intent(context, MyContentDetailActivity.class);
				intent.putExtra("title", datas.get(position-1).name);
				intent.putExtra("note", note);
				intent.putExtra("date", datas.get(position-1).time);
				startActivity(intent);
			}
		}.start();
	}

	private String note;
	private Kanner kanner;

	protected void processDetailData(String json) {
		try {
			JSONObject jsonObj = new JSONObject(json);
			JSONObject array = jsonObj.getJSONObject("data");
			note = (String) array.get("note");
		} catch (JSONException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
	}

}
