package com.example.jhf;

import java.util.List;
import java.util.Map;

import android.R.integer;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * 班级空间baseAdapter
 * @author Administrator
 *
 */
public class CPAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private List<Map<String, Object>> data;
	private int layoutID;

	public CPAdapter(Context context, List<Map<String, Object>> data,
			int layoutID) {
		this.context = context;
		this.layoutInflater = LayoutInflater.from(context);
		this.layoutID = layoutID;
		this.data=data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		final ViewHolder viewHolder;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.classpaceitem, null);
			viewHolder = new ViewHolder();
			viewHolder.IVcpI = (ImageView) convertView
					.findViewById(R.id.IVcpI_id);
			viewHolder.IVcpD = (ImageView) convertView
					.findViewById(R.id.IVcpD_id);
			viewHolder.TVcp = (TextView) convertView.findViewById(R.id.TVcp_id);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		Map<String, Object> map = data.get(position);
		int img= Integer.parseInt(map.get("图标").toString()); 
		switch (img) {
		case 1:
			viewHolder.IVcpI.setImageResource(R.drawable.class_bk);
			break;
		case 2:
			viewHolder.IVcpI.setImageResource(R.drawable.class_js);
			break;
		case 3:
			viewHolder.IVcpI.setImageResource(R.drawable.class_xc);
			break;
		case 4:
			viewHolder.IVcpI.setImageResource(R.drawable.class_zy);
			break;			
		case 5:
			viewHolder.IVcpI.setImageResource(R.drawable.class_zs);
			break;
		}
		
		switch (img) {
		case 1:
			viewHolder.IVcpD.setImageResource(R.drawable.class_xyehui);
			break;
		case 2:
			viewHolder.IVcpD.setImageResource(R.drawable.class_xyehui);
			break;
		case 3:
			viewHolder.IVcpD.setImageResource(R.drawable.class_xyehui);
			break;
		case 4:
			viewHolder.IVcpD.setImageResource(R.drawable.class_xyehui);
			break;			
		case 5:
			viewHolder.IVcpD.setImageResource(R.drawable.class_xyehui);
			
			break;
		}
		switch (img) {
		case 1:
			viewHolder.TVcp.setText("班级博客");
			break;
		case 2:
			viewHolder.TVcp.setText("教师成员");
			break;
		case 3:
			viewHolder.TVcp.setText("班级相册");
			break;
		case 4:
			viewHolder.TVcp.setText("作业");
			break;			
		case 5:
			viewHolder.TVcp.setText("知识回答");
			break;
		}
		return convertView;
	}

	public static class ViewHolder {
		public static ImageView IVcpI;
		public static TextView TVcp;
		public static ImageView IVcpD;
	}
}
