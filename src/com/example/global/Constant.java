package com.example.global;


/**
 * 全局变量
 * 
 * @author Administrator
 * 
 */
public class Constant {
	//public static final String db = "school_tk";
	public static final String db = "school";
	public static final String url = "http://114.215.96.130:8089";
	//public static final String url = "http://192.168.252.69:8069";
	/**
	 * 通知公告标题列表
	 */
	public static final String noticeList = url + "/school/noticeTitel";
	// 登录
	public static final String login = url + "/mobile/login";
	// 通知公告具体信息
	public static final String noticeContent = url + "/school/noticeMsg";
	// 轮播图具体信息
	// 作业公开
	public static final String homeworkofFiveOpen = url + "/notice/homework";
	// 作业公告具体信息
	public static final String detailofHomework = url + "/notice/homeworkMsg";
	// 课时列表
	public static final String classHour = url + "/notice/classHour";
	// 课时具体信息
	public static final String classHourMsg = url + "/notice/classHourMsg";
	// 期末检测
	public static final String finalInspection = url
			+ "/notice/finalInspection";
	// 期末检测具体信息
	public static final String finalInspectionMsg = url
			+ "/notice/finalInspectionMsg";
	// 节假日列表
	public static final String holiday = url + "/notice/holiday";
	// 节假日具体信息
	public static final String holidayMsg = url + "/notice/holidayMsg";
	// 课程列表
	public static final String course = url + "/notice/course";
	// 课程公告列表
	public static final String courseLine = url + "/notice/courseLine";
	// 课程公告具体信息
	public static final String courseMsg = url + "/notice/courseMsg";
	// 轮播图具体信息
	public static final String flashViewMsg = url + "/carousel/figureMsg";
	// 轮播图
	public static final String figure = url + "/carousel/figure";
	// 教师成员班级列表
	public static final String memberClass = url + "/teacher/memberClass";
	// 教师成员列表信息
	public static final String memberLineString = url + "/teacher/memberLine";
	// 教师成员家长列表
	public static final String memberParents = url + "/teacher/memberParents";
	// 家长具体信息
	public static final String parentMsg = url + "/teacher/ParentMsg";
	// 老师具体信息
	public static final String teacherMsg = url + "/teacher/memberMsg";
	// 班级博客列表
	public static final String classBlogList = url + "/class/spaceList";
	// 班级博客内容页
	public static final String spaceMsg = url + "/class/spaceMsg";
	// 我的博客
	public static final String mySpace = url + "/class/mySpace";
	// 发表评论
	public static final String postMsg = url + "/class/spaceComment";
	// 发博客
	public static final String postBlog = url + "/class/spaceCreate";
	// 认证获取年级列表
	public static final String getGrade = url + "/school/getGrade";
	// 认证获取班级列表
	public static final String getClass = url + "/school/getClass";
	// 认证获取家长角色列表
	public static final String getRole = url + "/school/getRole";
	// 验证码
	public static final String sendCode = url + "/send/verificationCode";
	// 认证
	public static final String sendAccreditation = url
			+ "/school/confirmParent";
	// 个人资料
	public static final String getProfile = url + "/personal/getProfile";
	//图片详情
	public static final String bigImage = url+"/space/imageMsg";
	//修改个人头像
	public static final String updatePortrait = url+"/personal/updatePortrait";
	//学校风貌
	public static final String schoolStyle = url+"/school/style";
	/**
	 * 学校简介
	 */
	public static final String synopsis = url + "/school/synopsis";
	/**
	 * 学校校史
	 */
	public static final String history = url + "/school/history";
	public static final String historyMsg = url + "/school/historyMsg";
	/**
	 * 每日白星
	 */
	public static final String starMonth = url + "/school/starMonth";

	/**
	 * 每日白星数据标题列表
	 */
	public static final String starTitle = url + "/school/starTitle";
	/**
	 * 每日白星数据标题列表细明
	 */
	public static final String startMsg = url + "/school/starMsg";
	/**
	 * 学生作品分类列表
	 */
	public static final String work = url + "/student/work";
	/**
	 * 学生作品列表
	 */
	public static final String workLine = url + "/student/workLine";
	/**
	 * 学生作品信息表
	 */
	public static final String workLineMsg = url + "/student/workLineMsg";
	/**
	 * 家长留言
	 */
	public static final String leaveMessage = url + "/parents/leaveMessage";
	/**
	 * 发表家长留言
	 */
	public static final String publishComment = url + "/parents/publishComment";
	/**
	 * 学生作业
	 */
	public static final String homework = url + "/class/homework";
	/**
	 * 学生具体作业
	 */
	public static final String homeworkMsg = url + "/class/homeworkMsg";
	/**
	 * 班级相册
	 */
	public static final String photos = url + "/class/photos";
	/**
	 * 班级相册细明
	 */
	public static final String line = url + "/photos/line";
	/**
	 * 校园广播
	 */
	public static final String radio = url + "/school/radio";
	/**
	 * 班级广播
	 */
	public static final String radioClass = url + "/class/radio";
	/**
	 * 校园电视台
	 */
	public static final String station = url + "/school/station";
	/**
	 * 班级电视台
	 */
	public static final String classstation = url + "/class/station";
	/**
	 * 问题具体信息
	 */
	public static final String answerMsg = url + "/knowledge/answerMsg";
	/**
	 * 知识回答首页
	 */
	public static final String answerList = url + "/knowledge/answerList";
	/**
	 * 每月百星班级
	 */
	public static final String starClass = url + "/school/starClass";
	/**
	 * 相册具体最后一页
	 */
	public static final String imageMsg = url + "/photos/imageMsg";
	/**
	 * 发表知识评论
	 */
	public static final String answerComment = url + "/knowledge/answerComment";
	/**
	 * 知识回答点赞
	 */
	public static final String answerLike = url + "/knowledge/answerLike";
	/**
	 * 获取课表菜单
	 */
	public static final String courseList = url + "/class/courseList";
	/**
	 * 发表问题
	 */
	public static final String answerCreate = url + "/knowledge/answerCreate";
	/**
	 * 获取验证码
	 */
	public static final String verificationCode = url
			+ "/send/verificationCode";
	/**
	 * 注册
	 */
	public static final String register = url + "/mobile/register";
	/**
	 * 找回密码
	 */
	public static final String changePasspword = url
			+ "/mobile/changePasspword";
	
	/**
	 * 找回密码的时候的验证
	 */
	public static final String verificationCodechange = url
			+ "/send/verificationCode";
	
	public static final String uploadImage = url+"/photos/uploadImage";
	
	/**
	 * 新建相册
	 */
	public static String createImage = url + "/photos/createImage";

}
