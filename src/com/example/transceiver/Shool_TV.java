package com.example.transceiver;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.day.event.ItemEveryYearEvent;
import com.example.global.Constant;
import com.example.jhf.R;
import com.example.monthstar.OneDatyPerson;
import com.example.monthstar.OneDay;
import com.example.monthstar.OneDayAdper;
import com.imooc.baseadapter.utils.Tools;

/**
 * 校园电视台
 * 
 * @author Administrator
 * 
 */
public class Shool_TV extends Activity {
	private String[] title = { "五年级二班", "三年级四班", "二年级一班", "五年级一班" };
	private String[] name = { "小明和小芳今天开展演讲活动", "李红播报我的祖国", "黎明主持献爱心活动",
			"学校陈丽20周年纪念日" };
	private Shool_TVPer shoolpe;
	private ArrayList<Shool_TVPer> data;
	private ListView mListView;
	private ImageView shooll_GK;
	private ObjectMapper om = new ObjectMapper();
	private String titleStr;// 标题
	private String noteStr;// 标题
	private String IamgeStr;// 图片
	private String code;
	private JSONArray jsonArray;
	private JSONObject jsonObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.shool_tv);
		mListView = (ListView) findViewById(R.id.listView_vt);
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		shooll_GK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Shool_TV.this.finish();
			}
		});
		data = new ArrayList<Shool_TVPer>();
		getJsonText();
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@SuppressLint("ShowToast")
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				try {

					Intent intent = new Intent(Shool_TV.this,
							Shool_TV_msg.class);
					JSONObject jsonObject3 = (JSONObject) jsonArray.opt(arg2);

					String startURL = jsonObject3.get("url").toString();
					String school_noteStr = jsonObject3.get("note").toString();
					String contenttitle=jsonObject3.getString("title");
					intent.putExtra("school_title", "校园电视台");
					intent.putExtra("startURL", startURL);
					intent.putExtra("school_noteStr", school_noteStr);
					intent.putExtra("contenttitle", contenttitle);
					startActivity(intent);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

	}

	private void intn() {
		shoolpe = new Shool_TVPer();
		for (int i = 0; i < title.length; i++) {
			shoolpe.setTitleStr(title[i]);
			shoolpe.setContentStr(name[i]);
			data.add(shoolpe);
		}
		Shool_TVAdapter shool_TVAdapter = new Shool_TVAdapter(Shool_TV.this,
				data);
		mListView.setAdapter(shool_TVAdapter);
	}

	/**
	 * 获取学校简介的JSON数据
	 */
	@SuppressLint("HandlerLeak")
	private void getJsonText() {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						jsonObject = new JSONObject(result);
						jsonArray = jsonObject.getJSONArray("data");
						for (int i = 0; i < jsonArray.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							titleStr = (String) jsonObject2.get("title");
							noteStr = jsonObject2.getString("note");
							IamgeStr = jsonObject2.getString("image");

							shoolpe = new Shool_TVPer();
							shoolpe.setTitleStr(titleStr);
							shoolpe.setContentStr(noteStr);
							//shoolpe.setBitmap(stringtoBitmap(IamgeStr));
							shoolpe.setHeadStr(IamgeStr);
							data.add(shoolpe);
						}
						Shool_TVAdapter shool_TVAdapter = new Shool_TVAdapter(
								Shool_TV.this, data);
						mListView.setAdapter(shool_TVAdapter);

						// shoolDAdater = new OneDayAdper(OneDay.this, data);
						// // 将添加的每条item放到Lstview中
						// lvshool.setAdapter(shoolDAdater);
						// lvshool.setOnItemClickListener(new
						// OnItemClickListener() {
						//
						// @Override
						// public void onItemClick(AdapterView<?> arg0,
						// View arg1, int arg2, long arg3) {
						// try {
						// Intent intent = new Intent(OneDay.this,
						// ItemEveryYearEvent.class);
						// JSONObject jsonObject3 = (JSONObject) jsonArray
						// .opt(arg2 - 1);
						// String start_id = jsonObject3
						// .get("star_id").toString();
						// intent.putExtra("star_id", start_id);
						// Log.v("OneDay", "start_id" + start_id);
						// // SharedPreferences
						// // sp=getSharedPreferences("star_id",
						// // MODE_PRIVATE);
						// // Editor edit = sp.edit();
						// // edit.putString("star_id", start_id);
						// // edit.commit();
						// startActivity(intent);
						// } catch (Exception e) {
						// // TODO: handle exceptions
						// }
						//
						// }
						// });
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// Intent intent = new Intent();
					// intent.setClass(ShoolJS.this, IndexActivity.class);
					// startActivity(intent);
				} else if (msg.what == 6) {
					Toast.makeText(Shool_TV.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(Shool_TV.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				String result = Tools.sendGet(Constant.station, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

	/**
	 * 图片转换
	 * 
	 * @param string
	 * @return
	 */
	private Bitmap stringtoBitmap(String string) {
		// 将字符串转换成Bitmap类型
		Bitmap bitmap = null;
		try {
			byte[] bitmapArray;
			bitmapArray = Base64.decode(string, Base64.DEFAULT);
			bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0,
					bitmapArray.length);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return bitmap;
	}
}
