package com.example.studentwork;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;//测试

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.day.event.ItemEveryYearEvent;
import com.example.global.Constant;
import com.example.index.ShoolLIAdpter;
import com.example.index.ShoolLITools;
import com.example.jhf.ContentDetailActivity;
import com.example.jhf.R;
import com.example.monthstar.OneDatyPerson;
import com.example.monthstar.OneDayAdper;
import com.example.view.ListViewIndicatorUtil;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.imooc.baseadapter.utils.Tools;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 学生作品第二页
 * 
 * @author Administrator
 * 
 */
public class StudentWork extends Activity {
	// private ListView tView;
	private ObjectMapper om = new ObjectMapper();
	private String textStr;// 每条item的标题
	private String code, stile;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private ArrayList<ShoolLITools> datas;
	private StudentWorkadapter shoolDAdater;
	private TextView tieleTV;
	private ImageView shooll_GK;
	private PullToRefreshListView mPullRefreshListView;
	private int pageCount = 0;
	private int page = 2;
	private String mWorkId;	//从上一层传输的id

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.studentworktwo);
		datas = new ArrayList<ShoolLITools>();
		getStudentJSON(1);
		init();
		back();
	}

	private void init() {
		// tView = (ListView) findViewById(R.id.list_swk);
		Intent intent = getIntent();
		mWorkId = intent.getStringExtra("work_id");
		String textTVStr = intent.getStringExtra("TitleName");
		mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.sutdent_work_list);
		mPullRefreshListView.setMode(Mode.BOTH);
		ListViewIndicatorUtil.initIndicator(mPullRefreshListView);
		shoolDAdater = new StudentWorkadapter(StudentWork.this, datas);
		mPullRefreshListView.setAdapter(shoolDAdater);
		tieleTV = (TextView) findViewById(R.id.title_id);
		tieleTV.setText(textTVStr);
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		mPullRefreshListView.setOnRefreshListener(new com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullDownToRefresh");

				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						datas.clear();
						getStudentJSON(1);
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullUpToRefresh");
				// 这里写上拉加载更多的任务

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						getStudentJSON(page);
						page++;
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);

			}

		});
	}

	/**
	 * 返回
	 */
	public void back() {
		shooll_GK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				StudentWork.this.finish();
			}
		});
	}

	/**
	 * 获得来自学生作品的数据
	 */
	public void getStudentJSON(final int i) {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						jsonObject = new JSONObject(result);
						System.out.println("josn"+jsonObject);
						pageCount = jsonObject.getInt("page_count");
						jsonArray = jsonObject.getJSONArray("data");
						for (int i = 0; i < jsonArray.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							textStr = (String) jsonObject2.get("title");
							String mDate = jsonObject2.getString("date");
							// tView.setText("\t\t\t\t" + textStr);
							ShoolLITools shoolLITools = new ShoolLITools();
							shoolLITools.setStr_shoolli(textStr);
							shoolLITools.setmDate(mDate);
							datas.add(shoolLITools);
						}
						shoolDAdater.notifyDataSetChanged();

						mPullRefreshListView.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
								try {
									Intent intent = new Intent(StudentWork.this, ContentDetailActivity.class);
									JSONObject jsonObject3 = (JSONObject) jsonArray.opt(arg2-1);
									String id = jsonObject3.get("id").toString();
									String textStrtilte = (String) jsonObject3.get("title");
									String mDate = jsonObject3.getString("date");
									intent.putExtra("id", id);
									intent.putExtra("onedaycontenid", textStrtilte);
									intent.putExtra("date", mDate);
									startActivity(intent);
								} catch (Exception e) {
									// TODO: handle exceptions
								}

							}
						});
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (msg.what == 6) {
					Toast.makeText(StudentWork.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(StudentWork.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				// Log.v("star_id", sta_line_id);
				params.add(new BasicNameValuePair("work_id", mWorkId));
				System.out.println("work_id"+mWorkId);
				params.add(new BasicNameValuePair("db", Constant.db));
				params.add(new BasicNameValuePair("uid", uid)); 
				params.add(new BasicNameValuePair("page", i+"")); 
				params.add(new BasicNameValuePair("page_count", pageCount+"")); 
				String result = Tools.sendGet(Constant.workLine, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}
}
