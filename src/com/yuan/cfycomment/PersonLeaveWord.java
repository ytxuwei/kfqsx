package com.yuan.cfycomment;

/**
 * 创建日期: 2015/10/24 创建者： 刘永华 功能说明：家长留言工具类，用于记录每条记录的属性 修改履历： VER 修改日 修改者 修改内容／理由
 * ──────────────────────────────────────────────────────────────
 */
public class PersonLeaveWord {
	private String classNameStr;// 班级名字
	private String patriarchStr;// 学生家长
	private String TiemIssueStr;// 发布时间
	private String ProblemContentStr;// 问题名字
	private String answerStr;// 管理员回答

	public String getClassNameStr() {
		return classNameStr;
	}

	public void setClassNameStr(String classNameStr) {
		this.classNameStr = classNameStr;
	}

	public String getPatriarchStr() {
		return patriarchStr;
	}

	public void setPatriarchStr(String patriarchStr) {
		this.patriarchStr = patriarchStr;
	}

	public String getTiemIssueStr() {
		return TiemIssueStr;
	}

	public void setTiemIssueStr(String tiemIssueStr) {
		TiemIssueStr = tiemIssueStr;
	}

	public String getProblemContentStr() {
		return ProblemContentStr;
	}

	public void setProblemContentStr(String problemContentStr) {
		ProblemContentStr = problemContentStr;
	}

	public String getAnswerStr() {
		return answerStr;
	}

	public void setAnswerStr(String answerStr) {
		this.answerStr = answerStr;
	}

}
