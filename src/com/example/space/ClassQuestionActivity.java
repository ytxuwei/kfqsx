package com.example.space;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.global.Constant;
import com.example.jhf.R;
import com.example.monthstar.OneDatyPerson;
import com.example.view.DropHtml;
import com.example.view.ListViewIndicatorUtil;
import com.example.view.MyGridView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.SingleRequestQueue;
import com.imooc.baseadapter.utils.Tools;
import com.king.photo.activity.MainActivitypoth;

/**
 * 知识回答
 * 
 * @author Administrator
 * 
 */
@SuppressLint("CutPasteId")
public class ClassQuestionActivity extends Activity implements OnClickListener, OnRefreshListener2<ListView> {
	private ImageView shooll_GK;
	private TextView tvQuick;
	private int i = 2;
	private PullToRefreshListView lvAnswer;// 加载更多
	private ObjectMapper om = new ObjectMapper();// 返回信息的载体
	private String code;
	private ArrayList<AnswerPer> datata;// listview的载体
	private ArrayList<OneDatyPerson> dataPerson;// griewiew的图片，在listview中嵌套
	public static final int REQUSET = 1;
	private String stranswer_count;// 点在人生与
	private Class_questionAapter_CQ class_questionAapter;// listview的adapter
	private AnswerPer answerPer;// 回答人数
	private TextView textView;// 知识回答

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.classquestion);
		initView();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("action.refreshQuestion");
		registerReceiver(mRefreshBroadcastReceiver, intentFilter);
		mQueue = SingleRequestQueue.getRequestQueue(getApplicationContext());
		imageLoader = new ImageLoader(mQueue, new BitmapCache());
		datata = new ArrayList<AnswerPer>();
		shooll_GK.setOnClickListener(this);
		tvQuick.setOnClickListener(this);
		// ivImgDetail.setOnClickListener(this);
		// getJsonText();
		judge();
		getJsonText(1);

		class_questionAapter = new Class_questionAapter_CQ(ClassQuestionActivity.this, datata);
		lvAnswer.setAdapter(class_questionAapter);
		lvAnswer.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(ClassQuestionActivity.this, QuestionDetail.class);
				String id2 = datata.get(position - 1).getId();
				intent.putExtra("questin_id", id2);
				intent.putExtra("position", position);
				startActivityForResult(intent, REQUSET);
			}
		});

	}

	private void judge() {
		SharedPreferences sharedPreferences = getSharedPreferences("chmod", 0);
		boolean b = sharedPreferences.getBoolean("isParent", true);
		boolean s = sharedPreferences.getBoolean("isStudent", true);
		if (b || s) {

		} else {
			textView.setText("");
			textView.setFocusable(false);
		}
	}

	private void initView() {
		// TODO Auto-generated method stub
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		tvQuick = (TextView) findViewById(R.id.tv_quick_questionaa_idid);
		lvAnswer = (PullToRefreshListView) findViewById(R.id.shouyeanwer_id);
		textView = (TextView) findViewById(R.id.tv_quick_questionaa_idid);
		ListViewIndicatorUtil.initIndicator(lvAnswer);// 改变下拉刷新的字体
		// 刷新
		lvAnswer.setOnRefreshListener(this);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		Intent intent = null;
		switch (arg0.getId()) {
		case R.id.shooll_GK:
			finish();
			break;
		case R.id.tv_quick_questionaa_idid:
			intent = new Intent(ClassQuestionActivity.this, MainActivitypoth.class);
			startActivity(intent);
			break;
		default:
			break;
		}

	}

	private int p = 0;// 取回的页数

	/**
	 * 获取提问问题
	 */
	private void getJsonText(final int i) {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			@SuppressLint("HandlerLeak")
			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();

					try {
						JSONObject jsonObject = new JSONObject(result);
						JSONArray jsonArray = jsonObject.getJSONArray("data");
						p = jsonObject.getInt("page_count");
						for (int i = 0; i < jsonArray.length(); i++) {
							answerPer = new AnswerPer();
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							JSONArray jsonArray2 = jsonObject2.getJSONArray("data_image");
							int length = jsonArray2.length();
							System.out.println(length);
							dataPerson = new ArrayList<OneDatyPerson>();
							for (int j = 0; j < jsonArray2.length(); j++) {
								JSONObject opt = (JSONObject) jsonArray2.opt(j);
								String object = (String) opt.get("image");
								OneDatyPerson oneDatyPerson = new OneDatyPerson();
								oneDatyPerson.setStudentytiem(object);
								dataPerson.add(oneDatyPerson);

							}
							String parent_name = (String) jsonObject2.get("parent_name");
							String student_class = (String) jsonObject2.get("student_class");
							String student_name = (String) jsonObject2.get("student_name");
							String date = (String) jsonObject2.get("date");
							String portrait = (String) jsonObject2.get("portrait");// 头像
							Integer iduser = (Integer) jsonObject2.get("id");// id
							Integer answer_count = (Integer) jsonObject2.get("answer_count");// 回答数量
							stranswer_count = answer_count.toString();
							String id = iduser.toString();
							String note = (String) jsonObject2.get("note");
							String course = (String) jsonObject2.get("course");
							answerPer.setStudentName(student_name);
							answerPer.setParentName("(家长:" + parent_name + ")");
							answerPer.setClassbanji(student_class);
							answerPer.setCuonte(stranswer_count);
							answerPer.setKemu(course);
							answerPer.setTime(date);
							answerPer.setNoteStr(note);
							answerPer.setTitle(portrait);
							answerPer.setImage_lsit_person(dataPerson);
							answerPer.setId(id);
							datata.add(answerPer);
						}

						class_questionAapter.notifyDataSetChanged();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (msg.what == 6) {
					// Toast.makeText(QuestionDetail.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					// Toast.makeText(QuestionDetail.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("page", i + "")); // 页数
				params.add(new BasicNameValuePair("page_count", p + ""));// 总页数
				String result = Tools.sendGet(Constant.answerList, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

	/**
	 * 增加回答人数
	 */
	/*@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUSET && resultCode == RESULT_OK) {
			String string = data.getStringExtra("addContent");
			int strint = data.getIntExtra("position_back", 0);
			if (string != null) {
				datata.get(strint).setCuonte(string);
				class_questionAapter.notifyDataSetChanged();// 底层数据改变，，让数据集刷新自己
			}

		}
	}*/

	class Class_questionAapter_CQ extends BaseAdapter {
		private Context context;
		private LayoutInflater layoutInflater;
		private ArrayList<AnswerPer> data;
		private int layoutID;

		Class_questionAapter_CQ(Context context, ArrayList<AnswerPer> data) {
			this.layoutInflater = LayoutInflater.from(context);
			this.layoutID = layoutID;
			this.data = data;
			this.context = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			final ViewHolder viewHolder;
			if (convertView == null) {
				convertView = layoutInflater.inflate(R.layout.class_questiadpter, null);
				viewHolder = new ViewHolder();
				viewHolder.image = (NetworkImageView) convertView.findViewById(R.id.head_portrait);
				// viewHolder.answerName = (TextView) convertView
				// .findViewById(R.id.question_Name_id);
				viewHolder.gridView = (MyGridView) convertView.findViewById(R.id.classgridview_ididid);
				viewHolder.gridView.setHorizontalSpacing(10);
				viewHolder.gridView.setVerticalSpacing(10);
				viewHolder.count = (TextView) convertView.findViewById(R.id.answerconut_id);
				viewHolder.note = (TextView) convertView.findViewById(R.id.note_id);
				viewHolder.time = (TextView) convertView.findViewById(R.id.time_id);
				viewHolder.kemu = (TextView) convertView.findViewById(R.id.sujiectName_id);
				viewHolder.stName = (TextView) convertView.findViewById(R.id.parent_id);
				viewHolder.parName = (TextView) convertView.findViewById(R.id.parentName_id);
				viewHolder.classbanji = (TextView) convertView.findViewById(R.id.class_name);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			ArrayList<OneDatyPerson> image_lsit = data.get(position).getImage_lsit_person();
			Class_QuestionGridView classAblumMSGAdapter = new Class_QuestionGridView(context, image_lsit);
			viewHolder.gridView.setAdapter(classAblumMSGAdapter);
			viewHolder.count.setText(data.get(position).getCuonte() + "人回答");
			viewHolder.note.setText(Html.fromHtml(DropHtml.drop(data.get(position).getNoteStr())));
			viewHolder.time.setText(data.get(position).getTime());
			viewHolder.stName.setText(data.get(position).getStudentName());
			viewHolder.kemu.setText(data.get(position).getKemu());
			viewHolder.parName.setText(data.get(position).getParentName());
			viewHolder.image.setImageUrl(data.get(position).getTitle(), imageLoader);
			// viewHolder.image.setImageURI(Uri.parse(data.get(position).getTitle()));
			viewHolder.classbanji.setText(data.get(position).getClassbanji());
			return convertView;

		}

		public class ViewHolder {
			public TextView count;
			public TextView note;
			public NetworkImageView image;
			public TextView time;
			public TextView stName;
			public TextView kemu;
			public TextView parName;
			public TextView classbanji;
			public MyGridView gridView;
		}

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(mRefreshBroadcastReceiver);
	}

	@Override
	public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				datata.clear();
				getJsonText(1);
				i=2;
				lvAnswer.onRefreshComplete();
			}
		}, 3000);
	}

	@Override
	public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				getJsonText(i);
				i++;
				lvAnswer.onRefreshComplete();
			}
		}, 3000);
	}

	private BroadcastReceiver mRefreshBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals("action.refreshQuestion")) {
				onPullDownToRefresh(lvAnswer);
			}
		}
	};
	private RequestQueue mQueue;
	private ImageLoader imageLoader;
}
