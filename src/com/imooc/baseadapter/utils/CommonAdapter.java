/*
 * @Title CommonAdapter.java
 * @Copyright Copyright 2010-2015 Yann Software Co,.Ltd All Rights Reserved.
 * @Description��
 * @author Yann
 * @date 2015-8-5 ����10:39:05
 * @version 1.0
 */
package com.imooc.baseadapter.utils;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * ��ע��
 * 
 * @author Yann
 * @date 2015-8-5 ����10:39:05
 */
public abstract class CommonAdapter<T> extends BaseAdapter {
	protected Context mContext;
	protected List<T> mDatas;
	protected LayoutInflater mInflater;
	// protected int mlayoutId;
	protected int selectPic = -1;

	public CommonAdapter(Context context, List<T> datas) {
		this.mContext = context;
		this.mDatas = datas;
		// this.mlayoutId = layoutId;
		mInflater = LayoutInflater.from(context);
	}

	/**
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return mDatas.size();
	}

	/**
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public T getItem(int position) {
		return mDatas.get(position);
	}

	/**
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {

		return position;
	}

	public void setNotifyDataChange(int id) {
		selectPic = id;
		super.notifyDataSetChanged();
	}

	/**
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 *      android.view.ViewGroup)
	 */
	// @Override
	// public abstract View getView(int position, View convertView, ViewGroup
	// parent);

	@Override
	public abstract View getView(int position, View convertView,
			ViewGroup parent);
	// {
	// ViewHolderDemo holder = ViewHolderDemo.get(mContext, convertView, parent,
	// mlayoutId, position);
	//
	// convert(holder, getItem(position));
	//
	// return holder.getConvertView();
	// }

	// public abstract void convert(ViewHolderDemo holder, T t);
}
