package com.example.space;

import java.util.ArrayList;
import java.util.HashMap;

import com.example.bean.BlogContentImageBean;
import com.example.monthstar.OneDatyPerson;

import android.graphics.Bitmap;
import android.widget.TextView;

public class AnswerPer {
	private String answerNameStr;// 姓名
	private String cuonte;// 数量
	private String noteStr;// 内容
	private String bitmap;// 头像
	private String time;// 时间
	private String kemu;// 科目
	private String StudentName;// 学生姓名
	private String classbanji;// 班级
	private String comment_id;// 每天item的id
	private boolean b;
	private String id;
	private String title;// 头像
	private String tou;

	public String getTou() {
		return tou;
	}

	public void setTou(String tou) {
		this.tou = tou;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isB() {
		return b;
	}

	public void setB(boolean b) {
		this.b = b;
	}

	public String getComment_id() {
		return comment_id;
	}

	public void setComment_id(String comment_id) {
		this.comment_id = comment_id;
	}

	private ArrayList<BlogContentImageBean> image_lsit;

	public ArrayList<BlogContentImageBean> getImage_lsit() {
		return image_lsit;
	}

	public void setImage_lsit(ArrayList<BlogContentImageBean> image_lsit) {
		this.image_lsit = image_lsit;
	}

	private ArrayList<OneDatyPerson> image_lsit_person;

	public ArrayList<OneDatyPerson> getImage_lsit_person() {
		return image_lsit_person;
	}

	public void setImage_lsit_person(ArrayList<OneDatyPerson> image_lsit_person) {
		this.image_lsit_person = image_lsit_person;
	}

	public String getClassbanji() {
		return classbanji;
	}

	public void setClassbanji(String classbanji) {
		this.classbanji = classbanji;
	}

	public String getKemu() {
		return kemu;
	}

	public void setKemu(String kemu) {
		this.kemu = kemu;
	}

	public String getStudentName() {
		return StudentName;
	}

	public void setStudentName(String studentName) {
		StudentName = studentName;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	private String parentName;// 家长姓名

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getAnswerNameStr() {
		return answerNameStr;
	}

	public void setAnswerNameStr(String answerNameStr) {
		this.answerNameStr = answerNameStr;
	}

	public String getCuonte() {
		return cuonte;
	}

	public void setCuonte(String cuonte) {
		this.cuonte = cuonte;
	}

	public String getNoteStr() {
		return noteStr;
	}

	public void setNoteStr(String noteStr) {
		this.noteStr = noteStr;
	}

	public String getBitmap() {
		return bitmap;
	}

	public void setBitmap(String bitmap) {
		this.bitmap = bitmap;
	}

}
