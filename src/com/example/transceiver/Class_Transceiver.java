package com.example.transceiver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.example.global.Constant;
import com.example.jhf.R;
import com.example.space.ClassAlbum;
import com.example.transceiver.Shool_Transceiver.MySeekbar;
import com.imooc.baseadapter.utils.Tools;

/**
 * 班级广播
 * 
 * @author Administrator
 * 
 */
public class Class_Transceiver extends Activity implements OnClickListener {
	private Shool_TransceiverPer shoolper;
	private ArrayList<Shool_TransceiverPer> data;
	private ListView mListView;
	private ImageView shooll_GK;
	private ImageButton btnnext;// 下一曲
	private ImageButton btnup;// 上一曲
	private MediaPlayer player;
	private ImageButton btnPlayer;
	private boolean ifplay = false; // 是否播放
	private boolean iffirst = false;
	private SeekBar seekBar;
	private Timer mTimer;
	private TimerTask mTimerTask;
	private boolean isChanging = false;// 互斥变量，防止定时器与SeekBar拖动时进度冲突
	private String dateStr;// 时间
	private String tieleStr;// 提标
	private String filesize;// 提标
	private String code;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private String url;
	private int ms;// 所有音乐的数量
	private int index = 0;
	private ObjectMapper om = new ObjectMapper();
	private Shool_TransceiverAdapter shool_TransceiverAdapter;
	private String next;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.class_transceiver);

		data = new ArrayList<Shool_TransceiverPer>();
		init();
		getJsonText();
		back();
		musicUp();
		muicsnext();
	}

	private void back() {
		shooll_GK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Class_Transceiver.this.finish();
			}
		});

	}

	/**
	 * 上一曲
	 */
	private void musicUp() {
		btnup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (index == 0) {
					// index = ms;
					Toast.makeText(Class_Transceiver.this, "当前已经是第一首歌曲了",
							Toast.LENGTH_SHORT).show();
				} else {
					// Toast.makeText(Shool_Transceiver.this, "请稍等，正在加载……",
					// Toast.LENGTH_SHORT).show();
					index = index - 1;
					shool_TransceiverAdapter.setSelectItem(index);
					shool_TransceiverAdapter.notifyDataSetInvalidated();

					String next = data.get(index).getURLStr();

					if (player != null) {
						// 开始播放，按钮设置成暂停
						btnPlayer.setBackgroundResource(R.drawable.music_pause);
						player.reset();
						try {
							player.setOnPreparedListener(new OnPreparedListener() {
								@Override
								public void onPrepared(MediaPlayer mp) {
									player.start();
									seekBar.setMax(player.getDuration());// 设置进度条
									// ----------定时器记录播放进度---------//
									mTimer = new Timer();
									mTimerTask = new TimerTask() {
										@Override
										public void run() {
											if (isChanging == true) {
												return;
											}
											seekBar.setProgress(player
													.getCurrentPosition());
										}
									};
									mTimer.schedule(mTimerTask, 0, 10);
								}
							});
							player.setDataSource(next);
							// player.prepare();// 准备
							player.prepareAsync();

						} catch (IllegalArgumentException e) {
							e.printStackTrace();
						} catch (IllegalStateException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}

					}

					// player.start();// 开始
					ifplay = true;

				}

			}
		});
	}

	/**
	 * 下一曲
	 */
	@SuppressLint("ShowToast")
	private void muicsnext() {
		btnnext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				index = index + 1;
				if (index < ms) {
					// Toast.makeText(Shool_Transceiver.this, "请稍等，正在加载……",
					// Toast.LENGTH_SHORT).show();
					next = data.get(index).getURLStr();

				} else {
					index = 0;
					// Toast.makeText(Shool_Transceiver.this, "请稍等，正在加载……",
					// Toast.LENGTH_SHORT).show();
					if(data.size()>1){
						next = data.get(index).getURLStr();
					}
				}

				shool_TransceiverAdapter.setSelectItem(index);
				shool_TransceiverAdapter.notifyDataSetInvalidated();

				if (player != null) {
					// 开始播放，按钮设置成暂停
					btnPlayer.setBackgroundResource(R.drawable.music_pause);
					player.reset();
					try {
						player.setOnPreparedListener(new OnPreparedListener() {
							@Override
							public void onPrepared(MediaPlayer mp) {

								seekBar.setMax(player.getDuration());// 设置进度条
								// ----------定时器记录播放进度---------//
								mTimer = new Timer();
								mTimerTask = new TimerTask() {
									@Override
									public void run() {
										if (isChanging == true) {
											return;
										}
										seekBar.setProgress(player
												.getCurrentPosition());
									}
								};
								mTimer.schedule(mTimerTask, 0, 10);
								player.start();
							}
						});
						if(data.size()>1){
							player.setDataSource(next);
							// player.prepare();// 准备
							player.prepareAsync();
						}
						

					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalStateException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}

				}

				// player.start();// 开始
				ifplay = true;

			}
		});
	}


	private void init() {
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		btnnext = (ImageButton) findViewById(R.id.imageButton_next);
		btnup = (ImageButton) findViewById(R.id.imageButton_up);
		seekBar = (SeekBar) findViewById(R.id.seekbarid);
		btnPlayer = (ImageButton) findViewById(R.id.btn_player);
		mListView = (ListView) findViewById(R.id.listView_classid);
		btnPlayer.setOnClickListener(this);
		seekBar.setOnSeekBarChangeListener(new MySeekbar());
		player = new MediaPlayer();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.btn_player:
			if (player != null && !ifplay) {
				// 开始播放，按钮设置成暂停
				btnPlayer.setBackgroundResource(R.drawable.music_pause);
				if (!iffirst) {
					player.reset();
					try {
						player.setOnPreparedListener(new OnPreparedListener() {
							@Override
							public void onPrepared(MediaPlayer mp) {
								seekBar.setMax(player.getDuration());// 设置进度条
								// ----------定时器记录播放进度---------//
								mTimer = new Timer();
								mTimerTask = new TimerTask() {
									@Override
									public void run() {
										if (isChanging == true) {
											return;
										}
										seekBar.setProgress(player
												.getCurrentPosition());
									}
								};
								mTimer.schedule(mTimerTask, 0, 10);
								player.start();
							}
						});
						if(url!=null){
							player.setDataSource(url);
							// player.prepare();// 准备
							player.prepareAsync();
						}
						

					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalStateException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}

					iffirst = true;
				}

				 player.start();// 开始
				ifplay = true;
			} else if (ifplay) {
				// 设置图片为播放按钮
				// play_pause.setText("继续");
				btnPlayer.setBackgroundResource(R.drawable.musci_player);
				player.pause();
				ifplay = false;
			}
			break;

		default:
			break;
		}
	}

	// 进度条处理
	class MySeekbar implements OnSeekBarChangeListener {
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
		}

		public void onStartTrackingTouch(SeekBar seekBar) {
			isChanging = true;
		}

		public void onStopTrackingTouch(SeekBar seekBar) {
			player.seekTo(seekBar.getProgress());
			isChanging = false;
		}

	}

	// 来电处理
	protected void onDestroy() {
		if (player != null) {
			if (player.isPlaying()) {
				player.stop();
			}
			// player.release();
		}
		super.onDestroy();
	}

	protected void onPause() {
		if (player != null) {
			if (player.isPlaying()) {
				player.pause();
			}
		}
		super.onPause();
	}

	protected void onResume() {
		if (player != null) {
			if (!player.isPlaying()) {
				player.start();
			}
		}
		super.onResume();
	}

	/**
	 * 获取班级广播的JSON数据
	 */
	@SuppressLint("HandlerLeak")
	private void getJsonText() {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;
			

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						jsonObject = new JSONObject(result);
						jsonArray = jsonObject.getJSONArray("data");
						for (int i = 0; i < jsonArray.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							dateStr = (String) jsonObject2.get("date");
							tieleStr = (String) jsonObject2.get("title");
							filesize = (String) jsonObject2.get("file_size");
							url = jsonObject2.get("url").toString();
							shoolper = new Shool_TransceiverPer();
							shoolper.setTime(dateStr);
							shoolper.setTileStr(tieleStr);
							shoolper.setContentStr(filesize);
							shoolper.setURLStr(url);
							data.add(shoolper);
						}
						shool_TransceiverAdapter = new Shool_TransceiverAdapter(
								Class_Transceiver.this, data);
						mListView.setAdapter(shool_TransceiverAdapter);
						mListView
								.setOnItemClickListener(new OnItemClickListener() {
									@Override
									public void onItemClick(
											AdapterView<?> arg0, View arg1,
											int arg2, long arg3) {

										try {
											JSONObject jsonObject3 = (JSONObject) jsonArray
													.opt(arg2);
											index = jsonArray.getInt(arg2);
											url = jsonObject3.get("url")
													.toString();
										} catch (JSONException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
										if (player != null) {
											// 开始播放，按钮设置成暂停
											btnPlayer
													.setBackgroundResource(R.drawable.music_pause);
											player.reset();
											try {
												player.setDataSource(url);
												player.prepare();// 准备

											} catch (IllegalArgumentException e) {
												e.printStackTrace();
											} catch (IllegalStateException e) {
												e.printStackTrace();
											} catch (IOException e) {
												e.printStackTrace();
											}
											seekBar.setMax(player.getDuration());// 设置进度条
											// ----------定时器记录播放进度---------//
											mTimer = new Timer();
											mTimerTask = new TimerTask() {
												@Override
												public void run() {
													if (isChanging == true) {
														return;
													}
													seekBar.setProgress(player
															.getCurrentPosition());
												}
											};
											mTimer.schedule(mTimerTask, 0, 10);
										}

										player.start();// 开始
										ifplay = true;

									}
								});
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// Intent intent = new Intent();
					// intent.setClass(ShoolJS.this, IndexActivity.class);
					// startActivity(intent);
				} else if (msg.what == 6) {
					Toast.makeText(Class_Transceiver.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(Class_Transceiver.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				String result = Tools.sendGet(Constant.radioClass, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}
}
