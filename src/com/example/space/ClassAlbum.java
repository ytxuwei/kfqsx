package com.example.space;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.global.Constant;
import com.example.jhf.R;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.imooc.baseadapter.utils.Tools;

/**
 * 班级相册
 * 
 * @author Administrator
 * 
 */
public class ClassAlbum extends Activity implements OnRefreshListener2<GridView> {
	private int i = 2;
	private ArrayList<ClassAlbumPer> data;
	private ClassAlbumPer per;
	private PullToRefreshGridView gridView;
	private ImageView shooll_GK;
	private ObjectMapper om = new ObjectMapper();
	private String code;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private JSONObject jsonObject2;
	private ImageView houndview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.classalbum);

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("action.refreshGridView");
		registerReceiver(mRefreshBroadcastReceiver, intentFilter);
		data = new ArrayList<ClassAlbumPer>();
		gridView = (PullToRefreshGridView) findViewById(R.id.classgv_id);
		gridView.setOnRefreshListener(this);
		 getJsonText(1);
		houndview = (ImageView) findViewById(R.id.cr_id);
		// 判断权限
		SharedPreferences spChmod = getSharedPreferences("chmod", MODE_PRIVATE);
		if (spChmod.getBoolean("isOffice", false) || spChmod.getBoolean("isPresident", false) || spChmod.getBoolean("isTeacher", false) || spChmod.getBoolean("isDirector", false)) {
			houndview.setVisibility(View.VISIBLE);
			houndview.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					startActivity(new Intent(ClassAlbum.this, FoundMainActivitypoth.class));
				}
			});
		}

		cAdapter = new ClassAlbumAdapter(ClassAlbum.this, data);
		gridView.setAdapter(cAdapter);

		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		// url图片转化

		shooll_GK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ClassAlbum.this.finish();
			}
		});

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				try {
					Intent intent = new Intent(ClassAlbum.this, ClassAlbumMSG.class);
					String start_id = data.get(arg2).getId();
					intent.putExtra("class_albume_id_UP", start_id);
					Log.v("OneDay", "start_id" + start_id);
					String title = data.get(arg2).getClasstutiem();
					intent.putExtra("titleName_id", title);
					startActivity(intent);
				} catch (Exception e) {
					// TODO: handle exceptions
				}
			}
		});

	}

	private int p = 0;// 取回的页数
	private ClassAlbumAdapter cAdapter;

	/**
	 * 获取班级相册的JSON数据
	 */
	@SuppressLint("HandlerLeak")
	private void getJsonText(final int i) {
		final Handler handler = new Handler() {

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						jsonObject = new JSONObject(result);
						jsonArray = jsonObject.getJSONArray("data");
						for (int i = 0; i < jsonArray.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							per = new ClassAlbumPer();
							String userNameString = (String) jsonObject2.get("user_name");
							String strImage = (String) jsonObject2.get("image");
							String strtitle = (String) jsonObject2.get("title");
							String strreade = jsonObject2.getString("read_count");
							String strname = (String) jsonObject2.get("user_name");
							String strId = jsonObject2.getString("id");
							per.setClasstuNum(strreade);
							per.setClasszhuozhe(strname);
							per.setClasstutiem(strtitle);
							per.setUrlStr(strImage);
							per.setId(strId);

							per.setClasstubiao(strtitle);
							data.add(per);
						}
						cAdapter.notifyDataSetChanged();
						// 展开相册
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (msg.what == 6) {
					Toast.makeText(ClassAlbum.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(ClassAlbum.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("page", i + "")); // 页数
				params.add(new BasicNameValuePair("page_count", p + ""));// 总页数
				String result = Tools.sendGet(Constant.photos, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unregisterReceiver(mRefreshBroadcastReceiver);  
	}
	@Override
	public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				data.clear();
				getJsonText(1);
				i=2;
				gridView.onRefreshComplete();
			}
		}, 3000);
	}

	@Override
	public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				getJsonText(i);
				i++;
				gridView.onRefreshComplete();
			}
		}, 3000);
	}
	private BroadcastReceiver mRefreshBroadcastReceiver = new BroadcastReceiver() {  
		  
	      @Override  
	      public void onReceive(Context context, Intent intent) {  
	          String action = intent.getAction();  
	          if (action.equals("action.refreshGridView"))  
	          {  
	              onPullDownToRefresh(gridView);  
	          }  
	      }  
	  };  
	  
}
