package com.example.jhf;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.example.centre.AccreditationActvity;
import com.example.centre.Center_XC;
import com.example.centre.Personal;
import com.example.centre.SetUp;
import com.example.global.Constant;
import com.example.view.CircleImageView;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.BitmapUtils;
import com.imooc.baseadapter.utils.Tools;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.AvoidXfermode.Mode;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class FourFragment extends Fragment implements OnClickListener {
	private boolean isLoged;
	private TextView toMessage;
	private TextView toMyCollection;
	private TextView help;
	private String[] str = { "个人资料", "设置" };
	private int[] inc = { 1, 3 };
	private int[] intL = { R.drawable.arrow_right };
	private Context context;
	private ArrayList<Map<String, Object>> data;
	private FourAdapter cpAdapter;
	private ListView mListView;
	private CircleImageView userAvar;
	private String[] items = new String[] { "选择本地图片", "拍照" };
	/* 头像名称 */
	private static final String IMAGE_FILE_NAME = "user.jpg";

	/* 请求码 */
	private static final int IMAGE_REQUEST_CODE = 0;
	private static final int CAMERA_REQUEST_CODE = 1;
	private static final int RESULT_REQUEST_CODE = 2;
	private ImageView ivAccreditation;
	private TextView tvName;
	private String uid;
	private SharedPreferences sp;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		

	}

	/*
	 * @Override public void onStop() { // TODO Auto-generated method stub
	 * super.onStop();
	 * ivAccreditation.setBackgroundResource(R.drawable.iv_accreditation_gold);
	 * }
	 */

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		
		View view = inflater
				.inflate(R.layout.custom_touxiang, container, false);
		SharedPreferences spUid = getActivity().getSharedPreferences("UserId",0);
		uid = spUid.getString("uid", "0");
		sp = getActivity().getSharedPreferences("user", 0);
		context = getActivity();
		userAvar = (CircleImageView) view.findViewById(R.id.user_avatar);
		//userAvar.setImageBitmap(BitmapUtils.stringtoBitmap(sp.getString("portrait", "")));
		String image = sp.getString("portrait", "");
		RequestQueue mQueue = Volley.newRequestQueue(context);
		ImageLoader imageLoader = new ImageLoader(mQueue, new BitmapCache());
		userAvar.setImageUrl(image, imageLoader);
		
		tvName = (TextView) view.findViewById(R.id.tv_rl_four_fragment);
		tvName.setText(sp.getString("userName", ""));
		mListView = (ListView) view.findViewById(R.id.lv_CTID);
		// userAvar=(CustomImage) view.findViewById(R.id.user_avatar);
		// userAvar.setOnClickListener(this);
		cpAdapter = new FourAdapter(context, intnn(),
				R.layout.custom_touxiangitem);

		ivAccreditation = (ImageView) view.findViewById(R.id.iv_accreditation);
		String isConfirm = sp.getString("isConfirm", "false");
		if (isConfirm.equals("true")) {
			ivAccreditation
					.setBackgroundResource(R.drawable.iv_accreditation_gold);
		}
		SharedPreferences confirm = getActivity().getSharedPreferences(
				"confirm", 0);
		String ifConfirm = confirm.getString("isConfirm", "false");
		if (ifConfirm.equals("true")) {
			ivAccreditation
					.setBackgroundResource(R.drawable.iv_accreditation_gold);
		}

		mListView.setAdapter(cpAdapter);
		Neme();
		init();
		return view;
	}

	private List<Map<String, Object>> intnn() {
		data = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < str.length; i++) {
			Map<String, Object> list = new HashMap<String, Object>();
			list.put("图标", inc[i]);
			list.put("项目", intL[0]);
			list.put("进入", str[i]);
			data.add(list);
		}
		return data;
	}

	private void init() {
		userAvar.setOnClickListener(this);

		ivAccreditation.setOnClickListener(this);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		SharedPreferences confirm = getActivity().getSharedPreferences(
				"confirm", 0);
		String ifConfirm = confirm.getString("isConfirm", "false");
		if (ifConfirm.equals("true")) {
			ivAccreditation
					.setBackgroundResource(R.drawable.iv_accreditation_gold);
		}
	}
 
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {

			switch (requestCode) {
			case IMAGE_REQUEST_CODE:
				startPhotoZoom(data.getData());
				break;
			case CAMERA_REQUEST_CODE:
				if (Toolss.hasSdcard()) {

					File tempFile = new File(
							Environment.getExternalStorageDirectory(),
							IMAGE_FILE_NAME);
					if (tempFile.exists()) {
						startPhotoZoom(Uri.fromFile(tempFile));
					} else {
						Toolss.disInfo(getActivity(), "文件不存在");
					}

				} else {
					Toolss.disInfo(getActivity(), "未找到存储卡，无法存储照片！");
				}

				break;
			case RESULT_REQUEST_CODE:
				if (data != null) {
					getImageToView(data);
				}
				break;
			default:
				break;
			}

		}

	}

	private void getImageToView(Intent data) {
		// TODO Auto-generated method stub
		Bundle extras = data.getExtras();
		if (extras != null) {
			Bitmap photo = extras.getParcelable("data");
			// Drawable drawable = new BitmapDrawable(photo);
			userAvar.setImageBitmap(photo);
			//把头像上传呗
			String convertBitmapToString = BitmapUtils.convertBitmapToString(photo);
			sendPortraitToNet(convertBitmapToString);
			//sp.edit().putString("portrait", convertBitmapToString).commit() ;
			
		}
	}

	/**
	 * 把头像上传
	 */
	private void sendPortraitToNet(final String photo) {
		// TODO Auto-generated method stub
		final Handler handler = new Handler() {
			private JSONObject jsonObject;
			
			public void handleMessage(Message msg) {
			}

		};
		new Thread() {
			public void run() {
				// 设置参数
					List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
					params.add(new BasicNameValuePair("db", Constant.db));// 数据库
					params.add(new BasicNameValuePair("uid", uid)); // uid
					params.add(new BasicNameValuePair("portrait", photo)); // 密码
					String result = Tools.sendPost(Constant.updatePortrait, params);
					Message message = new Message();
					message.what = 7;
					message.obj = result;
					handler.sendMessage(message);
			};
		}.start();
	}

	/**
	 * 裁剪图
	 * 
	 * @param uri
	 */
	private void startPhotoZoom(Uri uri) {
		// TODO Auto-generated method stub
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		// 设置裁剪
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 320);
		intent.putExtra("outputY", 320);
		intent.putExtra("return-data", true);
		startActivityForResult(intent, RESULT_REQUEST_CODE);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub

		switch (view.getId()) {
		case R.id.user_avatar:
			showDialog();
			break;
		case R.id.iv_accreditation:
			Intent intent = new Intent(context, AccreditationActvity.class);
			startActivity(intent);

		default:
			break;
		}

	}

	private void showDialog() {
		// TODO Auto-generated method stub
		Builder dialog = new AlertDialog.Builder(getActivity());
		dialog.setTitle("设置头像");
		dialog.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialogInterface, int which) {
				// TODO Auto-generated method stub
				switch (which) {
				case 0:
					Intent intentFromGallery = new Intent();
					intentFromGallery.setType("image/*");// 设置文件类型
					intentFromGallery.setAction(Intent.ACTION_GET_CONTENT);
					startActivityForResult(intentFromGallery,
							IMAGE_REQUEST_CODE);
					break;
				case 1:
					if (Toolss.hasSdcard()) {
						Intent intentFromCapture = new Intent(
								MediaStore.ACTION_IMAGE_CAPTURE);
						// 判断存储卡是否可以用，可用进行存储

						intentFromCapture.putExtra(MediaStore.EXTRA_OUTPUT, Uri
								.fromFile(new File(Environment
										.getExternalStorageDirectory(),
										IMAGE_FILE_NAME)));

						startActivityForResult(intentFromCapture,
								CAMERA_REQUEST_CODE);
					}
					break;
				default:
					break;
				}
			}
		});
		dialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int arg1) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		dialog.show();

	}

	public void Neme() {
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				@SuppressWarnings("unchecked")
				HashMap<String, String> map = (HashMap<String, String>) mListView
						.getItemAtPosition(arg2);
				String keyStr = map.get("进入");
				if (keyStr.equals("个人资料")) {
					Intent intent = new Intent();
					intent.setClass(context, Personal.class);
					startActivity(intent);
				}
				// if (keyStr.equals("消息")) {
				// Intent intent=new Intent();
				// intent.setClass(context, Center_XC.class);
				// startActivity(intent);
				// }
				if (keyStr.equals("设置")) {
					Intent intent = new Intent();
					intent.setClass(context, SetUp.class);
					startActivity(intent);
				}
			}
		});
	}
}
