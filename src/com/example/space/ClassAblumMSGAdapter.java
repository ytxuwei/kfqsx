package com.example.space;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import android.R.integer;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.example.bean.BlogContentImageBean;
import com.example.jhf.R;
import com.example.jhf.ShoolDAdater.ViewHolder;
import com.example.monthstar.OneDatyPerson;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.BitmapUtils;
import com.imooc.baseadapter.utils.CommonAdapter;
import com.imooc.baseadapter.utils.ViewHolderDemo;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

public class ClassAblumMSGAdapter extends CommonAdapter<OneDatyPerson> {
	private Context context;
	private ImageView iView;
	private String image;// 访问网络的URL
	private int h;
	private Bitmap returnBitMap;
	private LayoutInflater layoutInflater;

	public ClassAblumMSGAdapter(Context context, List<OneDatyPerson> datas) {
		super(context, datas);
		this.context = context;
		this.layoutInflater=LayoutInflater.from(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolderDemo viewHolder=null;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.classablummasgadapteritem, null);
			viewHolder = ViewHolderDemo.get(mContext, convertView, parent,
					R.layout.classablummasgadapteritem, position);
			convertView.setTag(viewHolder);
			
		} else {
			viewHolder =(ViewHolderDemo) convertView.getTag();
		}
		h = position;
		// 图片转换
		// RequestQueue mQueue = Volley.newRequestQueue(context);
		// ImageLoader imageLoader = new ImageLoader(mQueue, new BitmapCache());

		// image = mDatas.get(position).getStudentytiem();
		// BlogContentImageBean one=mDatas.get(position)

//		iView = viewHolder.getView(R.id.classblum_ivid);
		
		ImageLoader instance = ImageLoader.getInstance();
		instance.init(ImageLoaderConfiguration.createDefault(context));
//		/**
//		 * 防止图片
//		 */
//		ImageSize mImageSize = new ImageSize(100, 100);
//
//		// 显示图片的配置
//		DisplayImageOptions options = new DisplayImageOptions.Builder()
//				.cacheInMemory(true).cacheOnDisk(true)
//				.bitmapConfig(Bitmap.Config.RGB_565).build();
		// ImageLoader.getInstance().displayImage(mDatas.get(position).getStudentytiem(),
		// iView);
		// try {
		// ImageLoader.getInstance().loadImage(mDatas.get(position).getStudentytiem(),
		// mImageSize, options, new SimpleImageLoadingListener(){
		//
		// @Override
		// public void onLoadingComplete(String imageUri, View view,
		// Bitmap loadedImage) {
		// super.onLoadingComplete(imageUri, view, loadedImage);
		// iView.setImageBitmap(loadedImage);
		// }
		//
		// });
		// } catch (Exception e) {
		// // TODO: handle exception
		// e.printStackTrace();
		// }
		ImageView img = viewHolder.getView(R.id.classblum_ivid);
		ImageLoader.getInstance().displayImage(
				mDatas.get(position).getStudentytiem(), img);
		// com.nostra13.universalimageloader.core.ImageLoader.getInstance()
		// .loadImage(mDatas.get(position).getStudentytiem(),
		// new SimpleImageLoadingListener() {
		// @Override
		// public void onLoadingComplete(String imageUri,
		// View view, Bitmap loadedImage) {
		// // TODO Auto-generated method stub
		// super.onLoadingComplete(imageUri, view,
		// loadedImage);
		// iView.setImageBitmap(loadedImage);
		// }
		// });
		// final Handler handler=new Handler(){
		// public void handleMessage(Message msg) {
		// if (msg.what==7) {
		// iView.setImageBitmap(returnBitMap);
		// }
		//
		// }
		// };
		// new Thread() {
		// @Override
		// public void run() {
		// // returnBitMap = returnBitMap(image);
		//
		//
		// URL myFileUrl = null;
		// Bitmap bitmap = null;
		// Message message = new Message();
		// try {
		// myFileUrl = new URL(image);
		// } catch (MalformedURLException e) {
		// e.printStackTrace();
		// message.what=7;
		// handler.sendMessage(message);
		// }
		// try {
		// HttpURLConnection conn = (HttpURLConnection) myFileUrl
		// .openConnection();
		// conn.setDoInput(true);
		// conn.connect();
		// InputStream is = conn.getInputStream();
		// returnBitMap = BitmapFactory.decodeStream(is);
		// is.close();
		// message.what=7;
		// handler.sendMessage(message);
		// } catch (IOException e) {
		// e.printStackTrace();
		// message.what=7;
		// handler.sendMessage(message);
		// }
		//
		// }
		// }.start();
		//
		// if (selectPic == position) {
		// iView.setScaleType(ScaleType.CENTER_CROP);
		// } else {
		// iView.setScaleType(ScaleType.CENTER_INSIDE);
		// }
		// // 可放大缩小添加视图
		// // iView.setOnClickListener(new OnClickListener() {
		// //
		// // @Override
		// // public void onClick(View v) {
		// // QuestionDetail questionDetail = new QuestionDetail();
		// // questionDetail.setViewPagerAndZoom(iView, h);
		// // // setViewPagerAndZoom(v, position);
		// // }
		// // });

		return convertView;
	}
	/**
	 * 图片转换
	 * 
	 * @param url
	 * @return
	 */

	// public void setViewPagerAndZoom(View v, final int position) {
	//
	// // 得到要放大展示的视图界面
	// ViewPager expandedView = (ViewPager)findViewById(R.id.detail_view);
	// ViewPager expandedView=
	// // 最外层的容器，用来计算
	// View containerView = (FrameLayout) (findViewById(R.id.container));
	// // 实现放大缩小类，传入当前的容器和要放大展示的对象
	// final ZoomTutorial mZoomTutorial = new ZoomTutorial(containerView,
	// expandedView);
	// ViewPagerAdapter adapter = new
	// ViewPagerAdapter(ClassAblumMSGAdapter.this,
	// imageBean, mZoomTutorial);
	// expandedView.setAdapter(adapter);
	// expandedView.setCurrentItem(position);
	// // 通过传入Id来从小图片扩展到大图，开始执行动画
	// mZoomTutorial.zoomImageFromThumb(v);
	// }
}
