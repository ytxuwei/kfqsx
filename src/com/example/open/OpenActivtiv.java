package com.example.open;

import com.example.index.ShoolLI;
import com.example.jhf.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 五公开专栏首页
 * 
 * @author Administrator
 * 
 */
public class OpenActivtiv extends Activity {
	// private Button openzygg,openkc,openks,openqmjc,openjjr;
	private LinearLayout openzygg, openkc, openks, openqmjc, openjjr;
	private ImageView shooll_GK;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.open);
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		shooll_GK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				OpenActivtiv.this.finish();
			}
		});
		init();
		execute();
	}

	private void init() {
		openjjr = (LinearLayout) findViewById(R.id.openjjr);
		// openjjr=(Button) findViewById(R.id.openjjr);
		openkc = (LinearLayout) findViewById(R.id.openkc);
		openks = (LinearLayout) findViewById(R.id.openks);
		openqmjc = (LinearLayout) findViewById(R.id.openqmjc);
		openzygg = (LinearLayout) findViewById(R.id.openzygg);

	}

	private void execute() {
		openzygg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(OpenActivtiv.this,
						OpenzygkActivity.class));

			}
		});

		openkc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(OpenActivtiv.this,
						OpenkcActivity.class));

			}
		});
		openks.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(OpenActivtiv.this,
						OpenksActivity.class));

			}
		});
		openqmjc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(OpenActivtiv.this,
						OpenqmcsActivity.class));

			}
		});
		openjjr.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(OpenActivtiv.this,
						OpenjjrActivity.class));

			}
		});
	}
}
