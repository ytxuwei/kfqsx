package com.example.jhf;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.global.Constant;
import com.imooc.baseadapter.utils.Tools;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * 找回密码--输入验证码页面
 * 
 * @author Administrator
 * 
 */
public class ResetPassword extends Activity implements OnClickListener {
	private ObjectMapper om = new ObjectMapper();
	private String code;
	private EditText etGet;
	private Button btnGet;
	private ImageView ivBack;
	private String phoneNumStr;
	private String resultStr;
	private String verCodeStrback = "";// 接收的验证号码
	private String verCodeStr = "";// 验证码
	private String verCodePone = "";// 接收穿过来的手机号

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.reset_password);
		phoneNumStr = getIntent().getStringExtra("ihone_idback");
		verCodePone = getIntent().getStringExtra("ihone_Nab");
		// getJsonText();
		initView();

		/**
		 * 用户输入验证码，与服务器进行比对，正确则进入下一步
		 */
		btnGet.setOnClickListener(this);
		ivBack.setOnClickListener(this);
	}

	private void initView() {
		// TODO Auto-generated method stub
		etGet = (EditText) findViewById(R.id.et_get_code);
		btnGet = (Button) findViewById(R.id.btn_get_code);
		ivBack = (ImageView) findViewById(R.id.ver_mesBack);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.btn_get_code:
			verCodeStr = etGet.getText().toString().trim();
			if (TextUtils.isEmpty(verCodeStr)) {
				Toast.makeText(getApplicationContext(), "验证码不能为空", 0).show();
			} else if (verCodeStr.equals(phoneNumStr)) {// 验证码的判断
				Intent intent = new Intent();
				intent.setClass(ResetPassword.this, ChangePassword.class);
				intent.putExtra("phoneNum_back", verCodePone);// 将验证的手机号传递给下一个页面页面

				startActivity(intent);
				finish();
				// Intent intent = new Intent(ResetPassword.this,
				// ChangePassword.class);
				// startActivity(intent);
			} else {
				Toolss.disInfo(getApplicationContext(), "验证码有误");
			}
			break;
		case R.id.ver_mesBack:
			finish();
			break;
		default:
			break;
		}
	}

	// /**
	// * 获取的短信验证码JSON数据
	// */
	// @SuppressLint("HandlerLeak")
	// private void getJsonText() {
	// final Handler handler = new Handler() {
	//
	// public void handleMessage(Message msg) {
	// if (msg.what == 7) {
	// String result = msg.obj.toString();
	// try {
	// JSONObject jsonObject = new JSONObject(result);
	// JSONObject jsonObject3 = jsonObject
	// .getJSONObject("data");
	// verCodeStrback = (String) jsonObject3
	// .get("verificationCode");
	// } catch (JSONException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// } else if (msg.what == 6) {
	// Toast.makeText(ResetPassword.this, "帐号或密码错误", 0).show();
	// } else if (msg.what == 10) {
	// Toast.makeText(ResetPassword.this, "忘了联网了吧", 0).show();
	// }
	//
	// }
	//
	// };
	// new Thread() {
	// public void run() {
	// List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
	// params.add(new BasicNameValuePair("db", Constant.db));// 数据库
	// params.add(new BasicNameValuePair("phone", phoneNumStr));// 手机号
	// String result = Tools
	// .sendGet(Constant.verificationCode, params);
	// Message message = new Message();
	// try {
	// if (result != null) {
	// JsonNode jsonNode = om.readTree(result);
	// code = jsonNode.get("code").asText();
	// // history=jsonNode.get("note").asText();
	// if (code.equals("200")) {
	// message.what = 7;
	// message.obj = result;
	// } else {
	// message.what = 6;// 登陆失败
	// }
	// } else {
	// message.what = 9;// 无网络
	// }
	// handler.sendMessage(message);
	// } catch (Exception e) {
	// // TODO: handle exception
	// e.printStackTrace();
	// message.what = 10;
	// handler.sendMessage(message);
	// }
	// };
	// }.start();
	// }
}
