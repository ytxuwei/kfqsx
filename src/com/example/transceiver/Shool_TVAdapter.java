package com.example.transceiver;

import java.util.ArrayList;

import org.apache.http.util.EncodingUtils;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.example.jhf.R;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.SingleRequestQueue;

public class Shool_TVAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private ArrayList<Shool_TVPer> data;
	private int layoutID;
	private RequestQueue mQueue;
	private ImageLoader imageLoader;

	public Shool_TVAdapter(Context context, ArrayList<Shool_TVPer> data) {
		this.context = context;
		this.layoutInflater = LayoutInflater.from(context);
		this.layoutID = layoutID;
		this.data = data;
		mQueue = SingleRequestQueue.getRequestQueue(context);
		imageLoader = new ImageLoader(mQueue, new BitmapCache());
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		final ViewHolder viewHolder;
		
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.shool_tvtiem, null);
			viewHolder = new ViewHolder();
			viewHolder.image = (NetworkImageView) convertView
					.findViewById(R.id.classxc_id);
			viewHolder.title_ST_id = (TextView) convertView
					.findViewById(R.id.title_TV_id);
			viewHolder.title_ST_id.setFocusable(false);
			viewHolder.content_ST_id = (TextView) convertView
					.findViewById(R.id.content_TV_id);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.content_ST_id.setText(data.get(position).getTitleStr());
		// WebView webview_school = data.get(position).getWebview_school();
		String noteStr = data.get(position).getContentStr();
		Html.fromHtml(noteStr);
		viewHolder.title_ST_id.setText(Html.fromHtml(noteStr));
		viewHolder.title_ST_id.setBackgroundColor(Color.parseColor("#f0f0f0"));
		// viewHolder.title_ST_id.setFocusable(true);
		// viewHolder.title_ST_id.setFocusableInTouchMode(true);
		// int a = 12; // �����С
		// String strUrl = "<html> \n" + "<head> \n"
		// + "<style type=\"text/css\"> \n"
		// + "body {text-align:justify; font-size: " + a
		// + "px; line-height: " + (a + 6) + "px}\n" + "</style> \n"
		// + "</head> \n" + "<body>"
		// + EncodingUtils.getString(noteStr.getBytes(), "UTF-8")
		// + "</body> \n </html>";
		//
		// viewHolder.title_ST_id.loadData(strUrl, "text/html; charset=UTF-8",
		// null);
		// viewHolder.content_ST_id.setText(data.get(position).getContentStr());
		// viewHolder.content_ST_id.setWebViewClient(data.get(position).getWebview_school());
		// viewHolder.image.setImageResource(data.get(position).getImageId());
		// viewHolder.image.setImageBitmap(data.get(position).getBitmap());
		viewHolder.image.setImageUrl(data.get(position).getHeadStr(),
				imageLoader);
		return convertView;
	}

	public static class ViewHolder {
		public TextView title_ST_id;
		public TextView content_ST_id;
		public NetworkImageView image;
	}
}
