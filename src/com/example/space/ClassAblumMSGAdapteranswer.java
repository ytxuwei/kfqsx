package com.example.space;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.example.jhf.R;
import com.example.monthstar.OneDatyPerson;
import com.imooc.baseadapter.utils.CommonAdapter;
import com.imooc.baseadapter.utils.ViewHolderDemo;

public class ClassAblumMSGAdapteranswer extends CommonAdapter<OneDatyPerson> {

	public ClassAblumMSGAdapteranswer(Context context, List<OneDatyPerson> datas) {
		super(context, datas);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderDemo viewHolder = ViewHolderDemo.get(mContext, convertView,
				parent, R.layout.classablummasgadapteritemanswer, position);
		OneDatyPerson oneDatyPerson = mDatas.get(position);
		ImageView iView = viewHolder.getView(R.id.classblum_ivid);
		iView.setImageBitmap(oneDatyPerson.getImage_View());
		if (selectPic == position) {
			iView.setScaleType(ScaleType.CENTER_CROP);
		} else {
			iView.setScaleType(ScaleType.CENTER_INSIDE);
		}
		return viewHolder.getConvertView();
	}

}
