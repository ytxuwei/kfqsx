package com.example.open;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.global.Constant;
import com.example.jhf.MyContentDetailActivity;
import com.example.jhf.R;
import com.example.monthstar.OneDatyPerson;
import com.example.notice.Notices;
import com.example.view.RefreshLayout;
import com.example.view.RefreshLayout.OnLoadListener;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.imooc.baseadapter.utils.Tools;

/**
 * 节假日界面
 * 
 * @author Administrator
 * 
 */
public class OpenjjrActivity extends Activity implements OnClickListener{
	private ImageView shooll_GK;
//	private ListView lvshool;
	private OneDatyPerson shoolk;
	private TextView title;
	private Handler handler = new Handler();
	private ImageView ivBack;

	private ArrayList<Notices> datas;
	private String result;
	private String note;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private String contentName;// 每条item的标题
	private ObjectMapper om = new ObjectMapper();
	private String code;
	private int contentId;
	private String uid;
	private MyAdapter adapter;
	private PullToRefreshListView mPullRefreshListView;
	private int i=2;	//加载更多第几页
	private int pageCount;	//请求总页数
	private String mDate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.openjjr);
		SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
		uid = sp.getString("uid", "1");
		datas = new ArrayList<Notices>();
		initView();
		
		getJsonText(1);
		
		adapter = new MyAdapter();
		// 将添加的每条item放到Lstview中
		mPullRefreshListView.setAdapter(adapter);
		
		shooll_GK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				OpenjjrActivity.this.finish();
			}
		});
		mPullRefreshListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
				// TODO Auto-generated method stub
				new Thread() {
					public void run() {
						// 设置参数
						List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
						params.add(new BasicNameValuePair("db", Constant.db));// 数据库
						params.add(new BasicNameValuePair("uid", uid));
						params.add(new BasicNameValuePair("holiday_id", datas.get(arg2-1).id + ""));
						String homeworkDetail = Tools.sendGet(Constant.holidayMsg, params);
						processDetailData(homeworkDetail);

						Intent intent = new Intent(OpenjjrActivity.this, MyContentDetailActivity.class);
						intent.putExtra("title", datas.get(arg2-1).name);
						intent.putExtra("note", note);
						intent.putExtra("date", mDate);
						startActivity(intent);
					}
				}.start();
			}
		});

	}

	private void initView() {
		// TODO Auto-generated method stub
		title = (TextView) findViewById(R.id.tv_everyyear);
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);
		mPullRefreshListView.setMode(Mode.BOTH);
		mPullRefreshListView.setOnRefreshListener(new com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullDownToRefresh");
				
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						datas.clear();
						getJsonText(1);
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullUpToRefresh");
				// 这里写上拉加载更多的任务
				
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						getJsonText(i);
						i++;
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);
				
			}

		});
	}


	private void processDetailData(String json) {
		try {
			JSONObject jsonObj = new JSONObject(json);
			int cccode = jsonObj.getInt("code");
			JSONObject array = jsonObj.getJSONObject("data");
			note = (String) array.get("note");
			mDate = array.getString("date");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(OpenjjrActivity.this, MyContentDetailActivity.class);
		intent.putExtra("title", ((TextView) arg0).getText());
		startActivity(intent);
	}

	private void getJsonText(final int i) {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						jsonObject = new JSONObject(result);
						pageCount = jsonObject.getInt("page_count");
						jsonArray = jsonObject.getJSONArray("data");
						for (int i = 0; i < jsonArray.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							contentName = (String) jsonObject2.get("name");
							contentId = (Integer) jsonObject2.get("id");
							datas.add(new Notices(contentName, contentId));
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else if (msg.what == 6) {
					Toast.makeText(OpenjjrActivity.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(OpenjjrActivity.this, "忘了联网了吧", 0).show();
				}
				 adapter.notifyDataSetChanged();
			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("page", i+"")); // 密码
				params.add(new BasicNameValuePair("page_count", pageCount+"")); // 密码
				result = Tools.sendGet(Constant.holiday, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}


	public class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return datas.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return datas.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		private SparseArray<View> vmap = new SparseArray<View>();
		private ImageView readImageView;

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			final ViewHolder viewHolder;
			if (convertView == null) {
				convertView = LayoutInflater.from(OpenjjrActivity.this).inflate(R.layout.weenkend_item, null);
				viewHolder = new ViewHolder();
				viewHolder.TVshoolli = (TextView) convertView.findViewById(R.id.TV_tiemoneday_id);
				convertView.setTag(viewHolder);
				vmap.put(position, convertView);
			} else {
				convertView = vmap.get(position);
				viewHolder = (ViewHolder) convertView.getTag();
			}
			viewHolder.TVshoolli.setText(datas.get(position).name);
			return convertView;
		}

		public final class ViewHolder {
			public TextView TVshoolli;
		}

	}
}
