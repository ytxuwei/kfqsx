package com.example.studentwork;

import java.util.List;

import com.example.index.ShoolLITools;
import com.example.jhf.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class StudentWorksAdapter extends BaseAdapter {
	private Context context;
	private List<ShoolLITools> data;

	public StudentWorksAdapter(Context context, List<ShoolLITools> data) {
		this.context = context;
		this.data = data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.studentwroksitem, null);
			viewHolder = new ViewHolder();
			viewHolder.VHTextView = (TextView) convertView
					.findViewById(R.id.anjie_student);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.VHTextView.setText(data.get(position).getStr_shoolli()
				.toString());
		return convertView;
	}

	public final class ViewHolder {
		private TextView VHTextView;
	}
}
 