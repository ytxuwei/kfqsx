package com.example.jhf;

import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.global.Constant;
import com.example.index.RefreshableView;
import com.example.monthstar.OneDatyPerson;
import com.example.space.ClassHomeWork;
import com.example.space.SchoolWorkAdapter;
import com.example.space.StudentWorkParticular;
import com.imooc.baseadapter.utils.Tools;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

/**
 * 注册 验证手机号
 * 
 * @author Administrator
 * 
 */
public class yanzheng1 extends Activity implements OnClickListener {
	private Button complete;
	private EditText phoneNum, verCode;// 登录用户名和密码
	private Button getVerCode;
	private String phoneNumStr = "";// 手机号
	private String verCodeStr = "";// 验证码
	private String verCodeStrback = "";
	private TextView verResult;// 消息显示
	private ImageView ivBack;
	private Timer timer; // 计时器
	private TimerTask timerTask;
	private ObjectMapper om = new ObjectMapper();
	private String code;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.yanzhengone);

		phoneNum = (EditText) findViewById(R.id.ver_phone);
		verCode = (EditText) findViewById(R.id.verCode);
		getVerCode = (Button) findViewById(R.id.getVerCode);
		complete = (Button) findViewById(R.id.ver_next);
		verResult = (TextView) findViewById(R.id.ver_result);
		ivBack = (ImageView) findViewById(R.id.ver_mesBack);
		getVerCode.setOnClickListener(this);
		Net();

		ivBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				yanzheng1.this.finish();
			}
		});

	}

	public void onClick(View view) {
		// TODO Auto-generated method stub
		if (view.getId() == R.id.getVerCode) {
			getJsonText();
			// 判断手机号
			if (isPhoneRight()) {
				phoneNum.setEnabled(false);// 手机号验证成功后不可再编辑
				complete.setClickable(true);
				sendRequest(phoneNumStr);// 判断验证码
				verResult.setVisibility(View.VISIBLE);

			}
		}
	}

	/**
	 * 验证码
	 */
	private void Net() {
		complete.setOnClickListener(new OnClickListener() {

			@SuppressLint("DefaultLocale")
			@Override
			public void onClick(View arg0) {
				verCodeStr = verCode.getEditableText().toString().trim()
						.toLowerCase();// 忽略大小写和空格
				if ("".equalsIgnoreCase(verCodeStr)) {// 是否输入验证码
					verCode.setError("请先输入验证码！");
				} else if (verCodeStrback.equals(verCodeStrback)) {// 验证码的判断
					Intent intent = new Intent(yanzheng1.this, yanzheng2.class);
					intent.putExtra("ihone_Nab", phoneNumStr);
					startActivity(intent);
					finish();
				} else {
					verCode.setError("验证码输入错误！");
				}
			}
		});
	}

	/**
	 * 判断手机号
	 * 
	 * @return
	 */
	private boolean isPhoneRight() {
		phoneNumStr = phoneNum.getEditableText().toString().trim();
		Pattern p = Pattern
				.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
		Matcher m = p.matcher(phoneNumStr);
		if ("".equalsIgnoreCase(phoneNumStr)) {
			phoneNum.setError("请先输入您的手机号！");
			return false;
		} else if (!m.matches()) {
			phoneNum.setError("请确认您输入了正确的手机号！");
			return false;
		} else {
			return true;
		}

		// TODO Auto-generated method stub

	}

	/**
	 * 发送获取验证码的请求 phoneNumStr:手机号
	 */
	private void sendRequest(String phoneNumStr) {
		// TODO Auto-generated method stub
		getVerCode.setClickable(false);

		timer = new Timer();// 创建定时器
		/**
		 * 获取验证码是等待60秒后按钮才可以点击
		 */
		timerTask = new TimerTask() {
			// 倒数60秒
			int i = 60;

			@Override
			public void run() {
				Message msg = new Message();
				msg.what = i--;
				handler.sendMessage(msg);
			}
		};
		timer.schedule(timerTask, 0, 1000);
	}

	// 使用handler更新UI
	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			if (msg.what >= 0) {
				getVerCode.setText("(" + msg.what + "秒)");
			} else {
				getVerCode.setClickable(true);
				getVerCode.setText("获取验证码");

				timer.cancel();
				timerTask.cancel();
			}
		}
	};

	// /**
	// * 获取的JSON数据
	// */
	// @SuppressLint("HandlerLeak")
	// private void getJsonText() {
	// final Handler handler = new Handler() {
	//
	// public void handleMessage(Message msg) {
	// if (msg.what == 7) {
	// String result = msg.obj.toString();
	// try {
	// JSONObject jsonObject = new JSONObject(result);
	// JSONObject jsonObject3 = jsonObject
	// .getJSONObject("data");
	// verCodeStrback = (String) jsonObject3
	// .get("verificationCode");
	// } catch (JSONException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// } else if (msg.what == 6) {
	// Toast.makeText(yanzheng1.this, "帐号或密码错误", 0).show();
	// } else if (msg.what == 10) {
	// Toast.makeText(yanzheng1.this, "忘了联网了吧", 0).show();
	// }
	//
	// }
	//
	// };
	// new Thread() {
	// public void run() {
	// List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
	// params.add(new BasicNameValuePair("db", Constant.db));// 数据库
	// params.add(new BasicNameValuePair("phone", phoneNumStr));//手机号
	// String result = Tools
	// .sendGet(Constant.verificationCode, params);
	// Message message = new Message();
	// try {
	// if (result != null) {
	// JsonNode jsonNode = om.readTree(result);
	// code = jsonNode.get("code").asText();
	// // history=jsonNode.get("note").asText();
	// if (code.equals("200")) {
	// message.what = 7;
	// message.obj = result;
	// } else {
	// message.what = 6;// 登陆失败
	// }
	// } else {
	// message.what = 9;// 无网络
	// }
	// handler.sendMessage(message);
	// } catch (Exception e) {
	// // TODO: handle exception
	// e.printStackTrace();
	// message.what = 10;
	// handler.sendMessage(message);
	// }
	// };
	// }.start();
	// }
	/**
	 * 获取的JSON数据
	 */
	@SuppressLint("HandlerLeak")
	private void getJsonText() {
		final Handler handler = new Handler() {

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						JSONObject jsonObject = new JSONObject(result);
						JSONObject jsonObject3 = jsonObject
								.getJSONObject("data");
						verCodeStrback = (String) jsonObject3
								.get("verificationCode");

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (msg.what == 11) {
					String result = msg.obj.toString();
					try {
						JSONObject jsonObject = new JSONObject(result);
						JSONObject jsonObject3 = jsonObject
								.getJSONObject("data");
						verCodeStrback = (String) jsonObject3.get("error");

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Toast.makeText(yanzheng1.this, verCodeStrback, 0).show();
				}  else if (msg.what == 9) {
					Toast.makeText(yanzheng1.this, "网络中断", 0).show();
				}else if (msg.what == 10) {
					Toast.makeText(yanzheng1.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("phone", phoneNumStr));// 手机号
				params.add(new BasicNameValuePair("type", "register"));
				String result = Tools.sendGet(Constant.verificationCodechange,
						params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else if (code.equals("400")) {
							message.what = 11;
							message.obj = result;
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}
}
