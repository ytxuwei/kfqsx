package com.example.bean;

import android.graphics.Bitmap;

/**
 * 家长列表的Bean对象
 * @author Administrator
 *
 */
public class ParentListBean {
	public String imgSrc;	//图片
	public int lineId;
	public String parentName;	//家长名字
	public int parentId;
	public String studentName;	//学生名字
	public ParentListBean(String imgSrc, int lineId, String parentName,
			int parentId, String studentName) {
		super();
		this.imgSrc = imgSrc;
		this.lineId = lineId;
		this.parentName = parentName;
		this.parentId = parentId;
		this.studentName = studentName;
	}
	public ParentListBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
