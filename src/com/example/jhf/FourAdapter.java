package com.example.jhf;

import java.util.List;
import java.util.Map;

import android.R.integer;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * 个人中心baseAdapter
 * @author Administrator
 *
 */
public class FourAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private List<Map<String, Object>> data;
	private int layoutID;

	public FourAdapter(Context context, List<Map<String, Object>> data,
			int layoutID) {
		this.context = context;
		this.layoutInflater = LayoutInflater.from(context);
		this.layoutID = layoutID;
		this.data=data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		final ViewHolder viewHolder;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.custom_touxiangitem, null);
			viewHolder = new ViewHolder();
			viewHolder.CTIVcpI_id = (ImageView) convertView
					.findViewById(R.id.CTIVcpI_id);
			viewHolder.CTIVcpD_id = (ImageView) convertView
					.findViewById(R.id.CTIVcpD_id);
			viewHolder.CTTVcp_id = (TextView) convertView.findViewById(R.id.CTTVcp_id);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		Map<String, Object> map = data.get(position);
		int img= Integer.parseInt(map.get("图标").toString()); 
		switch (img) {
		case 1:
			viewHolder.CTIVcpI_id.setImageResource(R.drawable.gerenziliao);
			break;
		case 2:
			viewHolder.CTIVcpI_id.setImageResource(R.drawable.xiangcetubiao);
			break;
		case 3:
			viewHolder.CTIVcpI_id.setImageResource(R.drawable.shezhitubiao);
			break;
				
	
		}
		
		switch (img) {
		case 1:
			viewHolder.CTIVcpD_id.setImageResource(R.drawable.class_xyehui);
			break;
		case 2:
			viewHolder.CTIVcpD_id.setImageResource(R.drawable.class_xyehui);
			break;
		case 3:
			viewHolder.CTIVcpD_id.setImageResource(R.drawable.class_xyehui);
			break;
		
		}
		switch (img) {
		case 1:
			viewHolder.CTTVcp_id.setText("个人资料");
			break;
		case 2:
			viewHolder.CTTVcp_id.setText("消息");
			break;
		case 3:
			viewHolder.CTTVcp_id.setText("设置");
			break;
		}
//		int j=from.length;
//		for (int i = 0; i < j; i++) {
//			if (convertView.findViewById(to[i])instanceof TextView) {
//				TextView tv=(TextView) convertView.findViewById(to[i]);
//				tv.setText(data.get(position).get(from[i]).toString());
//			}
////			if (convertView.findViewById(to[i])instanceof ImageView) {
////				ImageView iv=(ImageView) convertView.findViewById(to[i]);
////				iv.setImageBitmap((Bitmap) data.get(position).get(from[i]));
////			}
//		}
		return convertView;
	}

	public static class ViewHolder {
		public static ImageView CTIVcpI_id;
		public static TextView CTTVcp_id;
		public static ImageView CTIVcpD_id;
	}
}
