package com.example.centre;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.global.Constant;
import com.example.jhf.R;
import com.imooc.baseadapter.utils.Tools;

public class AccreditationActvity extends Activity {
	private ImageView ivBack;
	private Button btnSend;
	private TextView tvGrade;

	private List<String> groups = new ArrayList<String>();
	private HashMap<String, Integer> gradeMaps = new HashMap<String, Integer>();
	private HashMap<String, Integer> classMaps = new HashMap<String, Integer>();
	private HashMap<String, Integer> roleMaps = new HashMap<String, Integer>();
	private List<String> classGroups = new ArrayList<String>();
	private List<String> roleGroups = new ArrayList<String>();
	private String uid;
	private TextView tvClass;
	private TextView tvRole;
	private Button btnCode;
	private EditText etPhone;
	private EditText etStudentName;
	private ObjectMapper om = new ObjectMapper();
	private EditText etCode;
	private String msgCode;
	private TextView tvResult;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.accreditation_actvity);

		SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
		uid = sp.getString("uid", "0");

		getGradeFromNet();
		getRoleFromNet();
		ivBack = (ImageView) findViewById(R.id.shooll_GK);
		ivBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		etParentName = (EditText) findViewById(R.id.et_accreditation_parent_name);
		etStudentName = (EditText) findViewById(R.id.et_student_name);
		btnSend = (Button) findViewById(R.id.btn_accreditation_send);
		btnSend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// TODO Auto-generated method stub
				String msgInput = etCode.getText().toString();
				if(msgInput.equals(msgCode)){
					sendAccreditationToNet();
				}else if(msgInput.equals("")){
					Toast.makeText(getApplicationContext(), "请输入验证码", 0).show();
				}else{
					Toast.makeText(getApplicationContext(), "验证码错误", 0).show();
				}
				
			}
		});
		tvGrade = (TextView) findViewById(R.id.tv_accreditation_grade);
		tvGrade.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialog();
			}
		});
		tvRole = (TextView) findViewById(R.id.tv_accreditation_role);
		tvRole.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showDialog3();
			}
		});
		tvClass = (TextView) findViewById(R.id.tv_accreditation_class);
		tvClass.setEnabled(false);
		tvClass.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showDialog2();
			}
		});
		tvResult = (TextView) findViewById(R.id.tv_accreditation_result);
		btnCode = (Button) findViewById(R.id.btn_accreditation_code);
		btnCode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				//验证是否是手机号
				if (isPhoneRight()) {
					etPhone.setEnabled(false);// 手机号验证成功后不可再编辑
					etPhone.setClickable(true);

					sendRequest(etPhone.getText().toString());
					
				}
				sendCodeToNet(); 
			}
		});
		etPhone = (EditText) findViewById(R.id.et_accreditation_phone);
		etCode = (EditText) findViewById(R.id.et_accreditation_code);
	}

	protected void sendAccreditationToNet() {
		// TODO Auto-generated method stub
		final Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				String result = msg.obj.toString();
				try {
					if (msg.what == 7) {
						Toast.makeText(getApplicationContext(), "认证成功！", 0).show();
						SharedPreferences sp = getSharedPreferences("confirm", MODE_PRIVATE);
						Editor edit = sp.edit();
						edit.putString("isConfirm", "true");
						edit.commit();
						finish();
						
					} else if (msg.what == 6) {
						JSONObject object = new JSONObject(result);
						JSONObject obj = object.getJSONObject("data");
						String msgCode2 = obj.getString("data");
						System.out.println("错误码："+msgCode2);
						Toast.makeText(AccreditationActvity.this, msgCode2, 0).show();
					} else if (msg.what == 10) {
						Toast.makeText(AccreditationActvity.this, "忘了联网了吧", 0)
								.show();
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // uid
				params.add(new BasicNameValuePair("student_name", etStudentName
						.getText().toString())); // uid
				params.add(new BasicNameValuePair("parent_name", etParentName
						.getText().toString())); // uid
				params.add(new BasicNameValuePair("grade_id", gradeMaps
						.get(tvGrade.getText().toString()) + "")); // uid
				params.add(new BasicNameValuePair("class_id", classMaps
						.get(tvClass.getText().toString()) + "")); // uid
				params.add(new BasicNameValuePair("role_id", roleMaps
						.get(tvRole.getText().toString()) + "")); // uid
				if (!TextUtils.isEmpty(etPhone.getText().toString())) {
					params.add(new BasicNameValuePair("phone", etPhone
							.getText().toString())); // phone
				}
				String result = Tools.sendPost(Constant.sendAccreditation,
						params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						String code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else if(code.equals("400")){
							message.what = 6;// 登陆失败
							message.obj = result;
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

	protected void sendCodeToNet() {
		final Handler handler = new Handler() {

			public void handleMessage(Message msg) {
				String result = msg.obj.toString();
				JSONObject object;
				try {
					if (msg.what == 7) {
						tvResult.setVisibility(View.VISIBLE);
						object = new JSONObject(result);
						JSONObject obj = object.getJSONObject("data");
						msgCode = obj.getString("verificationCode");	//验证码,判断验证码和输入的是否相同
					} else if (msg.what == 6) {
						object = new JSONObject(result);
						JSONObject obj = object.getJSONObject("data");
						//msgCode = obj.getString("error");
						Toast.makeText(getApplicationContext(), obj.getString("error"), 0).show();
					} else if (msg.what == 10) {
						Toast.makeText(AccreditationActvity.this, "忘了联网了吧", 0)
								.show();
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				if(!TextUtils.isEmpty(etPhone.getText().toString())){
					params.add(new BasicNameValuePair("phone", etPhone.getText().toString())); // phone
				}
				params.add(new BasicNameValuePair("type", "confirmParent"));
				String result = Tools.sendGet(Constant.sendCode, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						String code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else if(code.equals("400")){
							message.what = 6;// 登陆失败
							message.obj=result;
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

	protected void showDialog() {
		// TODO Auto-generated method stub
		final String[] items = new String[groups.size()];
		for (int i = 0; i < groups.size(); i++) {
			items[i] = groups.get(i);
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("选择年级");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				tvGrade.setText(items[item]);
				setClassFromNet();
				tvClass.setEnabled(true);
			}
		});

		AlertDialog alert = builder.create();
		alert.show();
	}

	protected void showDialog2() {
		// TODO Auto-generated method stub
		final String[] items = new String[classGroups.size()];
		for (int i = 0; i < classGroups.size(); i++) {
			items[i] = classGroups.get(i);
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("选择班级");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				tvClass.setText(items[item]);
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	protected void showDialog3() {
		// TODO Auto-generated method stub
		final String[] items = new String[roleGroups.size()];
		for (int i = 0; i < roleGroups.size(); i++) {
			items[i] = roleGroups.get(i);
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("选择角色");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				tvRole.setText(items[item]);
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void setClassFromNet() {
		final Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				String result = msg.obj.toString();
				JSONObject object;
				try {
					object = new JSONObject(result);
					JSONArray array = object.getJSONArray("data");
					for (int i = 0; i < array.length(); i++) {
						JSONObject obj = (JSONObject) array.opt(i);
						String grade = obj.getString("class_name");
						int gradeId = obj.getInt("class_id");
						classMaps.put(grade, gradeId);
						// gradeMaps.put(grade, gradeId);
						classGroups.add(grade);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // uid
				params.add(new BasicNameValuePair("grade_id", gradeMaps
						.get(tvGrade.getText()) + "")); // uid
				String result = Tools.sendGet(Constant.getClass, params);
				Message message = new Message();
				message.what = 7;
				message.obj = result;
				handler.sendMessage(message);
			};
		}.start();
	}

	private void getRoleFromNet() {

		final Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				String result = msg.obj.toString();
				JSONObject object;
				try {
					object = new JSONObject(result);
					JSONArray array = object.getJSONArray("data");
					for (int i = 0; i < array.length(); i++) {
						JSONObject obj = (JSONObject) array.opt(i);
						String grade = obj.getString("role_name");
						int gradeId = obj.getInt("role_id");
						roleGroups.add(grade);
						roleMaps.put(grade, gradeId);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // uid
				String result = Tools.sendGet(Constant.getRole, params);
				Message message = new Message();
				message.what = 7;
				message.obj = result;
				handler.sendMessage(message);
			};
		}.start();
	}

	private void getGradeFromNet() {

		final Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				String result = msg.obj.toString();
				JSONObject object;
				try {
					object = new JSONObject(result);
					JSONArray array = object.getJSONArray("data");
					for (int i = 0; i < array.length(); i++) {
						JSONObject obj = (JSONObject) array.opt(i);
						String grade = obj.getString("grade_name");
						int gradeId = obj.getInt("grade_id");
						gradeMaps.put(grade, gradeId);
						groups.add(grade);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // uid
				String result = Tools.sendGet(Constant.getGrade, params);
				Message message = new Message();
				message.what = 7;
				message.obj = result;
				handler.sendMessage(message);
			};
		}.start();
	}
	
	/**
	 * 判断手机号
	 * 
	 * @return
	 */
	private boolean isPhoneRight() {
		String phoneNumStr = etPhone.getEditableText().toString().trim();
		Pattern p = Pattern
				.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
		Matcher m = p.matcher(phoneNumStr);
		if ("".equalsIgnoreCase(phoneNumStr)) {
			etPhone.setError("请先输入您的手机号！");
			return false;
		} else if (!m.matches()) {
			etPhone.setError("请确认您输入了正确的手机号！");
			return false;
		} else {
			return true;
		}

		// TODO Auto-generated method stub

	}

	/**
	 * 发送获取验证码的请求 phoneNumStr:手机号
	 */
	private Timer timer; // 计时器
	private TimerTask timerTask;
	private void sendRequest(String phoneNumStr) {
		// TODO Auto-generated method stub
		btnCode.setClickable(false);

		timer = new Timer();// 创建定时器
		/**
		 * 获取验证码是等待60秒后按钮才可以点击
		 */
		timerTask = new TimerTask() {
			// 倒数60秒
			int i = 60;

			@Override
			public void run() {
				Message msg = new Message();
				msg.what = i--;
				handler.sendMessage(msg);
			}
		};
		timer.schedule(timerTask, 0, 1000);
	}

	// 使用handler更新UI
	Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			if (msg.what >= 0) {
				btnCode.setText("(" + msg.what + "秒)");
			} else {
				btnCode.setClickable(true);
				btnCode.setText("获取验证码");

				timer.cancel();
				timerTask.cancel();
			}
		}
	};
	private EditText etParentName;
	
}
