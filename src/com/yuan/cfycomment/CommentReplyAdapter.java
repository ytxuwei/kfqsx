package com.yuan.cfycomment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.example.jhf.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
/**
 * 娴滃瞼楠囩拠鍕啈鐢啫鐪�
 * @author Administrator
 *
 */
public class CommentReplyAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private List<HashMap<String, String>> list; // 娴滃瞼楠囩拠鍕啈閺佺増宓�
	private ViewHolder viewHolder;

	public CommentReplyAdapter(Context context,
			List<HashMap<String, String>> list) {
		inflater = LayoutInflater.from(context);
		// this.list = new ArrayList<HashMap<String, Object>>();
		// this.list.addAll(list);
		this.list = list;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	public void clearList() {
		this.list.clear();
	}

	public void updateList(List<HashMap<String, String>> list_comment) {
		this.list.addAll(list_comment);
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_comment_reply, null);
			viewHolder = new ViewHolder();
			viewHolder.tv_comment_reply_text = (TextView) convertView
					.findViewById(R.id.tv_comment_reply_text);
			viewHolder.tv_comment_reply_writer = (TextView) convertView
					.findViewById(R.id.tv_comment_reply_writer);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
			
		}
		viewHolder.tv_comment_reply_text.setText(list.get(position)
				.get("content").toString());
		// viewHolder.tv_comment_reply_writer.setText(list.get(position)
		// .get("user").toString());

		return convertView;
	}

	public class ViewHolder {
		private TextView tv_comment_reply_writer; // 鐠囧嫯顔戦懓鍛█缁夛拷
		private TextView tv_comment_reply_text; // 鐠囧嫯顔� 閸愬懎顔�
	}
}
