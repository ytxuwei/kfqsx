package com.example.monthstar;

/**
 * 创建日期: 2015/09/12 创建者： 刘永华 功能说明：自定义每日百星adapter 修改履历： VER 修改日 修改者 修改内容／理由
 * ──────────────────────────────────────────────────────────────
 */
import java.util.List;
import java.util.Map;
import com.imooc.baseadapter.utils.*;
import com.example.jhf.CPAdapter.ViewHolder;
import com.example.jhf.R;

import android.R.integer;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 每日百星baseAdapter
 * 
 * @author Administrator
 * 
 */
public class OneDayAdper extends CommonAdapter<OneDatyPerson> {
	public OneDayAdper(Context context, List<OneDatyPerson> data) {
		super(context, data);
	}

	// 用于保持选中状态
	private SparseArray<View> vmap = new SparseArray<View>();

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderDemo holder = ViewHolderDemo.get(mContext, convertView,
				parent, R.layout.onedayitemchange, position);
		OneDatyPerson onedatyperson = mDatas.get(position);
		// 方式一
		TextView tv = holder.getView(R.id.TV_tiemoneday_id);
		tv.setText(onedatyperson.getStrriqi());
		return holder.getConvertView();
	}

	// 添加数据
	public void addItem_OneDatyPerson(String value) {
		OneDatyPerson one = new OneDatyPerson();
		one.setStrriqi(value);
		mDatas.add(one);
	}
}
