package com.example.bean;

import java.util.ArrayList;

public class BlogJsonBean {
	public String space_user_portrait;	//用户头像
	public String page_count;	
	public ArrayList<Data> data;
	
	public class Data{
		public String parent_name;
		public ArrayList<DataImage> data_image;
		
		public class DataImage{
			public int image_id;
			public String image;
		}
		public String space_name;	//发博客用户
		public String student_name;
		public String date;
		public String note;
		public String role;	//角色
		public String portrait;
		public String id;
		public String comment_count;
		public ArrayList<DataComment> data_comment;
		
		public class DataComment{
			public String comment_note;
			public String comment_id;
			public String comment_user;
		}
	}
	public String space_user_name;	//发博客用户
	public String space_user_id;
	
	
	
}
