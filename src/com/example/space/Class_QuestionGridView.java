package com.example.space;

import java.util.ArrayList;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.jhf.R;
import com.example.monthstar.OneDatyPerson;
import com.facebook.drawee.view.SimpleDraweeView;

public class Class_QuestionGridView extends BaseAdapter {
	private Context context;
	private LayoutInflater layoutInflater;
	private ArrayList<OneDatyPerson> data;
	private int layoutID;

	public Class_QuestionGridView(Context context, ArrayList<OneDatyPerson> data) {
		this.layoutInflater = LayoutInflater.from(context);
		this.data = data;
		this.context = context;
	};

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// ImageLoader imageLoader = ImageLoader.getInstance();
		// imageLoader.init(ImageLoaderConfiguration.createDefault(context));//
		// 对ImageLoader进行实例化
		//ImageLoaderConfiguration.createDefault(context);
		//initImageLoader(context);
		final ViewHolder viewHolder;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.classablummasgadapteritem, null);
			viewHolder = new ViewHolder();
			viewHolder.mImage = (SimpleDraweeView) convertView.findViewById(R.id.classblum_ivid);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		//ImageLoader.getInstance().displayImage(// 将试图放入控件中
		//		data.get(position).getStudentytiem(), viewHolder.mImage);
		viewHolder.mImage.setImageURI(Uri.parse(data.get(position).getStudentytiem()));
		return convertView;
	}

	public class ViewHolder {
		public SimpleDraweeView mImage;
	}

	/**
	 * 对图片加入缓存，不需要重复加载
	 * 
	 * @param context
	 */
	/*public static void initImageLoader(Context context) {
		// 缓存文件的目录
		File cacheDir = StorageUtils.getOwnCacheDirectory(context, "imageloader/Cache");
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).memoryCacheExtraOptions(480, 800)
		// max width, max height，即保存的每个缓存文件的最大长宽
				.threadPoolSize(3)
				// 线程池内加载的数量
				.threadPriority(Thread.NORM_PRIORITY - 2).denyCacheImageMultipleSizesInMemory()
				// .diskCacheFileNameGenerator(new Md5FileNameGenerator())
				// //将保存的时候的URI名称用MD5 加密
				// .memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 *
				// 1024)) // You can pass your own memory cache
				// implementation/你可以通过自己的内存缓存实现
				.memoryCacheSize(2 * 1024 * 1024) // 内存缓存的最大值
				.diskCacheSize(50 * 1024 * 1024) // 50 Mb sd卡(本地)缓存的最大值
				// .tasksProcessingOrder(QueueProcessingType.LIFO)
				// 由原先的discCache -> diskCache
				.diskCache(new UnlimitedDiscCache(cacheDir))// 自定义缓存路径
				// .imageDownloader(new BaseImageDownloader(context, 5 * 1000,
				// 30 * 1000)) // connectTimeout (5 s), readTimeout (30 s)超时时间
				.writeDebugLogs() // Remove for release app
				.build();
		
		// 全局初始化此配置,对ImageLoader进行实例化
		ImageLoader.getInstance().init(config);
	}*/

}
