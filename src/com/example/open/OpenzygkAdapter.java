package com.example.open;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.index.ShoolLITools;
import com.example.jhf.R;
import com.example.notice.Notice;
import com.example.notice.Notices;

/**
 * 作业公开
 * 
 * @author Administrator
 * 
 */
public class OpenzygkAdapter extends BaseAdapter {
	private Context context;
	private List<Notices> data;

	public OpenzygkAdapter(Context context, ArrayList<Notices> noticeList) {
		this.context = context;
		this.data = noticeList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	// 用于保持选中状态
	private SparseArray<View> vmap = new SparseArray<View>();

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		final ViewHolder viewHolder;
		if (vmap.get(position) == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.homework, null);
			viewHolder = new ViewHolder();
			viewHolder.TVshoolli = (TextView) convertView
					.findViewById(R.id.TVshoolli_shoolsitem);
			convertView.setTag(viewHolder);
			vmap.put(position, convertView);
		} else {
			convertView = vmap.get(position);
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.TVshoolli.setText(data.get(position).name);
		return convertView;
	}

	public static class ViewHolder {
		public TextView TVshoolli;
	}

}
