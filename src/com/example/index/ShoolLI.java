package com.example.index;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.global.Constant;
import com.example.index.RefreshableView.PullToRefreshListener;
import com.example.jhf.ContentDetailActivity;
import com.example.jhf.R;
import com.example.view.ListViewIndicatorUtil;
import com.google.gson.JsonObject;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.imooc.baseadapter.utils.Tools;

/**
 * 创建日期: 2015/09/12 创建者： 刘永华 功能说明：用于介绍学校历史 修改履历： VER 修改日 修改者 修改内容／理由
 * ──────────────────────────────────────────────────────────────
 */
public class ShoolLI extends Activity {
	private ArrayList<ShoolLITools> data;
	private ImageView shooll_GK;
	private ShoolLIAdpter shoolDAdater;
	private int visibleLastIndex = 0; // 最后的可视项索引
	private int visibleItemCount; // 当前窗口可见项总数
	private View loadMoreView; // 定义视图
	private Handler handler = new Handler();
	private RefreshableView refreshableView;
	private ObjectMapper om = new ObjectMapper();
	private String history;// 每条item的标题
	private String code;
	private JSONArray jsonArray;
	private TextView loadMoreButton;
	private int i = 2;// 从大二页开始加载更多
	private PullToRefreshListView lvAnswer;// 加载更多
	private int p = 0;// 取回的页数

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.shoolfm);
		init();
		back();
		data = new ArrayList<ShoolLITools>();
		getJsonText(1);
		
		shoolDAdater = new ShoolLIAdpter(ShoolLI.this, data);
		lvAnswer.setAdapter(shoolDAdater);
	}

	private void back() {
		shooll_GK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ShoolLI.this.finish();
			}
		});
	}

	private void init() {
		View headerView = View.inflate(getApplicationContext(), R.layout.header_shoolfm, null);
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		lvAnswer = (PullToRefreshListView) findViewById(R.id.list);
		ListView View2 = lvAnswer.getRefreshableView();
		// View2.addHeaderView(headerView);
		ListViewIndicatorUtil.initIndicator(lvAnswer);// 下拉改变字体
		// 刷新
		lvAnswer.setOnRefreshListener(new com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullDownToRefresh");

				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						data.clear();
						getJsonText(1);
						i=2;
						lvAnswer.onRefreshComplete();
					}
				}, 3000);
			}

			// 加载更多
			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullUpToRefresh");
				// 这里写上拉加载更多的任务

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						getJsonText(i);
						i++;
						lvAnswer.onRefreshComplete();
					}
				}, 3000);

			}

		});
		
		lvAnswer.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
					try {
						Intent intent = new Intent(ShoolLI.this, ShoolLIActivity.class);
						JSONObject jsonObject3 = (JSONObject) jsonArray.opt(arg2 - 1);
						intent.putExtra("id", data.get(arg2-1).getId());
						startActivity(intent);
					} catch (Exception e) {
						// TODO: handle exception
					}

			}
		});
	}

	/**
	 * 获取学校校的JSON数据
	 */
	private void getJsonText(final int i) {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					JSONObject jsonObject;
					try {
						jsonObject = new JSONObject(result);
						jsonArray = jsonObject.getJSONArray("data");
						p = jsonObject.getInt("page_count");
						for (int i = 0; i < jsonArray.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							history = (String) jsonObject2.get("name");
							String id = jsonObject2.getString("id");
							// year = jsonObject2.getString("year");
							// note = jsonObject2.getString("note");
							data.add(new ShoolLITools(history, id));
						}
						shoolDAdater.notifyDataSetChanged();
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (msg.what == 6) {
					Toast.makeText(ShoolLI.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(ShoolLI.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("page", i + ""));
				params.add(new BasicNameValuePair("page_count", p + ""));
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				String result = Tools.sendGet(Constant.history, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}
}
