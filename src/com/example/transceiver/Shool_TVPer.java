package com.example.transceiver;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.webkit.WebView;

public class Shool_TVPer {
	private String titleStr;
	private String contentStr;
	private int imageId;
	private Bitmap bitmap;
	private WebView webview_school;
	private String headStr;
	public WebView getWebview_school() {
		return webview_school;
	}

	public String getHeadStr() {
		return headStr;
	}

	public void setHeadStr(String headStr) {
		this.headStr = headStr;
	}

	public void setWebview_school(WebView webview_school) {
		this.webview_school = webview_school;
	}

	public int getImageId() {
		return imageId;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public String getTitleStr() {
		return titleStr;
	}

	public void setTitleStr(String titleStr) {
		this.titleStr = titleStr;
	}

	public String getContentStr() {
		return contentStr;
	}

	public void setContentStr(String contentStr) {
		this.contentStr = contentStr;
	}
}
