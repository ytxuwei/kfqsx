package com.example.space;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bean.BlogToSendBean;
import com.example.global.Constant;
import com.example.jhf.R;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.imooc.baseadapter.utils.BitmapUtils;
import com.imooc.baseadapter.utils.Tools;
import com.king.photo.activity.BlogAlbumActivity;
import com.king.photo.activity.BlogGalleryActivity;
import com.king.photo.util.Bimp;
import com.king.photo.util.FileUtils;
import com.king.photo.util.ImageItem;
import com.king.photo.util.PublicWay;
import com.king.photo.util.Res;
import com.lidroid.xutils.bitmap.callback.BitmapLoadFrom;

/**
 * blog
 * 
 * @author Administrator
 * 
 */
public class Show extends Activity {
	private GridView noScrollgridview;
	private GridAdapter adapter;
	private View parentView;
	private PopupWindow pop = null;
	private LinearLayout ll_popup;
	public static Bitmap bimap;
	private ObjectMapper om = new ObjectMapper();

	// private ArrayList<ShowBlogBean> datas;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Res.init(this);
		SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
		uid = sp.getString("uid", "0");
		bimap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_addpic_unfocused);
		PublicWay.activityList.add(this);
		parentView = getLayoutInflater().inflate(R.layout.activity_selectimg, null);
		setContentView(parentView);

		Init();

	}

	public void Init() {

		pop = new PopupWindow(Show.this);

		View view = getLayoutInflater().inflate(R.layout.item_popupwindows, null);

		ll_popup = (LinearLayout) view.findViewById(R.id.ll_popup);
		tvSend = (TextView) findViewById(R.id.activity_selectimg_send);
		etBlog = (EditText) findViewById(R.id.et_send_blog);
		etBlog.setText("");

		/**
		 * 
		 * 得到博客内容、图片列表 传输数据？
		 */
		tvSend.setOnClickListener(new OnClickListener() {

			private String image;

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				tvSend.setFocusable(false);
				ArrayList<BlogToSendBean> data_image = new ArrayList<BlogToSendBean>();
				for (int i = 0; i < Bimp.tempSelectBitmap.size(); i++) {
					if (!TextUtils.isEmpty(Bimp.tempSelectBitmap.get(i).getImagePath())) {
						try {
							image = Bimp.bitmapToString(Bimp.tempSelectBitmap.get(i).getImagePath());
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else {
						image = BitmapUtils.convertBitmapToString(Bimp.tempSelectBitmap.get(i).getBitmap());
					}
					// 图片标题改成时间戳
					String store_fname = System.currentTimeMillis() + ".jpg";
					data_image.add(new BlogToSendBean(store_fname, image));
				}
				String strImg = "";
				try {
					strImg = om.writeValueAsString(data_image);
				} catch (JsonProcessingException e1) {
					// TODO Auto-generated catch block

					e1.printStackTrace();
				}
				if (TextUtils.isEmpty(etBlog.getText().toString())) {
					Toast.makeText(Show.this, "博客内容不能为空", 0).show();
				} else {
					sendJsontoNet(strImg);
				}
			}
		});
		ivBack = (ImageView) findViewById(R.id.iv_back_show);
		ivBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				finish();
			}
		});

		pop.setWidth(LayoutParams.MATCH_PARENT);
		pop.setHeight(LayoutParams.WRAP_CONTENT);
		pop.setBackgroundDrawable(new BitmapDrawable());
		pop.setFocusable(true);
		pop.setOutsideTouchable(true);
		pop.setContentView(view);

		RelativeLayout parent = (RelativeLayout) view.findViewById(R.id.parent);
		Button bt1 = (Button) view.findViewById(R.id.item_popupwindows_camera);
		Button bt2 = (Button) view.findViewById(R.id.item_popupwindows_Photo);
		Button bt3 = (Button) view.findViewById(R.id.item_popupwindows_cancel);
		parent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				pop.dismiss();
				ll_popup.clearAnimation();
			}
		});
		bt1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				photo();
				pop.dismiss();
				ll_popup.clearAnimation();
			}
		});
		bt2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(Show.this, BlogAlbumActivity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.activity_translate_in, R.anim.activity_translate_out);
				pop.dismiss();
				ll_popup.clearAnimation();
			}
		});
		bt3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				pop.dismiss();
				ll_popup.clearAnimation();
			}
		});

		noScrollgridview = (GridView) findViewById(R.id.noScrollgridview);
		noScrollgridview.setSelector(new ColorDrawable(Color.TRANSPARENT));
		adapter = new GridAdapter(this);
		adapter.update();
		noScrollgridview.setAdapter(adapter);
		noScrollgridview.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if (arg2 == Bimp.tempSelectBitmap.size()) {
					((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(Show.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
					ll_popup.startAnimation(AnimationUtils.loadAnimation(Show.this, R.anim.activity_translate_in));
					pop.showAtLocation(parentView, Gravity.BOTTOM, 0, 0);
				} else {
					Intent intent = new Intent(Show.this, BlogGalleryActivity.class);
					intent.putExtra("position", "1");
					intent.putExtra("ID", arg2);
					startActivity(intent);
				}
			}
		});

	}

	/**
	 * 
	 * 发送到后台
	 */
	protected void sendJsontoNet(final String strImg) {
		progressDialog = ProgressDialog.show(Show.this, "请稍等...", "正在发表...", true, true);
		// 点击不会消失
		progressDialog.setCanceledOnTouchOutside(false);
		final Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub

				super.handleMessage(msg);
				// Bimp.tempSelectBitmap.clear();
				if (msg.what == 7) {
					etBlog.setText("");
					SharedPreferences sp = getSharedPreferences("edit", MODE_PRIVATE);
					Editor editor = sp.edit();
					editor.clear();
					editor.commit();

					

					Toast.makeText(Show.this, "发表成功", 0).show();
					// finish();
					Intent intent = new Intent();
					intent.setAction("action.refreshFriend");
					sendBroadcast(intent);
					Show.this.finish();
					// finish();
				} else {
					Toast.makeText(Show.this, "发表失败", 0).show();
				}
				progressDialog.dismiss();
			}
		};

		new Thread() {
			public void run() {

				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库

				params.add(new BasicNameValuePair("uid", uid)); // uid

				params.add(new BasicNameValuePair("note", etBlog.getText().toString())); // 博客内容内容

				params.add(new BasicNameValuePair("data_image", strImg)); // 图片列表

				String result = Tools.sendPost(Constant.postBlog, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						String code = jsonNode.get("code").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
							
							for(int i=0;i<Bimp.tempSelectBitmap.size();i++){
								Bimp.tempSelectBitmap.get(i).getBitmap().recycle();
								Bitmap bitmap = Bimp.tempSelectBitmap.get(i).getBitmap();
								bitmap = null;
							}
							Bimp.tempSelectBitmap.clear();
							Bimp.max = 0;
							System.gc();

						} else {
							message.what = 6;// 登陆失败

						}
					} else {
						message.what = 9;// 无网络

					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception

					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}

			};
		}.start();
	}

	@SuppressLint("HandlerLeak")
	public class GridAdapter extends BaseAdapter {
		private LayoutInflater inflater;
		private int selectedPosition = -1;
		private boolean shape;

		public boolean isShape() {
			return shape;
		}

		public void setShape(boolean shape) {
			this.shape = shape;
		}

		public GridAdapter(Context context) {
			inflater = LayoutInflater.from(context);
		}

		public void update() {
			loading();
		}

		public int getCount() {
			if (Bimp.tempSelectBitmap.size() == 9) {
				return 9;
			}
			return (Bimp.tempSelectBitmap.size() + 1);
		}

		public Object getItem(int arg0) {
			return null;
		}

		public long getItemId(int arg0) {
			return 0;
		}

		public void setSelectedPosition(int position) {
			selectedPosition = position;
		}

		public int getSelectedPosition() {
			return selectedPosition;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.item_published_grida, parent, false);
				holder = new ViewHolder();
				holder.image = (ImageView) convertView.findViewById(R.id.item_grida_image);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			if (position == Bimp.tempSelectBitmap.size()) {
				holder.image.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.icon_addpic_unfocused));
				if (position == 9) {
					holder.image.setVisibility(View.GONE);
				}
			} else {
				holder.image.setImageBitmap(Bimp.tempSelectBitmap.get(position).getBitmap());
			}

			return convertView;
		}

		public class ViewHolder {
			public ImageView image;
		}

		Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case 1:
					adapter.notifyDataSetChanged();
					break;
				}
				super.handleMessage(msg);
			}
		};

		public void loading() {
			new Thread(new Runnable() {
				public void run() {
					while (true) {
						if (Bimp.max == Bimp.tempSelectBitmap.size()) {
							Message message = new Message();
							message.what = 1;
							handler.sendMessage(message);
							break;
						} else {
							Bimp.max += 1;
							Message message = new Message();
							message.what = 1;
							handler.sendMessage(message);
						}
					}
				}
			}).start();
		}
	}

	public String getString(String s) {
		String path = null;
		if (s == null)
			return "";
		for (int i = s.length() - 1; i > 0; i++) {
			s.charAt(i);
		}
		return path;
	}

	protected void onRestart() {
		adapter.update();
		super.onRestart();
	}

	private static final int TAKE_PICTURE = 0x000001;
	private static final int MYBLOG = 0x000002;
	private ImageView ivBack;
	private TextView tvSend;
	private EditText etBlog;
	private Map<String, Object> map;
	private String uid;
	private ProgressDialog progressDialog;

	public void photo() {
		Intent openCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		openCameraIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
		File out = new File(getPhotopath());
		Uri uri = Uri.fromFile(out);
		// 获取拍照后未压缩的原图片，并保存在uri路径中
		openCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
		startActivityForResult(openCameraIntent, TAKE_PICTURE);
	}

	/**
	 * 获取原图片存储路径
	 * 
	 * @return
	 */
	private String getPhotopath() {
		// 照片全路径
		String fileName = "";
		// 文件夹路径
		String pathUrl = Environment.getExternalStorageDirectory() + "/mymy/";
		String imageName = "imageTest.jpg";
		File file = new File(pathUrl);
		file.mkdirs();// 创建文件夹
		fileName = pathUrl + imageName;
		return fileName;
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		switch (requestCode) {
		case TAKE_PICTURE:
			if (Bimp.tempSelectBitmap.size() < 9 && resultCode == RESULT_OK) {

				String fileName = String.valueOf(System.currentTimeMillis());
				// Bitmap bm = (Bitmap) data.getExtras().get("data");
				Bitmap bm = getBitmapFromUrl(getPhotopath(), 400, 600);
				//saveScalePhoto(bm);
				FileUtils.saveBitmap(bm, fileName);
				ImageItem takePhoto = new ImageItem();
				takePhoto.setBitmap(bm);
				Bimp.tempSelectBitmap.add(takePhoto);
				bm.recycle();
				System.gc();
				break;
			}
		}
	}

	/**
	 * 根据路径获取图片资源（已缩放）
	 * 
	 * @param url
	 *            图片存储路径
	 * @param width
	 *            缩放的宽度
	 * @param height
	 *            缩放的高度
	 * @return
	 */
	private Bitmap getBitmapFromUrl(String url, double width, double height) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inTempStorage = new byte[100*1024];	//为图片设置100k的缓存
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		//设置图片可以被回收，创建Bitmap用于存储Pixel的内存空间在系统内存不足时可以被回收
		options.inPurgeable = true;
		
		Bitmap bitmap = BitmapFactory.decodeFile(url);
		// 防止OOM发生
		options.inJustDecodeBounds = false;
		int mWidth = bitmap.getWidth();
		int mHeight = bitmap.getHeight();
		Matrix matrix = new Matrix();
		float scaleWidth = 1;
		float scaleHeight = 1;
		// try {
		// ExifInterface exif = new ExifInterface(url);
		// String model = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// 按照固定宽高进行缩放
		// 这里希望知道照片是横屏拍摄还是竖屏拍摄
		// 因为两种方式宽高不同，缩放效果就会不同
		// 这里用了比较笨的方式
		if (mWidth <= mHeight) {
			scaleWidth = (float) (width / mWidth);
			scaleHeight = (float) (height / mHeight);
		} else {
			scaleWidth = (float) (height / mWidth);
			scaleHeight = (float) (width / mHeight);
		}
		// matrix.postRotate(90); /* 翻转90度 */
		// 按照固定大小对图片进行缩放
		matrix.postScale(scaleWidth, scaleHeight);
		Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0, 0, mWidth, mHeight, matrix, true);
		// 用完了记得回收
		bitmap.recycle();
		System.gc();
		return newBitmap;
	}

	/**
	 * 存储缩放的图片
	 * 
	 * @param data
	 *            图片数据
	 */
	/*private void saveScalePhoto(Bitmap bitmap) {
		// 照片全路径
		String fileName = "";
		// 文件夹路径
		String pathUrl = Environment.getExternalStorageDirectory().getPath() + "/mymy/";
		String imageName = "imageScale.jpg";
		FileOutputStream fos = null;
		File file = new File(pathUrl);
		file.mkdirs();// 创建文件夹
		fileName = pathUrl + imageName;
		try {
			fos = new FileOutputStream(fileName);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				fos.flush();
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}*/

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		SharedPreferences sp = getSharedPreferences("edit", MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString("edit", etBlog.getText().toString());
		editor.commit();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub

		super.onResume();
		SharedPreferences sp = getSharedPreferences("edit", MODE_PRIVATE);
		String string = sp.getString("edit", "");
		etBlog.setText(string);
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		bimap.recycle();
		Bimp.tempSelectBitmap.clear();
		System.gc();
	}
}