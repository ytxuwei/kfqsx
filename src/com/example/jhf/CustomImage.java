package com.example.jhf;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.ImageView;

public class CustomImage extends ImageView {

	/**
	 * 属性
	 */
	private int type;
	private static final int TYPE_CIRCLE = 0;
	private static final int TYPE_ROUND = 1;
	private Bitmap mSrc; // 图片
	private int mRadius;// 圆角大小
	private int mWidth;// 控件宽度
	private int mHeight;// 控件高度

	public CustomImage(Context context) {
		this(context, null);
		// TODO Auto-generated constructor stub
	}

	public CustomImage(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 初始化自定义的参数
	 * 
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */

	public CustomImage(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		// 获取自定义控件的属性
		TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
				R.styleable.CustomImageView, defStyle, 0);
		int n = a.getIndexCount();
		for (int i = 0; i < n; i++) {
			int attr = a.getIndex(i);
			switch (attr) {
			case R.styleable.CustomImageView_borderRadius:
				mRadius = a.getDimensionPixelSize(attr, (int) TypedValue
						.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10f,
								getResources().getDisplayMetrics()));
				break;
			/*
			 * case R.styleable.CustomImageView_src: mSrc =
			 * BitmapFactory.decodeResource(getResources(),
			 * a.getResourceId(attr, 0)); break;
			 */
			case R.styleable.CustomImageView_type:
				type = a.getInt(attr, 0);// 默认为circle
				break;
			default:
				break;
			}
		}
		a.recycle();

	}

	/**
	 * 绘制控件
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		/* super.onDraw(canvas); */
		Drawable drawable = getDrawable();
		mSrc = ((BitmapDrawable) drawable).getBitmap();
		switch (type) {
		case TYPE_CIRCLE:
			int min = Math.min(mWidth, mHeight);
			mSrc = Bitmap.createScaledBitmap(mSrc, min, min, false);
			canvas.drawBitmap(createCircleImage(mSrc, min), 0, 0, null);
			break;

		case TYPE_ROUND:
			canvas.drawBitmap(createRoundConerImage(mSrc), 0, 0, null);
			break;
		}

	}

	private Bitmap createRoundConerImage(Bitmap source) {
		// TODO Auto-generated method stub
		final Paint paint = new Paint();
		paint.setAntiAlias(true);
		Bitmap target = Bitmap.createBitmap(mWidth, mHeight, Config.ARGB_8888);
		Canvas canvas = new Canvas(target);
		RectF rect = new RectF(0, 0, source.getWidth(), source.getHeight());
		canvas.drawRoundRect(rect, mRadius, mRadius, paint);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(source, 0, 0, paint);
		return target;
	}

	private Bitmap createCircleImage(Bitmap source, int min) {
		// TODO Auto-generated method stub
		final Paint paint = new Paint();
		paint.setAntiAlias(true);
		Bitmap target = Bitmap.createBitmap(min, min, Config.ARGB_8888);

		/**
		 * 产生一个同样大小的画布
		 */
		Canvas canvas = new Canvas(target);
		// 绘制圆形
		canvas.drawCircle(min / 2, min / 2, min / 2, paint);
		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		canvas.drawBitmap(source, 0, 0, paint);
		// canvas.drawBitmap(source, -originX, -originY, paint);
		return target;
	}

	/**
	 * 计算控件的高度和宽度
	 */

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		// super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		/**
		 * 设置宽度
		 */
		int specMode = MeasureSpec.getMode(widthMeasureSpec);
		int specSize = MeasureSpec.getSize(widthMeasureSpec);

		if (specMode == MeasureSpec.EXACTLY) // 指定精确尺寸时
		{

			mWidth = specSize;
		} else {
			// 由图片决定宽
			int desireWidth = getPaddingLeft() + getPaddingRight()// 图片实际尺寸
					+ mSrc.getWidth();
			if (specMode == MeasureSpec.AT_MOST) {
				mWidth = Math.min(desireWidth, specSize);
			} else {
				mWidth = desireWidth;
			}
		}
		/**
		 * 设置高度
		 */
		specMode = MeasureSpec.getMode(heightMeasureSpec);
		specSize = MeasureSpec.getSize(heightMeasureSpec);
		if (specMode == MeasureSpec.EXACTLY) {
			mHeight = specSize;
		} else {
			int desireHeight = getPaddingTop() + getPaddingBottom()// 图片高度
					+ mSrc.getHeight();
			if (specMode == MeasureSpec.AT_MOST) {
				mHeight = Math.min(desireHeight, specSize);
			} else {
				mHeight = desireHeight;
			}
		}
		setMeasuredDimension(mWidth, mHeight);
	}
}
