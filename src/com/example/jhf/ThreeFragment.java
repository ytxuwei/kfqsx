package com.example.jhf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.example.transceiver.Class_TV;
import com.example.transceiver.Class_Transceiver;
import com.example.transceiver.Shool_TV;
import com.example.transceiver.Shool_Transceiver;

public class ThreeFragment extends Fragment {
	private String[] str = { "校园广播", "校园电视台", "班级广播", "班级电视台", "视频直播" };
	private int[] inc = { 1, 2, 3, 4, 5 };
	private int[] intL = { R.drawable.class_xye };
	private Context context;
	private ArrayList<Map<String, Object>> data;
	private ShoolDAdater shoolDAdater;
	private ListView mListView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.shooldst, container, false);
		context = getActivity();
		mListView = (ListView) view.findViewById(R.id.lvshool);
		shoolDAdater = new ShoolDAdater(context, intn(), R.layout.shooldstitem);
		mListView.setAdapter(shoolDAdater);
		onlickTiem();
		return view;
	}

	private List<Map<String, Object>> intn() {
		data = new ArrayList<Map<String, Object>>();
		for (int i = 0; i < str.length; i++) {
			Map<String, Object> list = new HashMap<String, Object>();
			list.put("图标", inc[i]);
			list.put("项目", intL[0]);
			list.put("进入", str[i]);
			data.add(list);
		}
		return data;
	}

	private void onlickTiem() {
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				@SuppressWarnings("unchecked")
				HashMap<String, String> map = (HashMap<String, String>) mListView.getItemAtPosition(arg2);
				String keyStr = map.get("进入");
				if (keyStr.equals("校园广播")) {
					Intent intent = new Intent();
					intent.setClass(context, Shool_Transceiver.class);
					startActivity(intent);
				}
				if (keyStr.equals("校园电视台")) {
					Intent intent = new Intent();
					intent.setClass(context, Shool_TV.class);
					startActivity(intent);
				}
				if (keyStr.equals("班级广播")) {
					Intent intent = new Intent();
					intent.setClass(context, Class_Transceiver.class);
					startActivity(intent);
				}
				if (keyStr.equals("班级电视台")) {
					Intent intent = new Intent();
					intent.setClass(context, Class_TV.class);
					startActivity(intent);
				}
				if (keyStr.equals("视频直播")) {
					Uri uri = Uri.parse("http://www.blhd.net.cn/lmt.html");
					Intent intent = new Intent(Intent.ACTION_VIEW, uri);
					startActivity(intent);
				}

			}
		});
	}
}
