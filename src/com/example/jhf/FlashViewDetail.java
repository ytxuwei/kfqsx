package com.example.jhf;

import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EncodingUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.global.Constant;
import com.imooc.baseadapter.utils.Tools;

public class FlashViewDetail extends Activity{
	private JSONObject jsonObject;
	private String contentName;// 每条item的标题
	private ObjectMapper om = new ObjectMapper();
	private String code;
	private int position;
	private WebView mWebView;
	private ImageView ivBack;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.flash_view_detail);
		mWebView = (WebView) findViewById(R.id.wv_flash_view);
		ivBack = (ImageView) findViewById(R.id.iv_flash_view_back);
		position = getIntent().getIntExtra("position", 1);
		getJsonText();
		
		ivBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}
	
	private void getJsonText() {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						jsonObject = new JSONObject(result);
						JSONObject data = jsonObject.getJSONObject("data");
							contentName = (String) data.get("note");
							//mWebView.loadUrl(contentName);
							int a =14;	//字体大小
							String strUrl= "<html> \n" +
						              "<head> \n" +
						              "<style type=\"text/css\"> \n" +
						              "body {text-align:justify; font-size: "+a+"px; line-height: "+(a+6)+"px}\n" +
						              "</style> \n" +
						              "</head> \n" +
						              "<body>"+EncodingUtils.getString(contentName.getBytes(), "UTF-8")+"</body> \n </html>";
						       mWebView.loadData(strUrl, "text/html; charset=UTF-8", null);
						 
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else if (msg.what == 6) {
					Toast.makeText(FlashViewDetail.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(FlashViewDetail.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			private String result;

			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // 密码
				System.out.println("pos"+position);
				params.add(new BasicNameValuePair("carousel_id", position+"")); // 密码
				result = Tools.sendGet(Constant.flashViewMsg, params);
				System.out.println("reslut"+result);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}
}
