package com.example.space;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jhf.R;

public class MyAnswer extends Activity{

	private LinearLayout llAnswer;
	private LinearLayout llGone;
	private ImageView ivBack;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.my_answer);
		
		ivBack = (ImageView) findViewById(R.id.shooll_GK);
		llAnswer = (LinearLayout) findViewById(R.id.ll_my_answer);
		llGone = (LinearLayout) findViewById(R.id.ll_my_answer_gone);
		llAnswer.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MyAnswer.this,QuestionDetail2.class);
				startActivity(intent);
			}
		});
		ivBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		TextView tvQuick = (TextView) findViewById(R.id.tv_quick_question);
		tvQuick.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MyAnswer.this,QuickQuestion.class);
				startActivity(intent);
			}
		});
	}
}
