/**
 * 创建日期: 2015/08/03
 * 创建者： 李文雷
 * 功能说明： 
 * 程序中使用到的工具
 * eg：Toast的显示信息
 * 修改履历：
 *   VER     修改日        修改者     修改内容／理由
 *  ──────────────────────────────────────────────────────────────
 *
 */

package com.example.jhf;

import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

public class Toolss {
	/**
	 * 利用Toast显示消息
	 * 
	 * @param context
	 *            上下文
	 * @param text
	 *            要显示的消息
	 */
	public static void disInfo(Context context, String text) {
		Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}
	/**
	 * sd卡是否存在
	 * @return
	 */
	 public static boolean hasSdcard(){
         String state = Environment.getExternalStorageState();
         if(state.equals(Environment.MEDIA_MOUNTED)){
                 return true;
         }else{
                 return false;
         }
 }
}
