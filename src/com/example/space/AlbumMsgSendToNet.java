package com.example.space;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bean.BlogToSendBean;
import com.example.global.Constant;
import com.example.jhf.R;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.imooc.baseadapter.utils.BitmapUtils;
import com.imooc.baseadapter.utils.Tools;
import com.king.photo.activity.CItem;
import com.king.photo.activity.ImageAlbumActivity;
import com.king.photo.activity.ImageGalleryActivity;
import com.king.photo.util.Bimp;
import com.king.photo.util.FileUtils;
import com.king.photo.util.ImageItem;
import com.king.photo.util.PublicWay;
import com.king.photo.util.Res;

public class AlbumMsgSendToNet extends Activity {

	private GridView noScrollgridview;
	private GridAdapter adapter;
	private View parentView;
	private PopupWindow pop = null;
	private LinearLayout ll_popup;
	public static Bitmap bimap;
	private ObjectMapper om = new ObjectMapper();
	private String code;
	private ArrayList<CItem> lst;
	private String id = "1";// 被单击的id
	// private String noteStr = "";// 问题内容
	private String strImg = "";// 图片字串
	private static final String FILE_NAME = "USER_NOTE";
	private TextView textView;
	private String Image_id_intent;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Res.init(this);

		SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
		uid = sp.getString("uid", "0");
		bimap = BitmapFactory.decodeResource(getResources(), R.drawable.icon_addpic_unfocused);
		PublicWay.activityList.add(this);
		parentView = getLayoutInflater().inflate(R.layout.imageactivity_selectimgtwo, null);

		setContentView(parentView);
		// Image_id_intent = getIntent().getStringExtra("Image_id_intentID_id");
		// SharedPreferences sharedPreferences = getSharedPreferences(FILE_NAME,
		// Activity.MODE_PRIVATE);
		// Editor edit = sharedPreferences.edit();
		// edit.putString("image", Image_id_intent);
		// edit.commit();
		// getJsonText();
		Init();
		// etBlog.setText(note);
	}

	// @Override
	// protected void onPause() {
	// // TODO Auto-generated method stub
	// super.onPause();
	// SharedPreferences sharedPreferences = getSharedPreferences(FILE_NAME,
	// Activity.MODE_PRIVATE);
	// Image_id_intent = sharedPreferences.getString("image", "");
	// }

	// @Override
	// protected void onStart() {
	// // TODO Auto-generated method stub
	// super.onStart();
	// }
	public void Init() {
		tvSend = (TextView) findViewById(R.id.activity_selectimg_send_idid);
		tvBack = (TextView) findViewById(R.id.shooll_GK_id);
		tvBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		// etBlog = (EditText) findViewById(R.id.activity_selectimg_send_idid);
		// textView = (TextView) findViewById(R.id.shooll_GK);
		// textView.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// startActivity(new Intent(getApplicationContext(),
		// Class_Question.class));
		// }
		// });
		/**
		 * 得到博客内容、图片列表 传输数据？
		 */
		tvSend.setOnClickListener(new OnClickListener() {
			private String image;

			@Override
			public void onClick(View arg0) {
				ArrayList<BlogToSendBean> images = new ArrayList<BlogToSendBean>();
				for (int i = 0; i < Bimp.tempSelectBitmap.size(); i++) {
					// String image =
					// BitmapUtils.convertBitmapToString(Bimp.tempSelectBitmap.get(i).getBitmap());
					if (!TextUtils.isEmpty(Bimp.tempSelectBitmap.get(i).getImagePath())) {
						try {
							image = Bimp.bitmapToString(Bimp.tempSelectBitmap.get(i).getImagePath());
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else {
						image = BitmapUtils.convertBitmapToString(Bimp.tempSelectBitmap.get(i).getBitmap());
					}
					String title = Math.random() * 1000 + ".jpg";
					images.add(new BlogToSendBean(title, image));

				}

				try {
					strImg = om.writeValueAsString(images);
				} catch (JsonProcessingException e1) {
					// TODO Auto-generated catch block
					// Toast.makeText(getApplicationContext(), e1, duration)
					e1.printStackTrace();
				}
				sendJsontoNet();
				// if (string != null) {
				//
				//
				// // finish();
				// }

			}
		});
		pop = new PopupWindow(AlbumMsgSendToNet.this);

		View view = getLayoutInflater().inflate(R.layout.item_popupwindows, null);

		ll_popup = (LinearLayout) view.findViewById(R.id.ll_popup);
		pop.setWidth(LayoutParams.MATCH_PARENT);
		pop.setHeight(LayoutParams.WRAP_CONTENT);
		pop.setBackgroundDrawable(new BitmapDrawable());
		pop.setFocusable(true);
		pop.setOutsideTouchable(true);
		pop.setContentView(view);

		RelativeLayout parent = (RelativeLayout) view.findViewById(R.id.parent);
		Button bt1 = (Button) view.findViewById(R.id.item_popupwindows_camera);
		Button bt2 = (Button) view.findViewById(R.id.item_popupwindows_Photo);
		Button bt3 = (Button) view.findViewById(R.id.item_popupwindows_cancel);
		parent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				pop.dismiss();
				ll_popup.clearAnimation();
			}
		});
		bt1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				photo();
				pop.dismiss();
				ll_popup.clearAnimation();
			}
		});
		bt2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(AlbumMsgSendToNet.this, ImageAlbumActivity.class);
				// Intent intent = new Intent(MainActivitypoth.this,
				// CopyOfAlbumActivity.class);
				startActivity(intent);
				overridePendingTransition(R.anim.activity_translate_in, R.anim.activity_translate_out);
				pop.dismiss();
				ll_popup.clearAnimation();
				finish();
			}
		});
		bt3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				pop.dismiss();
				ll_popup.clearAnimation();
			}
		});

		noScrollgridview = (GridView) findViewById(R.id.noScrollgridview);
		noScrollgridview.setSelector(new ColorDrawable(Color.TRANSPARENT));
		adapter = new GridAdapter(this);
		adapter.update();
		noScrollgridview.setAdapter(adapter);
		noScrollgridview.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// noteStr = etBlog.getText().toString().trim();
				// SharedPreferences sharedPreferences = getSharedPreferences(
				// FILE_NAME, Activity.MODE_PRIVATE);
				// edit = sharedPreferences.edit();
				// edit.putString("Value", noteStr);
				// edit.commit();
				// 隐藏输入法
				((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(AlbumMsgSendToNet.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				if (arg2 == Bimp.tempSelectBitmap.size()) {
					Log.i("ddddddd", "----------");
					ll_popup.startAnimation(AnimationUtils.loadAnimation(AlbumMsgSendToNet.this, R.anim.activity_translate_in));
					pop.showAtLocation(parentView, Gravity.BOTTOM, 0, 0);
				} else {
					Intent intent = new Intent(AlbumMsgSendToNet.this, ImageGalleryActivity.class);
					intent.putExtra("position", "1");
					intent.putExtra("ID", arg2);
					startActivity(intent);
				}
			}
		});

	}

	/**
	 * 发送到后台
	 */
	private void sendJsontoNet() {
		progressDialog = ProgressDialog.show(AlbumMsgSendToNet.this, "请稍等...", "正在发表...", true, true);
		// 点击不会消失
		progressDialog.setCanceledOnTouchOutside(false);
		final Handler handler = new Handler() {

			public void handleMessage(Message msg) {
				if (msg.what == 7) {

					for (int H = 0; H < PublicWay.activityList.size(); H++) {
						if (null != PublicWay.activityList.get(H)) {
							PublicWay.activityList.get(H).finish();
						}
					}
					Bimp.tempSelectBitmap.clear();
					Bimp.max = 0;
					Toast.makeText(getApplicationContext(), "发表成功！", 0).show();
					Intent intent = new Intent();
					intent.setAction("action.refreshImage");
					sendBroadcast(intent);
					finish();
				} else if (msg.what == 6) {
					String string2 = msg.obj.toString();
					Toast.makeText(getApplicationContext(), string2, Toast.LENGTH_LONG).show();
					Toast.makeText(getApplicationContext(), "发表失败！", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(getApplicationContext(), "评论消失在二次元", 0).show();
				}
			}

		};
		new Thread() {
			public void run() {
				SharedPreferences sharedPreferences = getSharedPreferences(FILE_NAME, Activity.MODE_PRIVATE);
				Image_id_intent = sharedPreferences.getString("image", "");
				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); // uid
				// params.add(new BasicNameValuePair("note", noteStr)); //
				// 博客内容内容
				params.add(new BasicNameValuePair("data_image", strImg)); // 图片列表
				params.add(new BasicNameValuePair("photo_id", Image_id_intent));
				String result = Tools.sendPost(Constant.uploadImage, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						String code = jsonNode.get("code").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;

						} else {
							// System.out.println(result+"结果测试");
							message.what = 6;// 登陆失败
							message.obj = result;
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
				progressDialog.dismiss();
			};
		}.start();
	}

	@SuppressLint("HandlerLeak")
	public class GridAdapter extends BaseAdapter {
		private LayoutInflater inflater;
		private int selectedPosition = -1;
		private boolean shape;

		public boolean isShape() {
			return shape;
		}

		public void setShape(boolean shape) {
			this.shape = shape;
		}

		public GridAdapter(Context context) {
			inflater = LayoutInflater.from(context);
		}

		public void update() {
			loading();
		}

		public int getCount() {
			if (Bimp.tempSelectBitmap.size() == 9) {
				return 9;
			}
			return (Bimp.tempSelectBitmap.size() + 1);
		}

		public Object getItem(int arg0) {
			return null;
		}

		public long getItemId(int arg0) {
			return 0;
		}

		public void setSelectedPosition(int position) {
			selectedPosition = position;
		}

		public int getSelectedPosition() {
			return selectedPosition;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.item_published_grida, parent, false);
				holder = new ViewHolder();
				holder.image = (ImageView) convertView.findViewById(R.id.item_grida_image);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			if (position == Bimp.tempSelectBitmap.size()) {
				holder.image.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.icon_addpic_unfocused));
				if (position == 9) {
					holder.image.setVisibility(View.GONE);
				}
			} else {
				holder.image.setImageBitmap(Bimp.tempSelectBitmap.get(position).getBitmap());
			}

			return convertView;
		}

		public class ViewHolder {
			public ImageView image;
		}

		Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case 1:
					adapter.notifyDataSetChanged();
					break;
				}
				super.handleMessage(msg);
			}
		};

		public void loading() {
			new Thread(new Runnable() {
				public void run() {
					while (true) {
						if (Bimp.max == Bimp.tempSelectBitmap.size()) {
							Message message = new Message();
							message.what = 1;
							handler.sendMessage(message);
							break;
						} else {
							Bimp.max += 1;
							Message message = new Message();
							message.what = 1;
							handler.sendMessage(message);
						}
					}
				}
			}).start();
		}
	}

	public String getString(String s) {
		String path = null;
		if (s == null)
			return "";
		for (int i = s.length() - 1; i > 0; i++) {
			s.charAt(i);
		}
		return path;
	}

	protected void onRestart() {
		adapter.update();
		super.onRestart();
	}

	private static final int TAKE_PICTURE = 0x000001;
	private TextView tvSend;// 发送
	private String uid;
	private ProgressDialog progressDialog;
	private TextView tvBack;

	public void photo() {
		Intent openCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		openCameraIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
		File out = new File(getPhotopath());
		Uri uri = Uri.fromFile(out);
		// 获取拍照后未压缩的原图片，并保存在uri路径中
		openCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
		startActivityForResult(openCameraIntent, TAKE_PICTURE);
	}

	/**
	 * 获取原图片存储路径
	 * 
	 * @return
	 */
	private String getPhotopath() {
		// 照片全路径
		String fileName = "";
		// 文件夹路径
		String pathUrl = Environment.getExternalStorageDirectory() + "/mymy/";
		String imageName = "imageTest.jpg";
		File file = new File(pathUrl);
		file.mkdirs();// 创建文件夹
		fileName = pathUrl + imageName;
		return fileName;
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case TAKE_PICTURE:
			if (Bimp.tempSelectBitmap.size() < 9 && resultCode == RESULT_OK) {

				String fileName = String.valueOf(System.currentTimeMillis());
				// Bitmap bm = (Bitmap) data.getExtras().get("data");
				Bitmap bm = getBitmapFromUrl(getPhotopath(), 300, 500);
				// saveScalePhoto(bm);
				FileUtils.saveBitmap(bm, fileName);

				ImageItem takePhoto = new ImageItem();
				takePhoto.setBitmap(bm);
				Bimp.tempSelectBitmap.add(takePhoto);
			}
			break;
		}
	}

	/**
	 * 根据路径获取图片资源（已缩放）
	 * 
	 * @param url
	 *            图片存储路径
	 * @param width
	 *            缩放的宽度
	 * @param height
	 *            缩放的高度
	 * @return
	 */
	private Bitmap getBitmapFromUrl(String url, double width, double height) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inTempStorage = new byte[100 * 1024]; // 为图片设置100k的缓存
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inPurgeable = true;

		Bitmap bitmap = BitmapFactory.decodeFile(url);
		// 防止OOM发生
		options.inJustDecodeBounds = false;
		int mWidth = bitmap.getWidth();
		int mHeight = bitmap.getHeight();
		Matrix matrix = new Matrix();
		float scaleWidth = 1;
		float scaleHeight = 1;
		// try {
		// ExifInterface exif = new ExifInterface(url);
		// String model = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// 按照固定宽高进行缩放
		// 这里希望知道照片是横屏拍摄还是竖屏拍摄
		// 因为两种方式宽高不同，缩放效果就会不同
		// 这里用了比较笨的方式
		if (mWidth <= mHeight) {
			scaleWidth = (float) (width / mWidth);
			scaleHeight = (float) (height / mHeight);
		} else {
			scaleWidth = (float) (height / mWidth);
			scaleHeight = (float) (width / mHeight);
		}
		// matrix.postRotate(90); /* 翻转90度 */
		// 按照固定大小对图片进行缩放
		matrix.postScale(scaleWidth, scaleHeight);
		Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0, 0, mWidth, mHeight, matrix, true);
		// 用完了记得回收
		bitmap.recycle();
		return newBitmap;
	}

	/**
	 * 存储缩放的图片
	 * 
	 * @param data
	 *            图片数据
	 */
	/*
	 * private void saveScalePhoto(Bitmap bitmap) { // 照片全路径 String fileName =
	 * ""; // 文件夹路径 String pathUrl =
	 * Environment.getExternalStorageDirectory().getPath()+"/mymy/"; String
	 * imageName = "imageScale.jpg"; FileOutputStream fos = null; File file =
	 * new File(pathUrl); file.mkdirs();// 创建文件夹 fileName = pathUrl + imageName;
	 * try { fos = new FileOutputStream(fileName);
	 * bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos); } catch
	 * (FileNotFoundException e) { e.printStackTrace(); } finally { try {
	 * fos.flush(); fos.close(); } catch (IOException e) { e.printStackTrace();
	 * } } }
	 */

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			for (int i = 0; i < PublicWay.activityList.size(); i++) {
				if (null != PublicWay.activityList.get(i)) {
					PublicWay.activityList.get(i).finish();
				}
			}
			finish();
		}
		return true;
	}
}