package com.example.space;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.bean.ParentListBean;
import com.example.global.Constant;
import com.example.jhf.R;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.SingleRequestQueue;
import com.imooc.baseadapter.utils.Tools;

public class ParentList extends Activity implements OnClickListener,OnRefreshListener2<ListView>{
	private PullToRefreshListView lv;
	private ArrayList<ParentListBean> datas;
	private ImageView ivBack;
	private MyAdapter adapter;
	private RequestQueue mQueue;
	private ImageLoader imageLoader;
	private int pageCount = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.parent_list);
		
		
		mQueue = SingleRequestQueue.getRequestQueue(getApplicationContext());
		imageLoader = new ImageLoader(mQueue, new BitmapCache());
		ivBack = (ImageView) findViewById(R.id.iv_back);
		ivBack.setOnClickListener(this);
		lv = (PullToRefreshListView) findViewById(R.id.lv_parent_list);
		
		datas = new ArrayList<ParentListBean>();
		getJsonText(1);
		
		
		adapter = new MyAdapter();
		lv.setAdapter(adapter);
		lv.setOnRefreshListener(this);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ParentList.this,ParentDetail.class);
				intent.putExtra("lineId", datas.get(arg2).lineId);
				startActivity(intent);
			}
		});
		
	}
	
	class MyAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return datas.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return datas.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			View view = View.inflate(getApplicationContext(), R.layout.item_parent_list, null);
			NetworkImageView iv = (NetworkImageView) view.findViewById(R.id.iv_parent_list_item);
			TextView tvParent = (TextView) view.findViewById(R.id.tv_parent_list_item_parent);
			TextView tvChild = (TextView) view.findViewById(R.id.tv_parent_list_item_child);
			//iv.setImageBitmap(datas.get(arg0).imgSrc);
			//iv.setImageURI(Uri.parse(datas.get(arg0).imgSrc));
			iv.setImageUrl(datas.get(arg0).imgSrc, imageLoader);
			tvParent.setText(datas.get(arg0).parentName);
			tvChild.setText(datas.get(arg0).studentName);
			return view;
		}
		
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.iv_back:
			finish();
			break;

		default:
			break;
		}
	}
	
	private void getJsonText(final int i) {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				String result = msg.obj.toString();
				try {
					JSONObject jsonObject = new JSONObject(result);
					pageCount = jsonObject.getInt("page_count");
					JSONArray jsonArray = jsonObject.getJSONArray("data");
					for (int i = 0; i < jsonArray.length(); i++) {
						jsonObject2 = (JSONObject) jsonArray.opt(i);
						//String contentName = (String) jsonObject2.get("class_name");
						//int contentId = (Integer) jsonObject2.get("id");
						//Bitmap mImg = BitmapUtils.stringtoBitmap(jsonObject2.getString("portrait"));
						String mImg = jsonObject2.getString("portrait");
						int lineId = jsonObject2.getInt("line_id");
						String parentName  = jsonObject2.getString("parent_name");
						int parentId = jsonObject2.getInt("parent_id");
						String studentName = jsonObject2.getString("student_name");
						datas.add(new ParentListBean(mImg, lineId, parentName, parentId, studentName));
					}
					//adapter = new MyAdapter();
					// 将添加的每条item放到Lstview中
					//lv.setAdapter(adapter);
					adapter.notifyDataSetChanged();

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数
				SharedPreferences sp = getSharedPreferences("UserId",
						MODE_PRIVATE);
				String uid = sp.getString("uid", "0");

				// 设置参数
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); //uid getIntent().getIntExtra("classId", 0)+""
				params.add(new BasicNameValuePair("class_id", getIntent().getIntExtra("classId", 0)+""));	//这里获取class_id有问题
				params.add(new BasicNameValuePair("page", i+""));
				params.add(new BasicNameValuePair("page_count", pageCount+""));
				String result = Tools.sendGet(Constant.memberParents, params);
				Message message = new Message();
				message.what = 7;
				message.obj = result;
				handler.sendMessage(message);
			};
		}.start();
	}
private int i = 2;
	@Override
	public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				datas.clear();
				getJsonText(1);
				i=2;
				lv.onRefreshComplete();
			}
		}, 3000);
	}

	@Override
	public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				getJsonText(i);
				i++;
				lv.onRefreshComplete();
			}
		}, 3000);
	}
}
