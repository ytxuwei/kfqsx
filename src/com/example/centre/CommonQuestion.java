package com.example.centre;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.bean.CourseBean;
import com.example.jhf.R;

public class CommonQuestion extends Activity{
	private ImageView ivBack;
	private ListView mListView;
	private ArrayList<CourseBean> datas;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.common_question);
		
		datas = new ArrayList<CourseBean>();
		datas.add(new CourseBean(0, "怎样上传图片"));
		datas.add(new CourseBean(1, "怎样查看微博全文"));
		datas.add(new CourseBean(2, "快速提问的使用方法"));
		datas.add(new CourseBean(3, "修改个人信息说明"));
		datas.add(new CourseBean(4, "班级认证步骤说明"));
		
		ivBack = (ImageView) findViewById(R.id.shooll_GK);
		ivBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		mListView = (ListView) findViewById(R.id.lv_common_question);
		MyAdapter adapter = new MyAdapter();
		mListView.setAdapter(adapter);
		
		mListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = null;
				
					intent = new Intent(CommonQuestion.this,CommQuestionDetail.class);

			
				startActivity(intent);
			}
		});
	}
	
	class MyAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return datas.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return datas.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			View view = View.inflate(getApplicationContext(), R.layout.common_question_item, null);
			TextView tvTitle= (TextView) view.findViewById(R.id.tv_comm_question_item);
			tvTitle.setText(datas.get(arg0).name);
			return view;
		}
		
	}
}
