package com.example.open;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.global.Constant;
import com.example.index.RefreshableView;
import com.example.index.RefreshableView.PullToRefreshListener;
import com.example.index.ShoolLITools;
import com.example.jhf.MyContentDetailActivity;
import com.example.jhf.R;
import com.example.notice.Notices;
import com.example.view.ListViewIndicatorUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.imooc.baseadapter.utils.Tools;

/**
 * 课时
 * 
 * @author Administrator
 * 
 */
public class OpenksActivity extends ListActivity{
	private TextView tv;
	private ImageView shooll_GK;
	private ShoolLITools shoolk;
	private OpenksActivityAdapter shoolDAdater;
	private TextView tvTitle;
	private Handler handler = new Handler();
	private RefreshableView refreshableView;

	private ArrayList<Notices> datas; // 列表
	private String result;
	private String note;
	private PullToRefreshListView mPullRefreshListView;
	private int i = 2; // 加载更多第几页
	private int pageCount;	//请求总页数
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.openks);

		SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
		uid = sp.getString("uid", "0");
		getJsonText(1);
		datas = new ArrayList<Notices>();
		tvTitle = (TextView) findViewById(R.id.tv_openks_title);
		shooll_GK = (ImageView) findViewById(R.id.shooll_GK);
		shooll_GK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				OpenksActivity.this.finish();
			}
		});
		//lvshool = getListView();
		mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);
		mPullRefreshListView.setMode(Mode.BOTH);
		ListViewIndicatorUtil.initIndicator(mPullRefreshListView);
		shoolDAdater = new OpenksActivityAdapter(OpenksActivity.this, datas);
		// 将添加的每条item放到Lstview中
		mPullRefreshListView.setAdapter(shoolDAdater);
		mPullRefreshListView.setOnRefreshListener(new com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2<ListView>() {
			@Override
			public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullDownToRefresh");
				
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						datas.clear();
						getJsonText(1);
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);
			}

			@Override
			public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
				Log.e("TAG", "onPullUpToRefresh");
				// 这里写上拉加载更多的任务
				
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						getJsonText(i);
						i++;
						mPullRefreshListView.onRefreshComplete();
					}
				}, 3000);
				
			}

		});
		// 自定义listview下部布局

		

		mPullRefreshListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
				new Thread() {
					public void run() {
						// 设置参数
						List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
						params.add(new BasicNameValuePair("db", Constant.db));// 数据库
						params.add(new BasicNameValuePair("uid", uid));
						params.add(new BasicNameValuePair("hour_id", contentId + ""));
						String homeworkDetail = Tools.sendGet(Constant.classHourMsg, params);
						System.out.println("homework"+homeworkDetail);
						processDetailData(homeworkDetail);

						Intent intent = new Intent(OpenksActivity.this, MyContentDetailActivity.class);
						intent.putExtra("title", datas.get(arg2-1).name);
						intent.putExtra("note", note);
						intent.putExtra("date", mDate);
						startActivity(intent);
					}
				}.start();

			}
		});
	}

	private void processDetailData(String json) {
		try {
			JSONObject jsonObj = new JSONObject(json);
			int cccode = jsonObj.getInt("code");
			// System.out.println("cccode"+cccode);
			JSONObject array = jsonObj.getJSONObject("data");
			note = (String) array.get("note");
			mDate = array.getString("date");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private String contentName;// 每条item的标题
	private ObjectMapper om = new ObjectMapper();
	private String code;
	private int contentId;
	private String uid;
	private String mDate;

	private void getJsonText(final int i) {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					String result = msg.obj.toString();
					try {
						jsonObject = new JSONObject(result);
						pageCount = jsonObject.getInt("page_count");
						jsonArray = jsonObject.getJSONArray("data");
						for (int i = 0; i < jsonArray.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray.opt(i);
							contentName = (String) jsonObject2.get("name");
							contentId = (Integer) jsonObject2.get("id");

							datas.add(new Notices(contentName, contentId));
						}
					/*	shoolDAdater = new OpenksActivityAdapter(OpenksActivity.this, datas);
						// 将添加的每条item放到Lstview中
						lvshool.setAdapter(shoolDAdater);*/
						shoolDAdater.notifyDataSetChanged();

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else if (msg.what == 6) {
					Toast.makeText(OpenksActivity.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(OpenksActivity.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数

				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("db", Constant.db));// 数据库
				params.add(new BasicNameValuePair("uid", uid)); 
				params.add(new BasicNameValuePair("page", i+"")); 
				params.add(new BasicNameValuePair("page_count", pageCount+"")); 
				result = Tools.sendGet(Constant.classHour, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败
						}
					} else {
						message.what = 9;// 无网络
					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

}
