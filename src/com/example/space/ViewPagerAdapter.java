package com.example.space;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher.OnPhotoTapListener;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.example.bean.BlogContentImageBean;



/**
 * @author:Jack Tony
 * @tips :viewpager��������
 * @date :2014-11-12
 */
public class ViewPagerAdapter extends PagerAdapter {

	private ArrayList<BlogContentImageBean> sDrawables;
	private Context mContext;
	private ZoomTutorial mZoomTutorial;
	private ImageLoader imageLoader;
	public ViewPagerAdapter(Context context, ArrayList<BlogContentImageBean> imgIds,
			ZoomTutorial zoomTutorial,ImageLoader imageLoader) {
		this.sDrawables = imgIds;
		this.mContext = context;
		this.mZoomTutorial = zoomTutorial;
		this.imageLoader = imageLoader;
	}

	@Override
	public int getCount() {
		return sDrawables.size();
	}

	@Override
	public View instantiateItem(ViewGroup container, final int position) {
		final PhotoView imageView = new PhotoView(mContext);
		imageView.setOnPhotoTapListener(new OnPhotoTapListener() {
			
			@Override
			public void onPhotoTap(View view, float x, float y) {
				// TODO Auto-generated method stub
				mZoomTutorial.closeZoomAnim(position);
			}
		});
		ImageListener listener = ImageLoader.getImageListener(imageView, 0, 0);
		imageLoader.get(sDrawables.get(position).image, listener);
		container.addView(imageView, LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		return imageView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}
	
}