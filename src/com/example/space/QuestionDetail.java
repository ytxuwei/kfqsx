package com.example.space;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.bean.BlogContentImageBean;
import com.example.global.Constant;
import com.example.jhf.R;
import com.example.monthstar.OneDatyPerson;
import com.example.view.DropHtml;
import com.facebook.drawee.view.SimpleDraweeView;
import com.imooc.baseadapter.utils.BitmapCache;
import com.imooc.baseadapter.utils.CommonAdapter;
import com.imooc.baseadapter.utils.SingleRequestQueue;
import com.imooc.baseadapter.utils.Tools;
import com.king.photo.activity.MainActivitypoth;

/**

 * 详细页面信息

 * 

 * @author Administrator

 * 

 */
public class QuestionDetail extends Activity {
	private ListView lvAnswer;
	//private MyAdapter adapter;
	//private TextView tvQuick;// 快速提问
	private TextView studentName;// 学生姓名

	private String studentNameStr;// 学生姓名

	private TextView parentName;// 父母名字

	private String parentNameStr;// 父母姓名

	private TextView subjectName;// 科目名字

	private String subjectNameStr;// 科目姓名

	private TextView className;// 班级名字

	private String classNameStr;// 班级姓名

	private TextView timeName;// 问题时间

	private String tiemNameStr;// 问题姓名

	private TextView answercount;// 问题数量

	private String answercountStr;// 回答人数

	private String head_portrait;// 头像；

	private Boolean f;
	// private ImageView heView;// 头像


	private SimpleDraweeView he;
	// private ImageView noteImage;// 内容


	private String noteStr_tu;// 内容；

	private String image_id;// 图片id

	private String imageStr;// 放大图

	private String noteStr;// 问题内容；

	private TextView noTextView;// 问题内容

	private ObjectMapper om = new ObjectMapper();
	private String onedayconten;// 每条item的标题

	private String code;
	private JSONArray jsonArray;
	private JSONObject jsonObject;
	private ArrayList<AnswerPer> data;
	private GridView gridView;
	private ImageView ivBack;
	private ArrayList<OneDatyPerson> datata;
	private EditText etMsg;// 输入回答问题

	private Button btsend;
	private String strdata;
	private String id;// 问题用户id

	private MyAdapter myAdapter;
	private String comment_id;// 评论者id

	private String comment_id2;
	private String commentidid;// 点赞的id

	private ProgressDialog progressDialog;// 进度条

	private String str;// 评论后的条数

	private int position;
	private int j = 0;// 控制评论加1的循环

	private ImageLoader imageLoader;
	private String uid;
	private ArrayList<BlogContentImageBean> imageBean;
	private Context context;
	private RequestQueue mQueue;

	// private MyAdapter adapter;


	// sprivate ArrayList<E>


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub


		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.question_detail);
		context = this;
		data = new ArrayList<AnswerPer>();
		imageBean = new ArrayList<BlogContentImageBean>();
		mQueue = SingleRequestQueue.getRequestQueue(getApplicationContext());
		imageLoader = new ImageLoader(mQueue, new BitmapCache());
		init();
		//quiz();


		back();
		getJsonText();
		senddata();
		// adapter = new MyAdapter();


		// lvAnswer.setAdapter(adapter);
		myAdapter = new MyAdapter(QuestionDetail.this, data);
		lvAnswer.setAdapter(myAdapter);

	}

	/**

	 * 数据发送

	 */
	private void senddata() {

		btsend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				strdata = etMsg.getText().toString().trim();
				if (!"".equals(strdata)) {
					sendJsonText();
					myAdapter.notifyDataSetChanged();
					int i = Integer.parseInt(answercountStr);
					if (0 == j) {
						i = i + 1;
						j = j + 1;
						str = i + "";
						answercount.setText(str + "人回答");
					} else {
						int a = Integer.parseInt(str);
						a = a + 1;
						str = a + "";
						answercount.setText(str + "人回答");
					}
				} else {
					Toast.makeText(getApplicationContext(), "不能为空", 0).show();
				}

			}
		});
	}

	/**

	 * 发送评论的JSON数据

	 */
	@SuppressLint("HandlerLeak")
	private void sendJsonText() {
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;
			public void handleMessage(Message msg) {
				if (msg.what == 7) {
					data.clear();
					String result = msg.obj.toString();
					try {
						jsonObject = new JSONObject(result);
						JSONObject jObject = jsonObject.getJSONObject("data");
						JSONArray jsonArray2 = jObject.getJSONArray("data_comment");
						for (int i = 0; i < jsonArray2.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray2.opt(i);
							String objectuser = (String) jsonObject2.get("comment_user");// 姓名

							String objectdate = (String) jsonObject2.get("comment_date");// 评论时间

							String objectnote = (String) jsonObject2.get("comment_note");// 评论内同

							String objectportrait = (String) jsonObject2.get("comment_portrait");// 评论者头像

							String objectcount = (String) jsonObject2.get("like_count").toString();// 点赞人数

							String comment_id = (String) jsonObject2.get("comment_id").toString();// 点赞者id

							AnswerPer answerPer = new AnswerPer();
							answerPer.setAnswerNameStr(objectuser);
							answerPer.setNoteStr(objectnote);
							answerPer.setTime(objectdate);
							answerPer.setTitle(objectportrait);
							answerPer.setCuonte(objectcount);
							answerPer.setComment_id(comment_id);
							data.add(answerPer);
						}
						myAdapter.notifyDataSetChanged();
						InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);// 隐藏对话框

						imm.hideSoftInputFromWindow(etMsg.getWindowToken(), 0);// 隐藏对话框

						etMsg.setText("");

					} catch (JSONException e) {
						// TODO Auto-generated catch block


						e.printStackTrace();
					}
				} else if (msg.what == 6) {
					// Toast.makeText(QuestionDetail.this, "帐号或密码错误", 0).show();


				} else if (msg.what == 10) {
					Toast.makeText(QuestionDetail.this, "忘了联网了吧", 0).show();
				}

			}

		};
		new Thread() {
			public void run() {
				// 设置参数


				SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				// strdata = etMsg.getText().toString().trim();


				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				params.add(new BasicNameValuePair("uid", uid)); // 密码

				params.add(new BasicNameValuePair("db", Constant.db));// 数据库

				params.add(new BasicNameValuePair("note", strdata));
				params.add(new BasicNameValuePair("knowledge_id", id));
				String result = Tools.sendPost(Constant.answerComment, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();


						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败

						}
					} else {
						message.what = 9;// 无网络

					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception


					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

	private void back() {
		ivBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent();
				intent.putExtra("addContent", str);
				intent.putExtra("position_back", position);
				setResult(RESULT_OK, intent);
				finish();
			}
		});
	}

	/*

	 * 块速提问

	 */
	/*private void quiz() {

		tvQuick.setOnClickListener(new OnClickListener() {



			@Override

			public void onClick(View arg0) {

				// TODO Auto-generated method stub

				Intent intent = new Intent(QuestionDetail.this, MainActivitypoth.class);

				startActivity(intent);

			}

		});

	}*/

	private void init() {

		View view = View.inflate(this, R.layout.head_answer, null);
		//tvQuick = (TextView) findViewById(R.id.tv_my_answer_id);


		studentName = (TextView) view.findViewById(R.id.parent_id);
		ivBack = (ImageView) findViewById(R.id.shooll_GK);
		parentName = (TextView) view.findViewById(R.id.parentName_id);
		subjectName = (TextView) view.findViewById(R.id.sujiectName_id);
		className = (TextView) view.findViewById(R.id.class_name);
		timeName = (TextView) view.findViewById(R.id.time_id);
		answercount = (TextView) view.findViewById(R.id.answerconut_id);
		// heView = (ImageView) view.findViewById(R.id.head_portrait);


		he = (SimpleDraweeView) view.findViewById(R.id.question_IV_id_ti);
		noTextView = (TextView) view.findViewById(R.id.note_id);
		gridView = (GridView) view.findViewById(R.id.classgridview_ididid);
		gridView.setVerticalSpacing(10);
		lvAnswer = (ListView) findViewById(R.id.lv_question_detail);
		btsend = (Button) findViewById(R.id.btn_post_msg);
		etMsg = (EditText) findViewById(R.id.et_blg_leave_msg);
		// 对话框自动上拉，布局设置成权重


		etMsg.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

				String contents = etMsg.getText().toString(); // 内容

				int len = contents.length(); // 内容长度

				int width = etMsg.getWidth(); // 控件宽度

				int height = etMsg.getHeight(); // 控件高度

				float px = etMsg.getTextSize();// 得到字体像素

				double length = Math.floor(width / px); // 能容纳字母个数


				if (len > length) {
					ViewGroup.LayoutParams lp = etMsg.getLayoutParams();
					int offset = 1; // 计算出需要行数

					if (len % length == 0) {
						offset = (int) (len / length);
					} else {
						offset = (int) (len / length) + 1;
					}

					// 需要的高度


					int need_h = (int) (px * offset) + (int) (9 * offset); // 前部分是行行高，后部分是行间距高度


					// Log.d(tag, "需要行数："+offset + ", 需要高度：" + need_h);




					lp.height = need_h;
					etMsg.setLayoutParams(lp);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub




			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub




			}
		});
		lvAnswer.addHeaderView(view);

	}

	// 评论


	class MyAdapter extends BaseAdapter {
		private LayoutInflater layoutInflater;
		private ArrayList<AnswerPer> data;
		private int layoutID;
		private Context context;
		private boolean ib = true;

		public MyAdapter(Context context, ArrayList<AnswerPer> data) {
			this.context = context;
			this.layoutInflater = LayoutInflater.from(context);
			this.layoutID = layoutID;
			this.data = data;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub


			return data.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub


			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub


			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			// TODO Auto-generated method stub


			// View view = View.inflate(getApplicationContext(),


			// R.layout.question_detail_item, null);


			// RequestQueue mQueue = Volley.newRequestQueue(context);


			// ImageLoader imageLoader = new ImageLoader(mQueue, new


			// BitmapCache());


			final int index = position;
			final ViewHolder viewHolder;
			if (convertView == null) {
				convertView = layoutInflater.inflate(R.layout.question_detail_item, null);
				viewHolder = new ViewHolder();
				viewHolder.image = (NetworkImageView) convertView.findViewById(R.id.question_IV_id);
				viewHolder.answerName = (TextView) convertView.findViewById(R.id.question_Name_id);
				viewHolder.count = (TextView) convertView.findViewById(R.id.question_count_id);
				viewHolder.note = (TextView) convertView.findViewById(R.id.question_content_id);
				viewHolder.time = (TextView) convertView.findViewById(R.id.question_Time_idid);
				viewHolder.imageButton = (ImageButton) convertView.findViewById(R.id.ib_question);// 评论者点赞

				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			viewHolder.answerName.setText(data.get(position).getAnswerNameStr());
			viewHolder.count.setText(data.get(position).getCuonte());
			viewHolder.note.setText(Html.fromHtml(DropHtml.drop(data.get(position).getNoteStr())));
			viewHolder.time.setText(data.get(position).getTime());
			viewHolder.image.setImageUrl(data.get(position).getTitle(), imageLoader);
			System.out.println(data.get(position).getTitle() + "cesjo");
			viewHolder.imageButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (!data.get(index).isB()) {
						commentidid = data.get(index).getComment_id();
						sendcountJsonText();

						String string = data.get(index).getCuonte();
						int i = Integer.valueOf(string).intValue();
						i = i + 1;
						data.get(index).setCuonte(i + "");
						data.get(index).setB(true);
						// data.get(index).isB()=true;


						myAdapter.notifyDataSetChanged();
					} else {
						Toast.makeText(getApplicationContext(), "您已点赞", 0).show();
					}

				}
			});
			return convertView;
		}

		/**

		 * 点赞评论的JSON数据

		 */
		@SuppressLint("HandlerLeak")
		private void sendcountJsonText() {
			final Handler handler = new Handler() {
				private JSONObject jsonObject2;

				public void handleMessage(Message msg) {
					if (msg.what == 7) {
						String result = msg.obj.toString();
						try {
							jsonObject = new JSONObject(result);

						} catch (JSONException e) {
							// TODO Auto-generated catch block


							e.printStackTrace();
						}
					} else if (msg.what == 6) {
						// Toast.makeText(QuestionDetail.this, "帐号或密码错误",


						// 0).show();


					} else if (msg.what == 10) {
						Toast.makeText(QuestionDetail.this, "忘了联网了吧", 0).show();
					}

				}

			};
			new Thread() {
				public void run() {
					// 设置参数


					SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
					uid = sp.getString("uid", "0");
					List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
					params.add(new BasicNameValuePair("uid", uid)); // 密码

					params.add(new BasicNameValuePair("db", Constant.db));// 数据库

					params.add(new BasicNameValuePair("comment_id", commentidid));
					String result = Tools.sendPost(Constant.answerLike, params);
					Message message = new Message();
					try {
						if (result != null) {
							JsonNode jsonNode = om.readTree(result);
							code = jsonNode.get("code").asText();
							// history=jsonNode.get("note").asText();


							if (code.equals("200")) {
								message.what = 7;
								message.obj = result;
							} else {
								message.what = 6;// 登陆失败

							}
						} else {
							message.what = 9;// 无网络

						}
						handler.sendMessage(message);
					} catch (Exception e) {
						// TODO: handle exception


						e.printStackTrace();
						message.what = 10;
						handler.sendMessage(message);
					}
				};
			}.start();
		}

		/**

		 * 每次加1

		 * 

		 * @param a

		 */
		// public void addItem(AnswerPer a) {


		// data.add(a);


		// }




		public class ViewHolder {
			public TextView answerName;
			public TextView count;
			public TextView note;
			public NetworkImageView image;
			public TextView time;
			public ImageButton imageButton;
		}
	}

	/**

	 * 获取提问问题

	 */
	private void getJsonText() {
		progressDialog = ProgressDialog.show(QuestionDetail.this, "请稍等...", "正在登陆...", true, true);
		// 点击不会消失
		progressDialog.setCanceledOnTouchOutside(false);
		final Handler handler = new Handler() {
			private JSONObject jsonObject2;

			@SuppressLint("HandlerLeak")
			public void handleMessage(Message msg) {
				if (msg.what == 7) {

					String result = msg.obj.toString();
					try {
						jsonObject = new JSONObject(result);
						datata = new ArrayList<OneDatyPerson>();
						JSONObject jsonObject1 = jsonObject.getJSONObject("data");
						parentNameStr = (String) jsonObject1.get("parent_name");
						classNameStr = jsonObject1.getString("student_class");
						subjectNameStr = jsonObject1.getString("course");
						tiemNameStr = jsonObject1.getString("date");
						studentNameStr = jsonObject1.getString("student_name");
						answercountStr = jsonObject1.getString("answer_count");
						head_portrait = jsonObject1.getString("portrait");// 获得头像

						id = jsonObject1.get("id").toString();
						noteStr = jsonObject1.getString("note");
						JSONArray jsonArray3 = jsonObject1.getJSONArray("data_image");
						for (int i = 0; i < jsonArray3.length(); i++) {
							JSONObject object = (JSONObject) jsonArray3.get(i);
							noteStr_tu = (String) object.get("thumbnail");// 缩略图

							imageStr = (String) object.get("image");// 大图

							int id = object.getInt("image_id");
							imageBean.add(new BlogContentImageBean(id, imageStr, noteStr_tu));
						}
						//


						ClassAblumMSGAdapter_Qu cAdapter = new ClassAblumMSGAdapter_Qu(QuestionDetail.this, imageBean);
						gridView.setAdapter(cAdapter);
						// jsonObject1.getJSONArray(name)


						parentName.setText("(家长：" + parentNameStr + ")");// 设置父母姓名

						className.setText(classNameStr);// 设置班级姓名

						subjectName.setText(subjectNameStr);// 设置科目姓名

						timeName.setText(tiemNameStr);// 设置时间姓名

						studentName.setText(studentNameStr);// 设置学生姓名

						answercount.setText(answercountStr + "人回答");// 回答人数

						noTextView.setText(Html.fromHtml(DropHtml.drop(noteStr)));// 问题内容


						//he.setImageUrl(head_portrait, imageLoader);
						he.setImageURI(Uri.parse(head_portrait));
						JSONArray jsonArray2 = jsonObject1.getJSONArray("data_comment");
						for (int i = 0; i < jsonArray2.length(); i++) {
							jsonObject2 = (JSONObject) jsonArray2.opt(i);
							String comment_noteStr = (String) jsonObject2.get("comment_note");// 评论内容

							String like_countStr = (String) jsonObject2.get("comment_user");// 评论者名字

							String comment_portraitStr = (String) jsonObject2.get("comment_portrait");
							String like_count = jsonObject2.get("like_count").toString();
							String comment_date = (String) jsonObject2.get("comment_date");
							comment_id = jsonObject2.get("comment_id").toString();
							f = jsonObject2.getBoolean("is_like");
							String date = comment_date.toString();
							// String str = like_count.toString();


							AnswerPer answerPer = new AnswerPer();
							answerPer.setComment_id(comment_id);
							answerPer.setAnswerNameStr(like_countStr);
							answerPer.setNoteStr(comment_noteStr);
							answerPer.setB(f);
							answerPer.setTime(date);
							// answerPer


							// .setBitmap(stringtoBitmap(comment_portraitStr));


							answerPer.setTitle(comment_portraitStr);
							answerPer.setCuonte(like_count);
							data.add(answerPer);
						}
						myAdapter = new MyAdapter(QuestionDetail.this, data);
						lvAnswer.setAdapter(myAdapter);

					} catch (JSONException e) {
						// TODO Auto-generated catch block


						e.printStackTrace();
					}
				} else if (msg.what == 6) {
					Toast.makeText(QuestionDetail.this, "帐号或密码错误", 0).show();
				} else if (msg.what == 10) {
					Toast.makeText(QuestionDetail.this, "忘了联网了吧", 0).show();
				}
				progressDialog.dismiss();
			}

		};
		new Thread() {
			public void run() {
				// 设置参数


				SharedPreferences sp = getSharedPreferences("UserId", MODE_PRIVATE);
				String uid = sp.getString("uid", "0");
				List<BasicNameValuePair> params = new LinkedList<BasicNameValuePair>();
				Intent intent = getIntent();
				String Image_id_intent = intent.getStringExtra("questin_id");
				position = intent.getIntExtra("position", 0);
				params.add(new BasicNameValuePair("knowledge_id", Image_id_intent));
				params.add(new BasicNameValuePair("db", Constant.db));
				params.add(new BasicNameValuePair("uid", uid)); // 密码

				String result = Tools.sendGet(Constant.answerMsg, params);
				Message message = new Message();
				try {
					if (result != null) {
						JsonNode jsonNode = om.readTree(result);
						code = jsonNode.get("code").asText();
						// history=jsonNode.get("note").asText();


						if (code.equals("200")) {
							message.what = 7;
							message.obj = result;
						} else {
							message.what = 6;// 登陆失败

						}
					} else {
						message.what = 9;// 无网络

					}
					handler.sendMessage(message);
				} catch (Exception e) {
					// TODO: handle exception


					e.printStackTrace();
					message.what = 10;
					handler.sendMessage(message);
				}
			};
		}.start();
	}

	/**

	 * 数值转化

	 */
	public void INtent() {

		int i = Integer.parseInt(answercountStr);
		i = i + 1;
		str = i + "";
		answercount.setText(str + "人回答");
	}

	/**

	 * 返回键

	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			System.out.println("按下了back键   onKeyDown()");
			Intent intent = new Intent();
			intent.putExtra("addContent", str);
			intent.putExtra("position_back", position);
			setResult(RESULT_OK, intent);
			finish();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}

	}

	public void setViewPagerAndZoom(View v, final int position) {
		ViewPager expandedView = (ViewPager) findViewById(R.id.detail_view_id);
		View containerView = (FrameLayout) findViewById(R.id.container_of_album);

		// 实现放大缩小类，传入当前的容器和要放大展示的对象


		final QueZoomTutorial mZoomTutorial = new QueZoomTutorial(containerView, expandedView);
		QuViewPagerAdapter adapter = new QuViewPagerAdapter(context, imageBean, mZoomTutorial);
		expandedView.setAdapter(adapter);
		expandedView.setCurrentItem(position);
		// 通过传入Id来从小图片扩展到大图，开始执行动画


		mZoomTutorial.zoomImageFromThumb(v);
	}

	/**

	 * 控制图片放大啊缩小

	 */
	class ClassAblumMSGAdapter_Qu extends CommonAdapter<BlogContentImageBean> {
		private Context context;
		private NetworkImageView iView;
		private int h;

		ClassAblumMSGAdapter_Qu(Context context, List<BlogContentImageBean> datas) {
			super(context, datas);
			this.context = context;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			String image = mDatas.get(position).image;
			if (convertView == null) {
				iView = new NetworkImageView(QuestionDetail.this);
				WindowManager wm = (WindowManager)QuestionDetail.this.getSystemService(Context.WINDOW_SERVICE);
				int width = wm.getDefaultDisplay().getWidth()/5;
				iView.setLayoutParams(new GridView.LayoutParams(width, width));// 设置ImageView对象布局

				iView.setAdjustViewBounds(false);// 设置边界对齐

				iView.setScaleType(ImageView.ScaleType.FIT_XY);// 设置刻度的类型

				iView.setPadding(2, 2, 2, 2);// 设置间距

			} else {
				iView = (NetworkImageView) convertView;
			}

			iView.setImageUrl(image, imageLoader);


			iView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					setViewPagerAndZoom(v, position);
				}
			});

			return iView;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mQueue.cancelAll(this);
	}

}